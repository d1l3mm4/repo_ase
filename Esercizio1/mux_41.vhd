----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:46:18 01/11/2020 
-- Design Name: 
-- Module Name:    mux_41 - structural 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity mux_41 is
    Port ( a : in  STD_LOGIC_VECTOR(3 downto 0);
           c : in  STD_LOGIC_VECTOR(1 downto 0);
           z : out  STD_LOGIC);
end mux_41;

architecture structural of mux_41 is

component mux_21
    Port ( a : in  STD_LOGIC;
           b : in  STD_LOGIC;
           c : in  STD_LOGIC;
           z : out  STD_LOGIC);
end component;

signal s0: std_logic;
signal s1: std_logic;

begin

m1: mux_21
port map (a(0),
		 a(1),
		 c(0),
		s0);

m2: mux_21
port map(a(2),
		a(3),
		c(0),
		s1);

m3: mux_21
port map(s0,
		s1,
		c(1),
		z);

end structural;

