----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:33:04 01/11/2020 
-- Design Name: 
-- Module Name:    mux_81 - Structural 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity mux_81 is
    Port ( a : in  STD_LOGIC_VECTOR(7 downto 0);
           c : in  STD_LOGIC_VECTOR(2 downto 0);
           z : out  STD_LOGIC);
end mux_81;

architecture Structural of mux_81 is

component mux_41
Port ( a : in  STD_LOGIC_VECTOR(3 downto 0);
           c : in  STD_LOGIC_VECTOR(1 downto 0);
           z : out  STD_LOGIC);
end component;

component mux_21
PORT(
         a : IN  std_logic;
         b : IN  std_logic;
         c : IN  std_logic;
         z : OUT  std_logic
        );
    END COMPONENT;

signal s: std_logic_vector(1 downto 0);
begin

m0to1: for i in 0 to 1 GENERATE
m: mux_41
port map(a(4*i+3 downto i*4),
		   c(1 downto 0),
			s(i));
END GENERATE;

m2: mux_21
port map(s(0),
			s(1),
			c(2),
		   z);

end Structural;

