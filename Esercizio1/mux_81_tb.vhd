--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   15:07:15 01/11/2020
-- Design Name:   
-- Module Name:   C:/Users/annal/Xilinx/Esercizio1/mux_81_tb.vhd
-- Project Name:  Esercizio1
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: mux_81
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY mux_81_tb IS
END mux_81_tb;
 
ARCHITECTURE behavior OF mux_81_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT mux_81
    PORT(
         a : IN  std_logic_vector(7 downto 0);
         c : IN  std_logic_vector(2 downto 0);
         z : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal a : std_logic_vector(7 downto 0) := (others => '0');
   signal c : std_logic_vector(2 downto 0) := (others => '0');

 	--Outputs
   signal z : std_logic;
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: mux_81 PORT MAP (
          a => a,
          c => c,
          z => z
        );

	-- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

		a<="00000001";
		c<="000";

      wait for 100 ns;	

		a<="10000000";
		c<="111";

      wait for 100 ns;	
		
		a<="11011111";
		c<="101";

      wait for 100 ns;	
		

		a<="00010000";
		c<="100";


		
		


      wait;
   end process;

END;
