--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   11:37:44 01/11/2020
-- Design Name:   
-- Module Name:   C:/Users/annal/Xilinx/Esercizio1/mux_21_tb.vhd
-- Project Name:  Esercizio1
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: mux_21
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY mux_21_tb IS
END mux_21_tb;
 
ARCHITECTURE behavior OF mux_21_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT mux_21
    PORT(
         a : IN  std_logic;
         b : IN  std_logic;
         c : IN  std_logic;
         z : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal a : std_logic := '0';
   signal b : std_logic := '0';
   signal c : std_logic := '0';

 	--Outputs
   signal z : std_logic;

 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: mux_21 PORT MAP (
          a => a,
          b => b,
          c => c,
          z => z
        );


   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

      wait for 10 ns;

      a<='0';
		b<='1';
		c<='0';
		
		wait for 10 ns;
		
		a<='1';
		b<='1';
		c<='0';
		
		wait for 10 ns;
		
		a<='0';
		b<='1';
		c<='1';

      wait;
   end process;

END;
