----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    20:32:11 01/13/2020 
-- Design Name: 
-- Module Name:    FF_D2 - structural 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity FF_D2 is
    Port ( D : in  STD_LOGIC;
           CLK : in  STD_LOGIC;
           Q : buffer  STD_LOGIC;
           notQ : buffer  STD_LOGIC);
end FF_D2;

architecture structural of FF_D2 is

component RS_nor
    Port ( R : in  STD_LOGIC;
           S : in  STD_LOGIC;
           Q : buffer  STD_LOGIC;
           notQ : buffer  STD_LOGIC);
end component;

signal Y, W, Z, X : std_logic;
signal R1: std_logic;

begin

latch2: RS_nor
port map( X,
			 CLK,
			 Y,
			 W);

R1 <= W OR CLK;

latch1: RS_nor
port map( R1,
			 D,
			 Z,
			 X);
 
latch3: RS_nor
port map( W,  
			 Z,
			 Q,
			 notQ);
			 
end structural;

