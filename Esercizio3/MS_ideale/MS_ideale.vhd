----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    20:34:29 01/13/2020 
-- Design Name: 
-- Module Name:    MS_ideale - Structural 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity MS_ideale is
    Port ( D : in  STD_LOGIC;
           CLK : in  STD_LOGIC;
           Q : BUFFER  STD_LOGIC;
           notQ : BUFFER  STD_LOGIC);
end MS_ideale;

architecture Structural of MS_ideale is

component FF_D
    Port ( D : in  STD_LOGIC;
           CLK : in  STD_LOGIC;
           Q : buffer  STD_LOGIC;
           notQ : buffer  STD_LOGIC);
end component;

component FF_D2
    Port ( D : in  STD_LOGIC;
           CLK : in  STD_LOGIC;
           Q : buffer  STD_LOGIC;
           notQ : buffer  STD_LOGIC);
end component;

signal y: std_logic;

begin

ff1: FF_D
port map (D, 
		    CLK,
			 y,
			 open);
			 
ff2: FF_D2
port map (y,
			 CLK,
			 Q,
			 notQ);
			 
end Structural;

