----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:08:39 01/13/2020 
-- Design Name: 
-- Module Name:    FF_D_ET - rtl 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity RS is
    Port ( R : in  STD_LOGIC;
           S : in  STD_LOGIC;
           Q : buffer  STD_LOGIC;
           notQ : buffer  STD_LOGIC);
end RS;

architecture gate of RS is

signal q0: std_logic;

begin

Q <= notQ NOR R;
notQ <= Q NOR S;

end gate;

