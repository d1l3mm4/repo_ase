----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:15:16 01/13/2020 
-- Design Name: 
-- Module Name:    FF_D - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity FF_D is
    Port ( CLK : in  STD_LOGIC;
           D : in  STD_LOGIC;
           Q : buffer  STD_LOGIC;
           notQ : buffer  STD_LOGIC);
end FF_D;

architecture Structural of FF_D is

component RS
    Port ( R : in  STD_LOGIC;
           S : in  STD_LOGIC;
           Q : buffer  STD_LOGIC;
           notQ : buffer  STD_LOGIC);

end component;

signal X, Y, Z, W: std_logic;

signal R1: std_logic;

begin

Latch2: RS
port map(X,
		   CLK,
			Y,
			W);

R1 <= CLK or W;

Latch1: RS
port map (R1,
			 D,
			 Z,
			 X);
			 
Latch3: RS 
port map (W,
			Z, 
			Q,
			notQ);
			
end Structural;

