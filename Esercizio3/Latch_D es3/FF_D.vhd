----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    22:07:20 01/13/2020 
-- Design Name: 
-- Module Name:    FF_D - Structural 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity FF_D is
    Port ( D : in  STD_LOGIC;
           CLK : in  STD_LOGIC;
			  CLK2: buffer STD_LOGIC;
           Q : BUFFER  STD_LOGIC;
           notQ : BUFFER  STD_LOGIC);
end FF_D;

architecture Structural of FF_D is

component Latch_RS
    Port ( R : in  STD_LOGIC;
           S : in  STD_LOGIC;
           Q : BUFFER  STD_LOGIC;
           notQ : BUFFER  STD_LOGIC);
end component;


signal D_n: STD_LOGIC;
signal CLK_n: STD_LOGIC;
signal s: STD_LOGIC;
signal r: STD_LOGIC;

begin

D_n <= NOT D;
CLK_n <= NOT CLK after 1 ns; -- inverto il clock e creo un ritardo 
CLK2 <= CLK NOR  CLK_n; -- metto in nor con il clock al fine di creare un ritardo e avere sul fronte di discesa del clock un impulso

s<= D AND CLK2;
r<= D_n AND CLK2;

latch: Latch_RS
port map (R,
			 S,
			 Q,
			 notQ);
			 
end Structural;

