--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   22:12:17 01/13/2020
-- Design Name:   
-- Module Name:   C:/Users/annal/Xilinx/Esercizio3/Latch_D/FF_D_tb.vhd
-- Project Name:  Latch_D
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: FF_D
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY FF_D_tb IS
END FF_D_tb;
 
ARCHITECTURE behavior OF FF_D_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT FF_D
    PORT(
         D : IN  std_logic;
         CLK : IN  std_logic;
			CLK2: buffer std_logic;
         Q : buffer  std_logic;
         notQ : buffer  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal D : std_logic := '0';
   signal CLK : std_logic := '0';
   signal CLK2 : std_logic := '0';
 	--Outputs
   signal Q : std_logic;
   signal notQ : std_logic;

   -- Clock period definitions
   constant CLK_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: FF_D PORT MAP (
          D => D,
          CLK => CLK,
			 CLK2 => CLK2,
          Q => Q,  
          notQ => notQ
        );

   -- Clock process definitions
   CLK_process :process
   begin
		CLK <= '0';
		wait for CLK_period/2;
		CLK <= '1';
		wait for CLK_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

      wait for 1 ns;

      D<='1';
		
		wait for CLK_period*10;
		
		D <='0';
		
		wait for CLK_period*10;
		
		D <='0';
		
		wait for CLK_period*10;
		
		D <='1';
		
		wait for CLK_period*10;
		
		D <='0';
		
		wait for CLK_period*10;
		
		D <='1';
		
		
		
      wait;
   end process;

END;
