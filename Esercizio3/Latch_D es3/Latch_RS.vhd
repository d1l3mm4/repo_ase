----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    21:38:48 01/13/2020 
-- Design Name: 
-- Module Name:    Latch_RS - gate 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Latch_RS is
    Port ( R : in  STD_LOGIC;
           S : in  STD_LOGIC;
           Q : buffer  STD_LOGIC;
           notQ : buffer  STD_LOGIC);
end Latch_RS;

architecture gate of Latch_RS is

begin

Q <= R nor notQ;
notQ <= S nor Q;


end gate;

