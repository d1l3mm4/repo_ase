----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    17:58:09 01/13/2020 
-- Design Name: 
-- Module Name:    RS_latch - gate 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity RS_latch is
    Port ( R : in  STD_LOGIC;
           S : in  STD_LOGIC;
			  CLK: in STD_LOGIC;
           Q : out  STD_LOGIC;
           notQ : out  STD_LOGIC);
end RS_latch;

architecture gate of RS_latch is

signal q0: std_logic;
signal notq0: std_logic;

begin


q0 <= (R AND CLK) NOR notq0;
notq0 <= (S AND CLK) NOR q0;

Q <=q0;
notQ <= notq0;

end gate;

