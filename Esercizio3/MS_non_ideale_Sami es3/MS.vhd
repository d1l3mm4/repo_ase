----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    18:01:39 01/13/2020 
-- Design Name: 
-- Module Name:    MS - Structural 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity MS is
    Port ( D : in  STD_LOGIC;
           CLK : in  STD_LOGIC;
           Q : OUT  STD_LOGIC;
           notQ : OUT  STD_LOGIC);
end MS;

architecture Structural of MS is

component RS_latch
    Port ( R : in  STD_LOGIC;
           S : in  STD_LOGIC;
			  CLK: in STD_LOGIC;
           Q : out  STD_LOGIC;
           notQ : out  STD_LOGIC);
end component;

signal Q1, notQ1: std_logic;
signal D_n, CLK_n: STD_LOGIC;

begin

D_n <= NOT D;
CLK_n <= NOT CLK;

Latch1: RS_latch
port map ( R => D_n,
			  S => D,
			  CLK => CLK,
			  Q => Q1,
			  notQ => notQ1);
			 
Latch2: RS_latch
port map ( R =>  notQ1,
			  S => Q1,
			  CLK => CLK_n,
			  Q => Q,
			  notQ => notQ);
			  
end Structural;

