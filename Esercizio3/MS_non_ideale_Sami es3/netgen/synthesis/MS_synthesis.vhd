--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Version: P.20131013
--  \   \         Application: netgen
--  /   /         Filename: MS_synthesis.vhd
-- /___/   /\     Timestamp: Thu Sep 03 17:55:45 2020
-- \   \  /  \ 
--  \___\/\___\
--             
-- Command	: -intstyle ise -ar Structure -tm MS -w -dir netgen/synthesis -ofmt vhdl -sim MS.ngc MS_synthesis.vhd 
-- Device	: xa3s200a-4-ftg256
-- Input file	: MS.ngc
-- Output file	: C:\ASE\Xilinx - Copy\Esercizio3\MS\netgen\synthesis\MS_synthesis.vhd
-- # of Entities	: 1
-- Design Name	: MS
-- Xilinx	: C:\Xilinx\14.7\ISE_DS\ISE\
--             
-- Purpose:    
--     This VHDL netlist is a verification model and uses simulation 
--     primitives which may not represent the true implementation of the 
--     device, however the netlist is functionally correct and should not 
--     be modified. This file cannot be synthesized and should only be used 
--     with supported simulation tools.
--             
-- Reference:  
--     Command Line Tools User Guide, Chapter 23
--     Synthesis and Simulation Design Guide, Chapter 6
--             
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
use UNISIM.VPKG.ALL;

entity MS is
  port (
    CLK : in STD_LOGIC := 'X'; 
    notQ : out STD_LOGIC; 
    D : in STD_LOGIC := 'X'; 
    Q : out STD_LOGIC 
  );
end MS;

architecture Structure of MS is
  signal CLK_BUFGP_1 : STD_LOGIC; 
  signal CLK_n : STD_LOGIC; 
  signal D_IBUF_4 : STD_LOGIC; 
  signal Latch1_notq0_5 : STD_LOGIC; 
  signal Latch1_notq0_not0001 : STD_LOGIC; 
  signal Latch1_q0_7 : STD_LOGIC; 
  signal Latch1_q0_not0001 : STD_LOGIC; 
  signal Latch2_notq0_9 : STD_LOGIC; 
  signal Latch2_notq0_not0001 : STD_LOGIC; 
  signal Latch2_q0_11 : STD_LOGIC; 
  signal Latch2_q0_not0001 : STD_LOGIC; 
  signal notQ_OBUF_15 : STD_LOGIC; 
begin
  Latch2_q0 : LD
    port map (
      D => Latch2_q0_not0001,
      G => CLK_n,
      Q => Latch2_q0_11
    );
  Latch2_notq0 : LD
    port map (
      D => Latch2_notq0_not0001,
      G => CLK_n,
      Q => Latch2_notq0_9
    );
  Latch1_q0 : LD
    port map (
      D => Latch1_q0_not0001,
      G => CLK_BUFGP_1,
      Q => Latch1_q0_7
    );
  Latch1_notq0 : LD
    port map (
      D => Latch1_notq0_not0001,
      G => CLK_BUFGP_1,
      Q => Latch1_notq0_5
    );
  Latch1_q0_not00011 : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => D_IBUF_4,
      I1 => Latch1_notq0_5,
      O => Latch1_q0_not0001
    );
  Latch1_notq0_not00011 : LUT2
    generic map(
      INIT => X"1"
    )
    port map (
      I0 => D_IBUF_4,
      I1 => Latch1_q0_7,
      O => Latch1_notq0_not0001
    );
  Latch2_q0_not00011 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => Latch2_notq0_9,
      I1 => Latch1_q0_7,
      O => Latch2_q0_not0001
    );
  Latch2_notq0_not00011 : LUT2
    generic map(
      INIT => X"1"
    )
    port map (
      I0 => Latch2_q0_11,
      I1 => Latch1_q0_7,
      O => Latch2_notq0_not0001
    );
  D_IBUF : IBUF
    port map (
      I => D,
      O => D_IBUF_4
    );
  notQ_OBUF : OBUF
    port map (
      I => notQ_OBUF_15,
      O => notQ
    );
  Q_OBUF : OBUF
    port map (
      I => Latch2_q0_11,
      O => Q
    );
  CLK_BUFGP : BUFGP
    port map (
      I => CLK,
      O => CLK_BUFGP_1
    );
  CLK_n1_INV_0 : INV
    port map (
      I => CLK_BUFGP_1,
      O => CLK_n
    );
  Latch2_notQ1_INV_0 : INV
    port map (
      I => Latch2_q0_11,
      O => notQ_OBUF_15
    );

end Structure;

