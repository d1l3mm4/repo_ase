----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    22:09:35 01/15/2020 
-- Design Name: 
-- Module Name:    Shift_Register - Structural 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Shift_Register is
    Port ( data_in : in  STD_LOGIC_VECTOR(7 downto 0);
			  clock: in STD_LOGIC;
           shift : in  STD_LOGIC;
           enable : in  STD_LOGIC;
           data_out : out  STD_LOGIC_VECTOR(7 downto 0));
end Shift_Register;

architecture Structural of Shift_Register is

component SS_cell
port (
		clock: in std_logic;
		enable: in std_logic;
		d_par: in std_logic;
		d_ser: in std_logic;
		shift: in std_logic;
		d_out: out std_logic);
end component;

signal q: std_logic_vector(7 downto 0);

begin

sc0: SS_Cell
port map( clock,
			 enable,
			 data_in(0),
			 q(7), -- il registro � circolare quindi metto in ingresso alla prima cella l'uscita dell'ultima
			 shift,
			 q(0));
			 
sc0to7:  for i in 1 to 7 generate
	sc: SS_Cell
	port map( clock,
				 enable,
				 data_in(i),
				 q(i-1),
				 shift,
				 q(i));
end generate;

data_out <= q;

end structural;