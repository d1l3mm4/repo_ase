--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   11:25:09 09/23/2020
-- Design Name:   
-- Module Name:   C:/Users/annal/Xilinx/ShiftRegister/SR.vhd
-- Project Name:  ShiftRegister
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: Shift_Register
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY SR IS
END SR;
 
ARCHITECTURE behavior OF SR IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT Shift_Register
    PORT(
         data_in : IN  std_logic_vector(7 downto 0);
         clock : IN  std_logic;
         shift : IN  std_logic;
         enable : IN  std_logic;
         data_out : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal data_in : std_logic_vector(7 downto 0) := (others => '0');
   signal clock : std_logic := '0';
   signal shift : std_logic := '0';
   signal enable : std_logic := '0';

 	--Outputs
   signal data_out : std_logic_vector(7 downto 0);

   -- Clock period definitions
   constant clock_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: Shift_Register PORT MAP (
          data_in => data_in,
          clock => clock,
          shift => shift,
          enable => enable,
          data_out => data_out
        );

   -- Clock process definitions
   clock_process :process
   begin
		clock <= '0';
		wait for clock_period/2;
		clock <= '1';
		wait for clock_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
		enable <='1';
		shift <='0';
		data_in <="00000000";
      wait for 90 ns;
		
		wait for clock_period;
		
		enable <='0';
		shift <='0';
		data_in <="01000000";
		
		wait for clock_period;
		
		enable <='1';
		shift <='0';
		data_in <="01000000";
		
		wait for clock_period;
		
		enable <='0';
		shift <='1';
		
      wait for clock_period;
		enable <='0';
		
		wait for clock_period;
		enable <='1';
		
	   wait for clock_period;
		enable <='0';
		      
		wait for clock_period;
		enable <='1';
		
		wait for clock_period;
		enable <='0';
		      
		wait for clock_period;
		enable <='1';
		
		wait for clock_period;
		enable <='0';
		      
		wait for clock_period;
		enable <='1';
		
		wait for clock_period;
		enable <='0';
		      
		wait for clock_period;
		enable <='1';
		
		wait for clock_period;
		enable <='0';
      wait;
   end process;

END;
