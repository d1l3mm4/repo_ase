--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   21:03:24 01/11/2020
-- Design Name:   
-- Module Name:   C:/Users/annal/Xilinx/Esercizio7/arbitro_tb.vhd
-- Project Name:  Esercizio7
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: arbitro
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY arbitro_tb IS
END arbitro_tb;
 
ARCHITECTURE behavior OF arbitro_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT arbitro
    PORT(
         A1 : IN  std_logic;
         A2 : IN  std_logic;
         A3 : IN  std_logic;
         Y : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal A1 : std_logic := '0';
   signal A2 : std_logic := '0';
   signal A3 : std_logic := '0';

 	--Outputs
   signal Y : std_logic;

BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: arbitro PORT MAP (
          A1 => A1,
          A2 => A2,
          A3 => A3,
          Y => Y
        );


   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	
		
		A1<= '0';
		A2 <= '0';
		A3 <= '1';
		
		wait for 10 ns;
		
		A1<= '0';
		A2 <= '1';
		A3 <= '1';
		
		wait for 10 ns;
		
		A1<= '1';
		A2 <= '0';
		A3 <= '1';
		
		wait for 10 ns;
		
		A1<= '0';
		A2 <= '1';
		A3 <= '0';
		
		wait for 10 ns;
		
		A1<= '1';
		A2 <= '1';
		A3 <= '1';
		
		wait for 10 ns;
		
		
      wait;
   end process;

END;
