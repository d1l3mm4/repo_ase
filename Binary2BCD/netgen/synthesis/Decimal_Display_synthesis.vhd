--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Version: P.20131013
--  \   \         Application: netgen
--  /   /         Filename: Decimal_Display_synthesis.vhd
-- /___/   /\     Timestamp: Mon Aug 31 16:20:01 2020
-- \   \  /  \ 
--  \___\/\___\
--             
-- Command	: -intstyle ise -ar Structure -tm Decimal_Display -w -dir netgen/synthesis -ofmt vhdl -sim Decimal_Display.ngc Decimal_Display_synthesis.vhd 
-- Device	: xc7a100t-3-csg324
-- Input file	: Decimal_Display.ngc
-- Output file	: C:\ASE\ProvaBinary2BCD\netgen\synthesis\Decimal_Display_synthesis.vhd
-- # of Entities	: 1
-- Design Name	: Decimal_Display
-- Xilinx	: C:\Xilinx\14.7\ISE_DS\ISE\
--             
-- Purpose:    
--     This VHDL netlist is a verification model and uses simulation 
--     primitives which may not represent the true implementation of the 
--     device, however the netlist is functionally correct and should not 
--     be modified. This file cannot be synthesized and should only be used 
--     with supported simulation tools.
--             
-- Reference:  
--     Command Line Tools User Guide, Chapter 23
--     Synthesis and Simulation Design Guide, Chapter 6
--             
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
use UNISIM.VPKG.ALL;

entity Decimal_Display is
  port (
    clock : in STD_LOGIC := 'X'; 
    reset : in STD_LOGIC := 'X'; 
    load : in STD_LOGIC := 'X'; 
    switches : in STD_LOGIC_VECTOR ( 13 downto 0 ); 
    anodes : out STD_LOGIC_VECTOR ( 7 downto 0 ); 
    cathodes : out STD_LOGIC_VECTOR ( 7 downto 0 ) 
  );
end Decimal_Display;

architecture Structure of Decimal_Display is
  signal switches_13_IBUF_0 : STD_LOGIC; 
  signal switches_12_IBUF_1 : STD_LOGIC; 
  signal switches_11_IBUF_2 : STD_LOGIC; 
  signal switches_10_IBUF_3 : STD_LOGIC; 
  signal switches_9_IBUF_4 : STD_LOGIC; 
  signal switches_8_IBUF_5 : STD_LOGIC; 
  signal switches_7_IBUF_6 : STD_LOGIC; 
  signal switches_6_IBUF_7 : STD_LOGIC; 
  signal switches_5_IBUF_8 : STD_LOGIC; 
  signal switches_4_IBUF_9 : STD_LOGIC; 
  signal switches_3_IBUF_10 : STD_LOGIC; 
  signal switches_2_IBUF_11 : STD_LOGIC; 
  signal switches_1_IBUF_12 : STD_LOGIC; 
  signal switches_0_IBUF_13 : STD_LOGIC; 
  signal clock_BUFGP_14 : STD_LOGIC; 
  signal load_IBUF_15 : STD_LOGIC; 
  signal cathodes_6_OBUF_32 : STD_LOGIC; 
  signal cathodes_5_OBUF_33 : STD_LOGIC; 
  signal cathodes_4_OBUF_34 : STD_LOGIC; 
  signal cathodes_3_OBUF_35 : STD_LOGIC; 
  signal cathodes_2_OBUF_36 : STD_LOGIC; 
  signal cathodes_1_OBUF_37 : STD_LOGIC; 
  signal cathodes_0_OBUF_38 : STD_LOGIC; 
  signal anodes_3_OBUF_39 : STD_LOGIC; 
  signal anodes_2_OBUF_40 : STD_LOGIC; 
  signal anodes_1_OBUF_41 : STD_LOGIC; 
  signal anodes_0_OBUF_42 : STD_LOGIC; 
  signal anodes_7_OBUF_43 : STD_LOGIC; 
  signal D_clk_filter_end_count_44 : STD_LOGIC; 
  signal D_counter_count_1_GND_10_o_mux_3_OUT_0_Q : STD_LOGIC; 
  signal D_counter_count_1_GND_10_o_mux_3_OUT_1_Q : STD_LOGIC; 
  signal D_clk_filter_PWR_11_o_divisore_frequenza_count_4_equal_1_o : STD_LOGIC; 
  signal D_clk_filter_Mcount_divisore_frequenza_count : STD_LOGIC; 
  signal D_clk_filter_Mcount_divisore_frequenza_count1 : STD_LOGIC; 
  signal D_clk_filter_Mcount_divisore_frequenza_count2 : STD_LOGIC; 
  signal D_clk_filter_Mcount_divisore_frequenza_count3 : STD_LOGIC; 
  signal D_clk_filter_Mcount_divisore_frequenza_count4 : STD_LOGIC; 
  signal B_Mcount_bit_count3 : STD_LOGIC; 
  signal B_Mcount_bit_count2 : STD_LOGIC; 
  signal B_Mcount_bit_count1 : STD_LOGIC; 
  signal B_Mcount_bit_count : STD_LOGIC; 
  signal B_state_68 : STD_LOGIC; 
  signal B_n0076_inv : STD_LOGIC; 
  signal B_n0071_inv : STD_LOGIC; 
  signal B_bit_count_3_ena_MUX_74_o : STD_LOGIC; 
  signal B_converter_inputs_0_72 : STD_LOGIC; 
  signal B_bcd_digits_4_digit_0_bcd_0_Q : STD_LOGIC; 
  signal B_bcd_digits_4_digit_0_bcd_1_Q : STD_LOGIC; 
  signal B_bcd_digits_4_digit_0_bcd_2_Q : STD_LOGIC; 
  signal B_bcd_digits_4_digit_0_bcd_3_Q : STD_LOGIC; 
  signal B_bcd_digits_3_digit_0_bcd_0_Q : STD_LOGIC; 
  signal B_bcd_digits_3_digit_0_bcd_1_Q : STD_LOGIC; 
  signal B_bcd_digits_3_digit_0_bcd_2_Q : STD_LOGIC; 
  signal B_bcd_digits_3_digit_0_bcd_3_Q : STD_LOGIC; 
  signal B_bcd_digits_2_digit_0_bcd_0_Q : STD_LOGIC; 
  signal B_bcd_digits_2_digit_0_bcd_1_Q : STD_LOGIC; 
  signal B_bcd_digits_2_digit_0_bcd_2_Q : STD_LOGIC; 
  signal B_bcd_digits_2_digit_0_bcd_3_Q : STD_LOGIC; 
  signal B_bcd_digits_1_digit_0_bcd_0_Q : STD_LOGIC; 
  signal B_bcd_digits_1_digit_0_bcd_1_Q : STD_LOGIC; 
  signal B_bcd_digits_1_digit_0_bcd_2_Q : STD_LOGIC; 
  signal B_bcd_digits_1_digit_0_bcd_3_Q : STD_LOGIC; 
  signal B_binary_reg_13_binary_reg_13_mux_9_OUT_0_Q : STD_LOGIC; 
  signal B_binary_reg_13_binary_reg_13_mux_9_OUT_1_Q : STD_LOGIC; 
  signal B_binary_reg_13_binary_reg_13_mux_9_OUT_2_Q : STD_LOGIC; 
  signal B_binary_reg_13_binary_reg_13_mux_9_OUT_3_Q : STD_LOGIC; 
  signal B_binary_reg_13_binary_reg_13_mux_9_OUT_4_Q : STD_LOGIC; 
  signal B_binary_reg_13_binary_reg_13_mux_9_OUT_5_Q : STD_LOGIC; 
  signal B_binary_reg_13_binary_reg_13_mux_9_OUT_6_Q : STD_LOGIC; 
  signal B_binary_reg_13_binary_reg_13_mux_9_OUT_7_Q : STD_LOGIC; 
  signal B_binary_reg_13_binary_reg_13_mux_9_OUT_8_Q : STD_LOGIC; 
  signal B_binary_reg_13_binary_reg_13_mux_9_OUT_9_Q : STD_LOGIC; 
  signal B_binary_reg_13_binary_reg_13_mux_9_OUT_10_Q : STD_LOGIC; 
  signal B_binary_reg_13_binary_reg_13_mux_9_OUT_11_Q : STD_LOGIC; 
  signal B_binary_reg_13_binary_reg_13_mux_9_OUT_12_Q : STD_LOGIC; 
  signal B_binary_reg_13_binary_reg_13_mux_9_OUT_13_Q : STD_LOGIC; 
  signal B_bcd_digits_4_digit_0_bcd_2_GND_25_o_mux_0_OUT_0_Q : STD_LOGIC; 
  signal B_bcd_digits_4_digit_0_bcd_2_GND_25_o_mux_0_OUT_1_Q : STD_LOGIC; 
  signal B_bcd_digits_4_digit_0_bcd_2_GND_25_o_mux_0_OUT_2_Q : STD_LOGIC; 
  signal B_bcd_digits_4_digit_0_bcd_2_GND_25_o_mux_0_OUT_3_Q : STD_LOGIC; 
  signal B_bcd_digits_3_digit_0_bcd_2_GND_25_o_mux_0_OUT_0_Q : STD_LOGIC; 
  signal B_bcd_digits_3_digit_0_bcd_2_GND_25_o_mux_0_OUT_1_Q : STD_LOGIC; 
  signal B_bcd_digits_3_digit_0_bcd_2_GND_25_o_mux_0_OUT_2_Q : STD_LOGIC; 
  signal B_bcd_digits_3_digit_0_bcd_2_GND_25_o_mux_0_OUT_3_Q : STD_LOGIC; 
  signal B_bcd_digits_2_digit_0_bcd_2_GND_25_o_mux_0_OUT_0_Q : STD_LOGIC; 
  signal B_bcd_digits_2_digit_0_bcd_2_GND_25_o_mux_0_OUT_1_Q : STD_LOGIC; 
  signal B_bcd_digits_2_digit_0_bcd_2_GND_25_o_mux_0_OUT_2_Q : STD_LOGIC; 
  signal B_bcd_digits_2_digit_0_bcd_2_GND_25_o_mux_0_OUT_3_Q : STD_LOGIC; 
  signal B_bcd_digits_1_digit_0_reset_n_inv : STD_LOGIC; 
  signal B_bcd_digits_1_digit_0_bcd_2_GND_25_o_mux_0_OUT_0_Q : STD_LOGIC; 
  signal B_bcd_digits_1_digit_0_bcd_2_GND_25_o_mux_0_OUT_1_Q : STD_LOGIC; 
  signal B_bcd_digits_1_digit_0_bcd_2_GND_25_o_mux_0_OUT_2_Q : STD_LOGIC; 
  signal B_bcd_digits_1_digit_0_bcd_2_GND_25_o_mux_0_OUT_3_Q : STD_LOGIC; 
  signal B_bcd_digits_4_digit_0_prev_ena_138 : STD_LOGIC; 
  signal N01 : STD_LOGIC; 
  signal B_bcd_0_dpot_173 : STD_LOGIC; 
  signal B_bcd_1_dpot_174 : STD_LOGIC; 
  signal B_bcd_2_dpot_175 : STD_LOGIC; 
  signal B_bcd_3_dpot_176 : STD_LOGIC; 
  signal B_bcd_4_dpot_177 : STD_LOGIC; 
  signal B_bcd_5_dpot_178 : STD_LOGIC; 
  signal B_bcd_6_dpot_179 : STD_LOGIC; 
  signal B_bcd_7_dpot_180 : STD_LOGIC; 
  signal B_bcd_8_dpot_181 : STD_LOGIC; 
  signal B_bcd_9_dpot_182 : STD_LOGIC; 
  signal B_bcd_10_dpot_183 : STD_LOGIC; 
  signal B_bcd_11_dpot_184 : STD_LOGIC; 
  signal B_bcd_12_dpot_185 : STD_LOGIC; 
  signal B_bcd_13_dpot_186 : STD_LOGIC; 
  signal B_bcd_14_dpot_187 : STD_LOGIC; 
  signal B_bcd_15_dpot_188 : STD_LOGIC; 
  signal B_bcd : STD_LOGIC_VECTOR ( 15 downto 0 ); 
  signal D_counter_count : STD_LOGIC_VECTOR ( 1 downto 0 ); 
  signal D_clk_filter_divisore_frequenza_count : STD_LOGIC_VECTOR ( 4 downto 0 ); 
  signal D_cathodes_instance_nibble : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal B_binary_reg : STD_LOGIC_VECTOR ( 13 downto 0 ); 
  signal B_bit_count : STD_LOGIC_VECTOR ( 3 downto 0 ); 
begin
  XST_VCC : VCC
    port map (
      P => anodes_7_OBUF_43
    );
  D_counter_count_1 : FDCE
    generic map(
      INIT => '0'
    )
    port map (
      C => clock_BUFGP_14,
      CE => D_clk_filter_end_count_44,
      CLR => B_bcd_digits_1_digit_0_reset_n_inv,
      D => D_counter_count_1_GND_10_o_mux_3_OUT_1_Q,
      Q => D_counter_count(1)
    );
  D_counter_count_0 : FDCE
    generic map(
      INIT => '0'
    )
    port map (
      C => clock_BUFGP_14,
      CE => D_clk_filter_end_count_44,
      CLR => B_bcd_digits_1_digit_0_reset_n_inv,
      D => D_counter_count_1_GND_10_o_mux_3_OUT_0_Q,
      Q => D_counter_count(0)
    );
  D_clk_filter_end_count : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => clock_BUFGP_14,
      CLR => B_bcd_digits_1_digit_0_reset_n_inv,
      D => D_clk_filter_PWR_11_o_divisore_frequenza_count_4_equal_1_o,
      Q => D_clk_filter_end_count_44
    );
  D_clk_filter_divisore_frequenza_count_0 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => clock_BUFGP_14,
      CLR => B_bcd_digits_1_digit_0_reset_n_inv,
      D => D_clk_filter_Mcount_divisore_frequenza_count,
      Q => D_clk_filter_divisore_frequenza_count(0)
    );
  D_clk_filter_divisore_frequenza_count_1 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => clock_BUFGP_14,
      CLR => B_bcd_digits_1_digit_0_reset_n_inv,
      D => D_clk_filter_Mcount_divisore_frequenza_count1,
      Q => D_clk_filter_divisore_frequenza_count(1)
    );
  D_clk_filter_divisore_frequenza_count_2 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => clock_BUFGP_14,
      CLR => B_bcd_digits_1_digit_0_reset_n_inv,
      D => D_clk_filter_Mcount_divisore_frequenza_count2,
      Q => D_clk_filter_divisore_frequenza_count(2)
    );
  D_clk_filter_divisore_frequenza_count_3 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => clock_BUFGP_14,
      CLR => B_bcd_digits_1_digit_0_reset_n_inv,
      D => D_clk_filter_Mcount_divisore_frequenza_count3,
      Q => D_clk_filter_divisore_frequenza_count(3)
    );
  D_clk_filter_divisore_frequenza_count_4 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => clock_BUFGP_14,
      CLR => B_bcd_digits_1_digit_0_reset_n_inv,
      D => D_clk_filter_Mcount_divisore_frequenza_count4,
      Q => D_clk_filter_divisore_frequenza_count(4)
    );
  B_bit_count_3 : FDCE
    generic map(
      INIT => '0'
    )
    port map (
      C => clock_BUFGP_14,
      CE => B_bit_count_3_ena_MUX_74_o,
      CLR => B_bcd_digits_1_digit_0_reset_n_inv,
      D => B_Mcount_bit_count3,
      Q => B_bit_count(3)
    );
  B_bit_count_2 : FDCE
    generic map(
      INIT => '0'
    )
    port map (
      C => clock_BUFGP_14,
      CE => B_bit_count_3_ena_MUX_74_o,
      CLR => B_bcd_digits_1_digit_0_reset_n_inv,
      D => B_Mcount_bit_count2,
      Q => B_bit_count(2)
    );
  B_bit_count_1 : FDCE
    generic map(
      INIT => '0'
    )
    port map (
      C => clock_BUFGP_14,
      CE => B_bit_count_3_ena_MUX_74_o,
      CLR => B_bcd_digits_1_digit_0_reset_n_inv,
      D => B_Mcount_bit_count1,
      Q => B_bit_count(1)
    );
  B_bit_count_0 : FDCE
    generic map(
      INIT => '0'
    )
    port map (
      C => clock_BUFGP_14,
      CE => B_bit_count_3_ena_MUX_74_o,
      CLR => B_bcd_digits_1_digit_0_reset_n_inv,
      D => B_Mcount_bit_count,
      Q => B_bit_count(0)
    );
  B_bcd_15 : FDCE
    port map (
      C => clock_BUFGP_14,
      CE => B_state_68,
      CLR => B_bcd_digits_1_digit_0_reset_n_inv,
      D => B_bcd_15_dpot_188,
      Q => B_bcd(15)
    );
  B_bcd_14 : FDCE
    port map (
      C => clock_BUFGP_14,
      CE => B_state_68,
      CLR => B_bcd_digits_1_digit_0_reset_n_inv,
      D => B_bcd_14_dpot_187,
      Q => B_bcd(14)
    );
  B_bcd_13 : FDCE
    port map (
      C => clock_BUFGP_14,
      CE => B_state_68,
      CLR => B_bcd_digits_1_digit_0_reset_n_inv,
      D => B_bcd_13_dpot_186,
      Q => B_bcd(13)
    );
  B_bcd_12 : FDCE
    port map (
      C => clock_BUFGP_14,
      CE => B_state_68,
      CLR => B_bcd_digits_1_digit_0_reset_n_inv,
      D => B_bcd_12_dpot_185,
      Q => B_bcd(12)
    );
  B_bcd_11 : FDCE
    port map (
      C => clock_BUFGP_14,
      CE => B_state_68,
      CLR => B_bcd_digits_1_digit_0_reset_n_inv,
      D => B_bcd_11_dpot_184,
      Q => B_bcd(11)
    );
  B_bcd_10 : FDCE
    port map (
      C => clock_BUFGP_14,
      CE => B_state_68,
      CLR => B_bcd_digits_1_digit_0_reset_n_inv,
      D => B_bcd_10_dpot_183,
      Q => B_bcd(10)
    );
  B_bcd_9 : FDCE
    port map (
      C => clock_BUFGP_14,
      CE => B_state_68,
      CLR => B_bcd_digits_1_digit_0_reset_n_inv,
      D => B_bcd_9_dpot_182,
      Q => B_bcd(9)
    );
  B_bcd_8 : FDCE
    port map (
      C => clock_BUFGP_14,
      CE => B_state_68,
      CLR => B_bcd_digits_1_digit_0_reset_n_inv,
      D => B_bcd_8_dpot_181,
      Q => B_bcd(8)
    );
  B_bcd_7 : FDCE
    port map (
      C => clock_BUFGP_14,
      CE => B_state_68,
      CLR => B_bcd_digits_1_digit_0_reset_n_inv,
      D => B_bcd_7_dpot_180,
      Q => B_bcd(7)
    );
  B_bcd_6 : FDCE
    port map (
      C => clock_BUFGP_14,
      CE => B_state_68,
      CLR => B_bcd_digits_1_digit_0_reset_n_inv,
      D => B_bcd_6_dpot_179,
      Q => B_bcd(6)
    );
  B_bcd_5 : FDCE
    port map (
      C => clock_BUFGP_14,
      CE => B_state_68,
      CLR => B_bcd_digits_1_digit_0_reset_n_inv,
      D => B_bcd_5_dpot_178,
      Q => B_bcd(5)
    );
  B_bcd_4 : FDCE
    port map (
      C => clock_BUFGP_14,
      CE => B_state_68,
      CLR => B_bcd_digits_1_digit_0_reset_n_inv,
      D => B_bcd_4_dpot_177,
      Q => B_bcd(4)
    );
  B_bcd_3 : FDCE
    port map (
      C => clock_BUFGP_14,
      CE => B_state_68,
      CLR => B_bcd_digits_1_digit_0_reset_n_inv,
      D => B_bcd_3_dpot_176,
      Q => B_bcd(3)
    );
  B_bcd_2 : FDCE
    port map (
      C => clock_BUFGP_14,
      CE => B_state_68,
      CLR => B_bcd_digits_1_digit_0_reset_n_inv,
      D => B_bcd_2_dpot_175,
      Q => B_bcd(2)
    );
  B_bcd_1 : FDCE
    port map (
      C => clock_BUFGP_14,
      CE => B_state_68,
      CLR => B_bcd_digits_1_digit_0_reset_n_inv,
      D => B_bcd_1_dpot_174,
      Q => B_bcd(1)
    );
  B_bcd_0 : FDCE
    port map (
      C => clock_BUFGP_14,
      CE => B_state_68,
      CLR => B_bcd_digits_1_digit_0_reset_n_inv,
      D => B_bcd_0_dpot_173,
      Q => B_bcd(0)
    );
  B_state : FDC
    port map (
      C => clock_BUFGP_14,
      CLR => B_bcd_digits_1_digit_0_reset_n_inv,
      D => B_bit_count_3_ena_MUX_74_o,
      Q => B_state_68
    );
  B_converter_inputs_0 : FDE
    port map (
      C => clock_BUFGP_14,
      CE => B_n0076_inv,
      D => B_binary_reg(13),
      Q => B_converter_inputs_0_72
    );
  B_binary_reg_13 : FDE
    port map (
      C => clock_BUFGP_14,
      CE => B_n0071_inv,
      D => B_binary_reg_13_binary_reg_13_mux_9_OUT_13_Q,
      Q => B_binary_reg(13)
    );
  B_binary_reg_12 : FDE
    port map (
      C => clock_BUFGP_14,
      CE => B_n0071_inv,
      D => B_binary_reg_13_binary_reg_13_mux_9_OUT_12_Q,
      Q => B_binary_reg(12)
    );
  B_binary_reg_11 : FDE
    port map (
      C => clock_BUFGP_14,
      CE => B_n0071_inv,
      D => B_binary_reg_13_binary_reg_13_mux_9_OUT_11_Q,
      Q => B_binary_reg(11)
    );
  B_binary_reg_10 : FDE
    port map (
      C => clock_BUFGP_14,
      CE => B_n0071_inv,
      D => B_binary_reg_13_binary_reg_13_mux_9_OUT_10_Q,
      Q => B_binary_reg(10)
    );
  B_binary_reg_9 : FDE
    port map (
      C => clock_BUFGP_14,
      CE => B_n0071_inv,
      D => B_binary_reg_13_binary_reg_13_mux_9_OUT_9_Q,
      Q => B_binary_reg(9)
    );
  B_binary_reg_8 : FDE
    port map (
      C => clock_BUFGP_14,
      CE => B_n0071_inv,
      D => B_binary_reg_13_binary_reg_13_mux_9_OUT_8_Q,
      Q => B_binary_reg(8)
    );
  B_binary_reg_7 : FDE
    port map (
      C => clock_BUFGP_14,
      CE => B_n0071_inv,
      D => B_binary_reg_13_binary_reg_13_mux_9_OUT_7_Q,
      Q => B_binary_reg(7)
    );
  B_binary_reg_6 : FDE
    port map (
      C => clock_BUFGP_14,
      CE => B_n0071_inv,
      D => B_binary_reg_13_binary_reg_13_mux_9_OUT_6_Q,
      Q => B_binary_reg(6)
    );
  B_binary_reg_5 : FDE
    port map (
      C => clock_BUFGP_14,
      CE => B_n0071_inv,
      D => B_binary_reg_13_binary_reg_13_mux_9_OUT_5_Q,
      Q => B_binary_reg(5)
    );
  B_binary_reg_4 : FDE
    port map (
      C => clock_BUFGP_14,
      CE => B_n0071_inv,
      D => B_binary_reg_13_binary_reg_13_mux_9_OUT_4_Q,
      Q => B_binary_reg(4)
    );
  B_binary_reg_3 : FDE
    port map (
      C => clock_BUFGP_14,
      CE => B_n0071_inv,
      D => B_binary_reg_13_binary_reg_13_mux_9_OUT_3_Q,
      Q => B_binary_reg(3)
    );
  B_binary_reg_2 : FDE
    port map (
      C => clock_BUFGP_14,
      CE => B_n0071_inv,
      D => B_binary_reg_13_binary_reg_13_mux_9_OUT_2_Q,
      Q => B_binary_reg(2)
    );
  B_binary_reg_1 : FDE
    port map (
      C => clock_BUFGP_14,
      CE => B_n0071_inv,
      D => B_binary_reg_13_binary_reg_13_mux_9_OUT_1_Q,
      Q => B_binary_reg(1)
    );
  B_binary_reg_0 : FDE
    port map (
      C => clock_BUFGP_14,
      CE => B_n0071_inv,
      D => B_binary_reg_13_binary_reg_13_mux_9_OUT_0_Q,
      Q => B_binary_reg(0)
    );
  B_bcd_digits_4_digit_0_bcd_3 : FDCE
    port map (
      C => clock_BUFGP_14,
      CE => B_state_68,
      CLR => B_bcd_digits_1_digit_0_reset_n_inv,
      D => B_bcd_digits_4_digit_0_bcd_2_GND_25_o_mux_0_OUT_3_Q,
      Q => B_bcd_digits_4_digit_0_bcd_3_Q
    );
  B_bcd_digits_4_digit_0_bcd_2 : FDCE
    port map (
      C => clock_BUFGP_14,
      CE => B_state_68,
      CLR => B_bcd_digits_1_digit_0_reset_n_inv,
      D => B_bcd_digits_4_digit_0_bcd_2_GND_25_o_mux_0_OUT_2_Q,
      Q => B_bcd_digits_4_digit_0_bcd_2_Q
    );
  B_bcd_digits_4_digit_0_bcd_1 : FDCE
    port map (
      C => clock_BUFGP_14,
      CE => B_state_68,
      CLR => B_bcd_digits_1_digit_0_reset_n_inv,
      D => B_bcd_digits_4_digit_0_bcd_2_GND_25_o_mux_0_OUT_1_Q,
      Q => B_bcd_digits_4_digit_0_bcd_1_Q
    );
  B_bcd_digits_4_digit_0_bcd_0 : FDCE
    port map (
      C => clock_BUFGP_14,
      CE => B_state_68,
      CLR => B_bcd_digits_1_digit_0_reset_n_inv,
      D => B_bcd_digits_4_digit_0_bcd_2_GND_25_o_mux_0_OUT_0_Q,
      Q => B_bcd_digits_4_digit_0_bcd_0_Q
    );
  B_bcd_digits_4_digit_0_prev_ena : FDC
    port map (
      C => clock_BUFGP_14,
      CLR => B_bcd_digits_1_digit_0_reset_n_inv,
      D => B_state_68,
      Q => B_bcd_digits_4_digit_0_prev_ena_138
    );
  B_bcd_digits_3_digit_0_bcd_3 : FDCE
    port map (
      C => clock_BUFGP_14,
      CE => B_state_68,
      CLR => B_bcd_digits_1_digit_0_reset_n_inv,
      D => B_bcd_digits_3_digit_0_bcd_2_GND_25_o_mux_0_OUT_3_Q,
      Q => B_bcd_digits_3_digit_0_bcd_3_Q
    );
  B_bcd_digits_3_digit_0_bcd_2 : FDCE
    port map (
      C => clock_BUFGP_14,
      CE => B_state_68,
      CLR => B_bcd_digits_1_digit_0_reset_n_inv,
      D => B_bcd_digits_3_digit_0_bcd_2_GND_25_o_mux_0_OUT_2_Q,
      Q => B_bcd_digits_3_digit_0_bcd_2_Q
    );
  B_bcd_digits_3_digit_0_bcd_1 : FDCE
    port map (
      C => clock_BUFGP_14,
      CE => B_state_68,
      CLR => B_bcd_digits_1_digit_0_reset_n_inv,
      D => B_bcd_digits_3_digit_0_bcd_2_GND_25_o_mux_0_OUT_1_Q,
      Q => B_bcd_digits_3_digit_0_bcd_1_Q
    );
  B_bcd_digits_3_digit_0_bcd_0 : FDCE
    port map (
      C => clock_BUFGP_14,
      CE => B_state_68,
      CLR => B_bcd_digits_1_digit_0_reset_n_inv,
      D => B_bcd_digits_3_digit_0_bcd_2_GND_25_o_mux_0_OUT_0_Q,
      Q => B_bcd_digits_3_digit_0_bcd_0_Q
    );
  B_bcd_digits_2_digit_0_bcd_3 : FDCE
    port map (
      C => clock_BUFGP_14,
      CE => B_state_68,
      CLR => B_bcd_digits_1_digit_0_reset_n_inv,
      D => B_bcd_digits_2_digit_0_bcd_2_GND_25_o_mux_0_OUT_3_Q,
      Q => B_bcd_digits_2_digit_0_bcd_3_Q
    );
  B_bcd_digits_2_digit_0_bcd_2 : FDCE
    port map (
      C => clock_BUFGP_14,
      CE => B_state_68,
      CLR => B_bcd_digits_1_digit_0_reset_n_inv,
      D => B_bcd_digits_2_digit_0_bcd_2_GND_25_o_mux_0_OUT_2_Q,
      Q => B_bcd_digits_2_digit_0_bcd_2_Q
    );
  B_bcd_digits_2_digit_0_bcd_1 : FDCE
    port map (
      C => clock_BUFGP_14,
      CE => B_state_68,
      CLR => B_bcd_digits_1_digit_0_reset_n_inv,
      D => B_bcd_digits_2_digit_0_bcd_2_GND_25_o_mux_0_OUT_1_Q,
      Q => B_bcd_digits_2_digit_0_bcd_1_Q
    );
  B_bcd_digits_2_digit_0_bcd_0 : FDCE
    port map (
      C => clock_BUFGP_14,
      CE => B_state_68,
      CLR => B_bcd_digits_1_digit_0_reset_n_inv,
      D => B_bcd_digits_2_digit_0_bcd_2_GND_25_o_mux_0_OUT_0_Q,
      Q => B_bcd_digits_2_digit_0_bcd_0_Q
    );
  B_bcd_digits_1_digit_0_bcd_3 : FDCE
    port map (
      C => clock_BUFGP_14,
      CE => B_state_68,
      CLR => B_bcd_digits_1_digit_0_reset_n_inv,
      D => B_bcd_digits_1_digit_0_bcd_2_GND_25_o_mux_0_OUT_3_Q,
      Q => B_bcd_digits_1_digit_0_bcd_3_Q
    );
  B_bcd_digits_1_digit_0_bcd_2 : FDCE
    port map (
      C => clock_BUFGP_14,
      CE => B_state_68,
      CLR => B_bcd_digits_1_digit_0_reset_n_inv,
      D => B_bcd_digits_1_digit_0_bcd_2_GND_25_o_mux_0_OUT_2_Q,
      Q => B_bcd_digits_1_digit_0_bcd_2_Q
    );
  B_bcd_digits_1_digit_0_bcd_1 : FDCE
    port map (
      C => clock_BUFGP_14,
      CE => B_state_68,
      CLR => B_bcd_digits_1_digit_0_reset_n_inv,
      D => B_bcd_digits_1_digit_0_bcd_2_GND_25_o_mux_0_OUT_1_Q,
      Q => B_bcd_digits_1_digit_0_bcd_1_Q
    );
  B_bcd_digits_1_digit_0_bcd_0 : FDCE
    port map (
      C => clock_BUFGP_14,
      CE => B_state_68,
      CLR => B_bcd_digits_1_digit_0_reset_n_inv,
      D => B_bcd_digits_1_digit_0_bcd_2_GND_25_o_mux_0_OUT_0_Q,
      Q => B_bcd_digits_1_digit_0_bcd_0_Q
    );
  D_anodes_instance_n0016_0_1 : LUT2
    generic map(
      INIT => X"E"
    )
    port map (
      I0 => D_counter_count(1),
      I1 => D_counter_count(0),
      O => anodes_0_OBUF_42
    );
  D_anodes_instance_n0016_1_1 : LUT2
    generic map(
      INIT => X"D"
    )
    port map (
      I0 => D_counter_count(0),
      I1 => D_counter_count(1),
      O => anodes_1_OBUF_41
    );
  D_anodes_instance_n0016_2_1 : LUT2
    generic map(
      INIT => X"D"
    )
    port map (
      I0 => D_counter_count(1),
      I1 => D_counter_count(0),
      O => anodes_2_OBUF_40
    );
  D_counter_Mmux_count_1_GND_10_o_mux_3_OUT21 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => D_counter_count(1),
      I1 => D_counter_count(0),
      O => D_counter_count_1_GND_10_o_mux_3_OUT_1_Q
    );
  D_anodes_instance_n0016_3_1 : LUT2
    generic map(
      INIT => X"7"
    )
    port map (
      I0 => D_counter_count(0),
      I1 => D_counter_count(1),
      O => anodes_3_OBUF_39
    );
  D_clk_filter_Mcount_divisore_frequenza_count_xor_1_11 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => D_clk_filter_divisore_frequenza_count(0),
      I1 => D_clk_filter_divisore_frequenza_count(1),
      O => D_clk_filter_Mcount_divisore_frequenza_count1
    );
  D_cathodes_instance_Mram_cathodes_for_digit111 : LUT4
    generic map(
      INIT => X"E228"
    )
    port map (
      I0 => D_cathodes_instance_nibble(2),
      I1 => D_cathodes_instance_nibble(0),
      I2 => D_cathodes_instance_nibble(1),
      I3 => D_cathodes_instance_nibble(3),
      O => cathodes_1_OBUF_37
    );
  D_cathodes_instance_Mram_cathodes_for_digit61 : LUT4
    generic map(
      INIT => X"0941"
    )
    port map (
      I0 => D_cathodes_instance_nibble(1),
      I1 => D_cathodes_instance_nibble(2),
      I2 => D_cathodes_instance_nibble(3),
      I3 => D_cathodes_instance_nibble(0),
      O => cathodes_6_OBUF_32
    );
  D_cathodes_instance_Mram_cathodes_for_digit41 : LUT4
    generic map(
      INIT => X"02BA"
    )
    port map (
      I0 => D_cathodes_instance_nibble(0),
      I1 => D_cathodes_instance_nibble(1),
      I2 => D_cathodes_instance_nibble(2),
      I3 => D_cathodes_instance_nibble(3),
      O => cathodes_4_OBUF_34
    );
  D_cathodes_instance_Mmux_nibble_0_11 : LUT6
    generic map(
      INIT => X"FD75B931EC64A820"
    )
    port map (
      I0 => D_counter_count(0),
      I1 => D_counter_count(1),
      I2 => B_bcd(4),
      I3 => B_bcd(12),
      I4 => B_bcd(8),
      I5 => B_bcd(0),
      O => D_cathodes_instance_nibble(0)
    );
  D_cathodes_instance_Mmux_nibble_1_11 : LUT6
    generic map(
      INIT => X"FD75B931EC64A820"
    )
    port map (
      I0 => D_counter_count(0),
      I1 => D_counter_count(1),
      I2 => B_bcd(5),
      I3 => B_bcd(13),
      I4 => B_bcd(9),
      I5 => B_bcd(1),
      O => D_cathodes_instance_nibble(1)
    );
  D_cathodes_instance_Mmux_nibble_2_11 : LUT6
    generic map(
      INIT => X"FD75B931EC64A820"
    )
    port map (
      I0 => D_counter_count(0),
      I1 => D_counter_count(1),
      I2 => B_bcd(6),
      I3 => B_bcd(14),
      I4 => B_bcd(10),
      I5 => B_bcd(2),
      O => D_cathodes_instance_nibble(2)
    );
  D_cathodes_instance_Mmux_nibble_3_11 : LUT6
    generic map(
      INIT => X"FD75B931EC64A820"
    )
    port map (
      I0 => D_counter_count(0),
      I1 => D_counter_count(1),
      I2 => B_bcd(7),
      I3 => B_bcd(15),
      I4 => B_bcd(11),
      I5 => B_bcd(3),
      O => D_cathodes_instance_nibble(3)
    );
  D_clk_filter_Mcount_divisore_frequenza_count_xor_4_11 : LUT5
    generic map(
      INIT => X"6AAAAA2A"
    )
    port map (
      I0 => D_clk_filter_divisore_frequenza_count(4),
      I1 => D_clk_filter_divisore_frequenza_count(0),
      I2 => D_clk_filter_divisore_frequenza_count(1),
      I3 => D_clk_filter_divisore_frequenza_count(2),
      I4 => D_clk_filter_divisore_frequenza_count(3),
      O => D_clk_filter_Mcount_divisore_frequenza_count4
    );
  D_clk_filter_Mcount_divisore_frequenza_count_xor_3_11 : LUT4
    generic map(
      INIT => X"6AAA"
    )
    port map (
      I0 => D_clk_filter_divisore_frequenza_count(3),
      I1 => D_clk_filter_divisore_frequenza_count(0),
      I2 => D_clk_filter_divisore_frequenza_count(1),
      I3 => D_clk_filter_divisore_frequenza_count(2),
      O => D_clk_filter_Mcount_divisore_frequenza_count3
    );
  D_clk_filter_Mcount_divisore_frequenza_count_xor_2_11 : LUT5
    generic map(
      INIT => X"78787078"
    )
    port map (
      I0 => D_clk_filter_divisore_frequenza_count(0),
      I1 => D_clk_filter_divisore_frequenza_count(1),
      I2 => D_clk_filter_divisore_frequenza_count(2),
      I3 => D_clk_filter_divisore_frequenza_count(4),
      I4 => D_clk_filter_divisore_frequenza_count(3),
      O => D_clk_filter_Mcount_divisore_frequenza_count2
    );
  D_clk_filter_PWR_11_o_divisore_frequenza_count_4_equal_1_o_4_1 : LUT5
    generic map(
      INIT => X"00400000"
    )
    port map (
      I0 => D_clk_filter_divisore_frequenza_count(3),
      I1 => D_clk_filter_divisore_frequenza_count(4),
      I2 => D_clk_filter_divisore_frequenza_count(0),
      I3 => D_clk_filter_divisore_frequenza_count(2),
      I4 => D_clk_filter_divisore_frequenza_count(1),
      O => D_clk_filter_PWR_11_o_divisore_frequenza_count_4_equal_1_o
    );
  D_cathodes_instance_Mram_cathodes_for_digit11 : LUT4
    generic map(
      INIT => X"2812"
    )
    port map (
      I0 => D_cathodes_instance_nibble(0),
      I1 => D_cathodes_instance_nibble(1),
      I2 => D_cathodes_instance_nibble(2),
      I3 => D_cathodes_instance_nibble(3),
      O => cathodes_0_OBUF_38
    );
  D_cathodes_instance_Mram_cathodes_for_digit31 : LUT4
    generic map(
      INIT => X"8294"
    )
    port map (
      I0 => D_cathodes_instance_nibble(1),
      I1 => D_cathodes_instance_nibble(2),
      I2 => D_cathodes_instance_nibble(0),
      I3 => D_cathodes_instance_nibble(3),
      O => cathodes_3_OBUF_35
    );
  D_cathodes_instance_Mram_cathodes_for_digit51 : LUT4
    generic map(
      INIT => X"6054"
    )
    port map (
      I0 => D_cathodes_instance_nibble(3),
      I1 => D_cathodes_instance_nibble(1),
      I2 => D_cathodes_instance_nibble(0),
      I3 => D_cathodes_instance_nibble(2),
      O => cathodes_5_OBUF_33
    );
  D_cathodes_instance_Mram_cathodes_for_digit21 : LUT4
    generic map(
      INIT => X"D004"
    )
    port map (
      I0 => D_cathodes_instance_nibble(0),
      I1 => D_cathodes_instance_nibble(1),
      I2 => D_cathodes_instance_nibble(2),
      I3 => D_cathodes_instance_nibble(3),
      O => cathodes_2_OBUF_36
    );
  B_Mcount_bit_count11 : LUT3
    generic map(
      INIT => X"28"
    )
    port map (
      I0 => B_state_68,
      I1 => B_bit_count(0),
      I2 => B_bit_count(1),
      O => B_Mcount_bit_count1
    );
  B_Mcount_bit_count21 : LUT4
    generic map(
      INIT => X"2888"
    )
    port map (
      I0 => B_state_68,
      I1 => B_bit_count(2),
      I2 => B_bit_count(0),
      I3 => B_bit_count(1),
      O => B_Mcount_bit_count2
    );
  B_Mcount_bit_count31 : LUT5
    generic map(
      INIT => X"28888888"
    )
    port map (
      I0 => B_state_68,
      I1 => B_bit_count(3),
      I2 => B_bit_count(2),
      I3 => B_bit_count(0),
      I4 => B_bit_count(1),
      O => B_Mcount_bit_count3
    );
  B_bit_count_3_ena_MUX_74_o11 : LUT6
    generic map(
      INIT => X"3FFFFFFFAAAAAAAA"
    )
    port map (
      I0 => load_IBUF_15,
      I1 => B_bit_count(3),
      I2 => B_bit_count(2),
      I3 => B_bit_count(1),
      I4 => B_bit_count(0),
      I5 => B_state_68,
      O => B_bit_count_3_ena_MUX_74_o
    );
  B_n0076_inv11 : LUT6
    generic map(
      INIT => X"1555555500000000"
    )
    port map (
      I0 => B_bcd_digits_1_digit_0_reset_n_inv,
      I1 => B_bit_count(3),
      I2 => B_bit_count(2),
      I3 => B_bit_count(1),
      I4 => B_bit_count(0),
      I5 => B_state_68,
      O => B_n0076_inv
    );
  B_Mcount_bit_count_xor_0_11 : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => B_state_68,
      I1 => B_bit_count(0),
      O => B_Mcount_bit_count
    );
  B_Mmux_binary_reg_13_binary_reg_13_mux_9_OUT15 : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => switches_0_IBUF_13,
      I1 => B_state_68,
      O => B_binary_reg_13_binary_reg_13_mux_9_OUT_0_Q
    );
  B_Mmux_binary_reg_13_binary_reg_13_mux_9_OUT21 : LUT3
    generic map(
      INIT => X"D8"
    )
    port map (
      I0 => B_state_68,
      I1 => B_binary_reg(9),
      I2 => switches_10_IBUF_3,
      O => B_binary_reg_13_binary_reg_13_mux_9_OUT_10_Q
    );
  B_Mmux_binary_reg_13_binary_reg_13_mux_9_OUT31 : LUT3
    generic map(
      INIT => X"D8"
    )
    port map (
      I0 => B_state_68,
      I1 => B_binary_reg(10),
      I2 => switches_11_IBUF_2,
      O => B_binary_reg_13_binary_reg_13_mux_9_OUT_11_Q
    );
  B_Mmux_binary_reg_13_binary_reg_13_mux_9_OUT41 : LUT3
    generic map(
      INIT => X"D8"
    )
    port map (
      I0 => B_state_68,
      I1 => B_binary_reg(11),
      I2 => switches_12_IBUF_1,
      O => B_binary_reg_13_binary_reg_13_mux_9_OUT_12_Q
    );
  B_Mmux_binary_reg_13_binary_reg_13_mux_9_OUT51 : LUT3
    generic map(
      INIT => X"D8"
    )
    port map (
      I0 => B_state_68,
      I1 => B_binary_reg(12),
      I2 => switches_13_IBUF_0,
      O => B_binary_reg_13_binary_reg_13_mux_9_OUT_13_Q
    );
  B_Mmux_binary_reg_13_binary_reg_13_mux_9_OUT61 : LUT3
    generic map(
      INIT => X"D8"
    )
    port map (
      I0 => B_state_68,
      I1 => B_binary_reg(0),
      I2 => switches_1_IBUF_12,
      O => B_binary_reg_13_binary_reg_13_mux_9_OUT_1_Q
    );
  B_Mmux_binary_reg_13_binary_reg_13_mux_9_OUT71 : LUT3
    generic map(
      INIT => X"D8"
    )
    port map (
      I0 => B_state_68,
      I1 => B_binary_reg(1),
      I2 => switches_2_IBUF_11,
      O => B_binary_reg_13_binary_reg_13_mux_9_OUT_2_Q
    );
  B_Mmux_binary_reg_13_binary_reg_13_mux_9_OUT81 : LUT3
    generic map(
      INIT => X"D8"
    )
    port map (
      I0 => B_state_68,
      I1 => B_binary_reg(2),
      I2 => switches_3_IBUF_10,
      O => B_binary_reg_13_binary_reg_13_mux_9_OUT_3_Q
    );
  B_Mmux_binary_reg_13_binary_reg_13_mux_9_OUT91 : LUT3
    generic map(
      INIT => X"D8"
    )
    port map (
      I0 => B_state_68,
      I1 => B_binary_reg(3),
      I2 => switches_4_IBUF_9,
      O => B_binary_reg_13_binary_reg_13_mux_9_OUT_4_Q
    );
  B_Mmux_binary_reg_13_binary_reg_13_mux_9_OUT101 : LUT3
    generic map(
      INIT => X"D8"
    )
    port map (
      I0 => B_state_68,
      I1 => B_binary_reg(4),
      I2 => switches_5_IBUF_8,
      O => B_binary_reg_13_binary_reg_13_mux_9_OUT_5_Q
    );
  B_Mmux_binary_reg_13_binary_reg_13_mux_9_OUT111 : LUT3
    generic map(
      INIT => X"D8"
    )
    port map (
      I0 => B_state_68,
      I1 => B_binary_reg(5),
      I2 => switches_6_IBUF_7,
      O => B_binary_reg_13_binary_reg_13_mux_9_OUT_6_Q
    );
  B_Mmux_binary_reg_13_binary_reg_13_mux_9_OUT121 : LUT3
    generic map(
      INIT => X"D8"
    )
    port map (
      I0 => B_state_68,
      I1 => B_binary_reg(6),
      I2 => switches_7_IBUF_6,
      O => B_binary_reg_13_binary_reg_13_mux_9_OUT_7_Q
    );
  B_Mmux_binary_reg_13_binary_reg_13_mux_9_OUT131 : LUT3
    generic map(
      INIT => X"D8"
    )
    port map (
      I0 => B_state_68,
      I1 => B_binary_reg(7),
      I2 => switches_8_IBUF_5,
      O => B_binary_reg_13_binary_reg_13_mux_9_OUT_8_Q
    );
  B_Mmux_binary_reg_13_binary_reg_13_mux_9_OUT141 : LUT3
    generic map(
      INIT => X"D8"
    )
    port map (
      I0 => B_state_68,
      I1 => B_binary_reg(8),
      I2 => switches_9_IBUF_4,
      O => B_binary_reg_13_binary_reg_13_mux_9_OUT_9_Q
    );
  B_bcd_digits_4_digit_0_Mmux_bcd_2_GND_25_o_mux_0_OUT41 : LUT5
    generic map(
      INIT => X"80828080"
    )
    port map (
      I0 => B_bcd_digits_4_digit_0_prev_ena_138,
      I1 => B_bcd_digits_4_digit_0_bcd_0_Q,
      I2 => B_bcd_digits_4_digit_0_bcd_3_Q,
      I3 => B_bcd_digits_4_digit_0_bcd_1_Q,
      I4 => B_bcd_digits_4_digit_0_bcd_2_Q,
      O => B_bcd_digits_4_digit_0_bcd_2_GND_25_o_mux_0_OUT_3_Q
    );
  B_bcd_digits_4_digit_0_Mmux_bcd_2_GND_25_o_mux_0_OUT31 : LUT5
    generic map(
      INIT => X"82808288"
    )
    port map (
      I0 => B_bcd_digits_4_digit_0_prev_ena_138,
      I1 => B_bcd_digits_4_digit_0_bcd_1_Q,
      I2 => B_bcd_digits_4_digit_0_bcd_0_Q,
      I3 => B_bcd_digits_4_digit_0_bcd_3_Q,
      I4 => B_bcd_digits_4_digit_0_bcd_2_Q,
      O => B_bcd_digits_4_digit_0_bcd_2_GND_25_o_mux_0_OUT_2_Q
    );
  B_bcd_digits_4_digit_0_Mmux_bcd_2_GND_25_o_mux_0_OUT21 : LUT5
    generic map(
      INIT => X"22282028"
    )
    port map (
      I0 => B_bcd_digits_4_digit_0_prev_ena_138,
      I1 => B_bcd_digits_4_digit_0_bcd_0_Q,
      I2 => B_bcd_digits_4_digit_0_bcd_3_Q,
      I3 => B_bcd_digits_4_digit_0_bcd_2_Q,
      I4 => B_bcd_digits_4_digit_0_bcd_1_Q,
      O => B_bcd_digits_4_digit_0_bcd_2_GND_25_o_mux_0_OUT_1_Q
    );
  B_bcd_digits_4_digit_0_Mmux_bcd_2_GND_25_o_mux_0_OUT11 : LUT5
    generic map(
      INIT => X"AAAA8880"
    )
    port map (
      I0 => B_bcd_digits_4_digit_0_prev_ena_138,
      I1 => B_bcd_digits_3_digit_0_bcd_2_Q,
      I2 => B_bcd_digits_3_digit_0_bcd_0_Q,
      I3 => B_bcd_digits_3_digit_0_bcd_1_Q,
      I4 => B_bcd_digits_3_digit_0_bcd_3_Q,
      O => B_bcd_digits_4_digit_0_bcd_2_GND_25_o_mux_0_OUT_0_Q
    );
  B_bcd_digits_3_digit_0_Mmux_bcd_2_GND_25_o_mux_0_OUT41 : LUT5
    generic map(
      INIT => X"80828080"
    )
    port map (
      I0 => B_bcd_digits_4_digit_0_prev_ena_138,
      I1 => B_bcd_digits_3_digit_0_bcd_0_Q,
      I2 => B_bcd_digits_3_digit_0_bcd_3_Q,
      I3 => B_bcd_digits_3_digit_0_bcd_1_Q,
      I4 => B_bcd_digits_3_digit_0_bcd_2_Q,
      O => B_bcd_digits_3_digit_0_bcd_2_GND_25_o_mux_0_OUT_3_Q
    );
  B_bcd_digits_3_digit_0_Mmux_bcd_2_GND_25_o_mux_0_OUT31 : LUT5
    generic map(
      INIT => X"82808288"
    )
    port map (
      I0 => B_bcd_digits_4_digit_0_prev_ena_138,
      I1 => B_bcd_digits_3_digit_0_bcd_1_Q,
      I2 => B_bcd_digits_3_digit_0_bcd_0_Q,
      I3 => B_bcd_digits_3_digit_0_bcd_3_Q,
      I4 => B_bcd_digits_3_digit_0_bcd_2_Q,
      O => B_bcd_digits_3_digit_0_bcd_2_GND_25_o_mux_0_OUT_2_Q
    );
  B_bcd_digits_3_digit_0_Mmux_bcd_2_GND_25_o_mux_0_OUT21 : LUT5
    generic map(
      INIT => X"22282028"
    )
    port map (
      I0 => B_bcd_digits_4_digit_0_prev_ena_138,
      I1 => B_bcd_digits_3_digit_0_bcd_0_Q,
      I2 => B_bcd_digits_3_digit_0_bcd_3_Q,
      I3 => B_bcd_digits_3_digit_0_bcd_2_Q,
      I4 => B_bcd_digits_3_digit_0_bcd_1_Q,
      O => B_bcd_digits_3_digit_0_bcd_2_GND_25_o_mux_0_OUT_1_Q
    );
  B_bcd_digits_3_digit_0_Mmux_bcd_2_GND_25_o_mux_0_OUT11 : LUT5
    generic map(
      INIT => X"AAAA8880"
    )
    port map (
      I0 => B_bcd_digits_4_digit_0_prev_ena_138,
      I1 => B_bcd_digits_2_digit_0_bcd_2_Q,
      I2 => B_bcd_digits_2_digit_0_bcd_0_Q,
      I3 => B_bcd_digits_2_digit_0_bcd_1_Q,
      I4 => B_bcd_digits_2_digit_0_bcd_3_Q,
      O => B_bcd_digits_3_digit_0_bcd_2_GND_25_o_mux_0_OUT_0_Q
    );
  B_bcd_digits_2_digit_0_Mmux_bcd_2_GND_25_o_mux_0_OUT41 : LUT5
    generic map(
      INIT => X"80828080"
    )
    port map (
      I0 => B_bcd_digits_4_digit_0_prev_ena_138,
      I1 => B_bcd_digits_2_digit_0_bcd_0_Q,
      I2 => B_bcd_digits_2_digit_0_bcd_3_Q,
      I3 => B_bcd_digits_2_digit_0_bcd_1_Q,
      I4 => B_bcd_digits_2_digit_0_bcd_2_Q,
      O => B_bcd_digits_2_digit_0_bcd_2_GND_25_o_mux_0_OUT_3_Q
    );
  B_bcd_digits_2_digit_0_Mmux_bcd_2_GND_25_o_mux_0_OUT31 : LUT5
    generic map(
      INIT => X"82808288"
    )
    port map (
      I0 => B_bcd_digits_4_digit_0_prev_ena_138,
      I1 => B_bcd_digits_2_digit_0_bcd_1_Q,
      I2 => B_bcd_digits_2_digit_0_bcd_0_Q,
      I3 => B_bcd_digits_2_digit_0_bcd_3_Q,
      I4 => B_bcd_digits_2_digit_0_bcd_2_Q,
      O => B_bcd_digits_2_digit_0_bcd_2_GND_25_o_mux_0_OUT_2_Q
    );
  B_bcd_digits_2_digit_0_Mmux_bcd_2_GND_25_o_mux_0_OUT21 : LUT5
    generic map(
      INIT => X"22282028"
    )
    port map (
      I0 => B_bcd_digits_4_digit_0_prev_ena_138,
      I1 => B_bcd_digits_2_digit_0_bcd_0_Q,
      I2 => B_bcd_digits_2_digit_0_bcd_3_Q,
      I3 => B_bcd_digits_2_digit_0_bcd_2_Q,
      I4 => B_bcd_digits_2_digit_0_bcd_1_Q,
      O => B_bcd_digits_2_digit_0_bcd_2_GND_25_o_mux_0_OUT_1_Q
    );
  B_bcd_digits_2_digit_0_Mmux_bcd_2_GND_25_o_mux_0_OUT11 : LUT5
    generic map(
      INIT => X"AAAA8880"
    )
    port map (
      I0 => B_bcd_digits_4_digit_0_prev_ena_138,
      I1 => B_bcd_digits_1_digit_0_bcd_2_Q,
      I2 => B_bcd_digits_1_digit_0_bcd_0_Q,
      I3 => B_bcd_digits_1_digit_0_bcd_1_Q,
      I4 => B_bcd_digits_1_digit_0_bcd_3_Q,
      O => B_bcd_digits_2_digit_0_bcd_2_GND_25_o_mux_0_OUT_0_Q
    );
  B_bcd_digits_1_digit_0_Mmux_bcd_2_GND_25_o_mux_0_OUT41 : LUT5
    generic map(
      INIT => X"80828080"
    )
    port map (
      I0 => B_bcd_digits_4_digit_0_prev_ena_138,
      I1 => B_bcd_digits_1_digit_0_bcd_0_Q,
      I2 => B_bcd_digits_1_digit_0_bcd_3_Q,
      I3 => B_bcd_digits_1_digit_0_bcd_1_Q,
      I4 => B_bcd_digits_1_digit_0_bcd_2_Q,
      O => B_bcd_digits_1_digit_0_bcd_2_GND_25_o_mux_0_OUT_3_Q
    );
  B_bcd_digits_1_digit_0_Mmux_bcd_2_GND_25_o_mux_0_OUT31 : LUT5
    generic map(
      INIT => X"82808288"
    )
    port map (
      I0 => B_bcd_digits_4_digit_0_prev_ena_138,
      I1 => B_bcd_digits_1_digit_0_bcd_1_Q,
      I2 => B_bcd_digits_1_digit_0_bcd_0_Q,
      I3 => B_bcd_digits_1_digit_0_bcd_3_Q,
      I4 => B_bcd_digits_1_digit_0_bcd_2_Q,
      O => B_bcd_digits_1_digit_0_bcd_2_GND_25_o_mux_0_OUT_2_Q
    );
  B_bcd_digits_1_digit_0_Mmux_bcd_2_GND_25_o_mux_0_OUT21 : LUT5
    generic map(
      INIT => X"22282028"
    )
    port map (
      I0 => B_bcd_digits_4_digit_0_prev_ena_138,
      I1 => B_bcd_digits_1_digit_0_bcd_0_Q,
      I2 => B_bcd_digits_1_digit_0_bcd_3_Q,
      I3 => B_bcd_digits_1_digit_0_bcd_2_Q,
      I4 => B_bcd_digits_1_digit_0_bcd_1_Q,
      O => B_bcd_digits_1_digit_0_bcd_2_GND_25_o_mux_0_OUT_1_Q
    );
  B_bcd_digits_1_digit_0_Mmux_bcd_2_GND_25_o_mux_0_OUT11 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => B_bcd_digits_4_digit_0_prev_ena_138,
      I1 => B_converter_inputs_0_72,
      O => B_bcd_digits_1_digit_0_bcd_2_GND_25_o_mux_0_OUT_0_Q
    );
  B_n0071_inv1_SW0 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => B_bit_count(1),
      I1 => B_bit_count(0),
      O => N01
    );
  B_n0071_inv1 : LUT6
    generic map(
      INIT => X"0333222233332222"
    )
    port map (
      I0 => load_IBUF_15,
      I1 => B_bcd_digits_1_digit_0_reset_n_inv,
      I2 => B_bit_count(3),
      I3 => B_bit_count(2),
      I4 => B_state_68,
      I5 => N01,
      O => B_n0071_inv
    );
  switches_13_IBUF : IBUF
    port map (
      I => switches(13),
      O => switches_13_IBUF_0
    );
  switches_12_IBUF : IBUF
    port map (
      I => switches(12),
      O => switches_12_IBUF_1
    );
  switches_11_IBUF : IBUF
    port map (
      I => switches(11),
      O => switches_11_IBUF_2
    );
  switches_10_IBUF : IBUF
    port map (
      I => switches(10),
      O => switches_10_IBUF_3
    );
  switches_9_IBUF : IBUF
    port map (
      I => switches(9),
      O => switches_9_IBUF_4
    );
  switches_8_IBUF : IBUF
    port map (
      I => switches(8),
      O => switches_8_IBUF_5
    );
  switches_7_IBUF : IBUF
    port map (
      I => switches(7),
      O => switches_7_IBUF_6
    );
  switches_6_IBUF : IBUF
    port map (
      I => switches(6),
      O => switches_6_IBUF_7
    );
  switches_5_IBUF : IBUF
    port map (
      I => switches(5),
      O => switches_5_IBUF_8
    );
  switches_4_IBUF : IBUF
    port map (
      I => switches(4),
      O => switches_4_IBUF_9
    );
  switches_3_IBUF : IBUF
    port map (
      I => switches(3),
      O => switches_3_IBUF_10
    );
  switches_2_IBUF : IBUF
    port map (
      I => switches(2),
      O => switches_2_IBUF_11
    );
  switches_1_IBUF : IBUF
    port map (
      I => switches(1),
      O => switches_1_IBUF_12
    );
  switches_0_IBUF : IBUF
    port map (
      I => switches(0),
      O => switches_0_IBUF_13
    );
  reset_IBUF : IBUF
    port map (
      I => reset,
      O => B_bcd_digits_1_digit_0_reset_n_inv
    );
  load_IBUF : IBUF
    port map (
      I => load,
      O => load_IBUF_15
    );
  anodes_7_OBUF : OBUF
    port map (
      I => anodes_7_OBUF_43,
      O => anodes(7)
    );
  anodes_6_OBUF : OBUF
    port map (
      I => anodes_7_OBUF_43,
      O => anodes(6)
    );
  anodes_5_OBUF : OBUF
    port map (
      I => anodes_7_OBUF_43,
      O => anodes(5)
    );
  anodes_4_OBUF : OBUF
    port map (
      I => anodes_7_OBUF_43,
      O => anodes(4)
    );
  anodes_3_OBUF : OBUF
    port map (
      I => anodes_3_OBUF_39,
      O => anodes(3)
    );
  anodes_2_OBUF : OBUF
    port map (
      I => anodes_2_OBUF_40,
      O => anodes(2)
    );
  anodes_1_OBUF : OBUF
    port map (
      I => anodes_1_OBUF_41,
      O => anodes(1)
    );
  anodes_0_OBUF : OBUF
    port map (
      I => anodes_0_OBUF_42,
      O => anodes(0)
    );
  cathodes_7_OBUF : OBUF
    port map (
      I => anodes_7_OBUF_43,
      O => cathodes(7)
    );
  cathodes_6_OBUF : OBUF
    port map (
      I => cathodes_6_OBUF_32,
      O => cathodes(6)
    );
  cathodes_5_OBUF : OBUF
    port map (
      I => cathodes_5_OBUF_33,
      O => cathodes(5)
    );
  cathodes_4_OBUF : OBUF
    port map (
      I => cathodes_4_OBUF_34,
      O => cathodes(4)
    );
  cathodes_3_OBUF : OBUF
    port map (
      I => cathodes_3_OBUF_35,
      O => cathodes(3)
    );
  cathodes_2_OBUF : OBUF
    port map (
      I => cathodes_2_OBUF_36,
      O => cathodes(2)
    );
  cathodes_1_OBUF : OBUF
    port map (
      I => cathodes_1_OBUF_37,
      O => cathodes(1)
    );
  cathodes_0_OBUF : OBUF
    port map (
      I => cathodes_0_OBUF_38,
      O => cathodes(0)
    );
  B_bcd_0_dpot : LUT6
    generic map(
      INIT => X"EAAAAAAA2AAAAAAA"
    )
    port map (
      I0 => B_bcd(0),
      I1 => B_bit_count(0),
      I2 => B_bit_count(1),
      I3 => B_bit_count(2),
      I4 => B_bit_count(3),
      I5 => B_bcd_digits_1_digit_0_bcd_0_Q,
      O => B_bcd_0_dpot_173
    );
  B_bcd_1_dpot : LUT6
    generic map(
      INIT => X"EAAAAAAA2AAAAAAA"
    )
    port map (
      I0 => B_bcd(1),
      I1 => B_bit_count(0),
      I2 => B_bit_count(1),
      I3 => B_bit_count(2),
      I4 => B_bit_count(3),
      I5 => B_bcd_digits_1_digit_0_bcd_1_Q,
      O => B_bcd_1_dpot_174
    );
  B_bcd_2_dpot : LUT6
    generic map(
      INIT => X"EAAAAAAA2AAAAAAA"
    )
    port map (
      I0 => B_bcd(2),
      I1 => B_bit_count(0),
      I2 => B_bit_count(1),
      I3 => B_bit_count(2),
      I4 => B_bit_count(3),
      I5 => B_bcd_digits_1_digit_0_bcd_2_Q,
      O => B_bcd_2_dpot_175
    );
  B_bcd_3_dpot : LUT6
    generic map(
      INIT => X"EAAAAAAA2AAAAAAA"
    )
    port map (
      I0 => B_bcd(3),
      I1 => B_bit_count(0),
      I2 => B_bit_count(1),
      I3 => B_bit_count(2),
      I4 => B_bit_count(3),
      I5 => B_bcd_digits_1_digit_0_bcd_3_Q,
      O => B_bcd_3_dpot_176
    );
  B_bcd_4_dpot : LUT6
    generic map(
      INIT => X"EAAAAAAA2AAAAAAA"
    )
    port map (
      I0 => B_bcd(4),
      I1 => B_bit_count(0),
      I2 => B_bit_count(1),
      I3 => B_bit_count(2),
      I4 => B_bit_count(3),
      I5 => B_bcd_digits_2_digit_0_bcd_0_Q,
      O => B_bcd_4_dpot_177
    );
  B_bcd_5_dpot : LUT6
    generic map(
      INIT => X"EAAAAAAA2AAAAAAA"
    )
    port map (
      I0 => B_bcd(5),
      I1 => B_bit_count(0),
      I2 => B_bit_count(1),
      I3 => B_bit_count(2),
      I4 => B_bit_count(3),
      I5 => B_bcd_digits_2_digit_0_bcd_1_Q,
      O => B_bcd_5_dpot_178
    );
  B_bcd_6_dpot : LUT6
    generic map(
      INIT => X"EAAAAAAA2AAAAAAA"
    )
    port map (
      I0 => B_bcd(6),
      I1 => B_bit_count(0),
      I2 => B_bit_count(1),
      I3 => B_bit_count(2),
      I4 => B_bit_count(3),
      I5 => B_bcd_digits_2_digit_0_bcd_2_Q,
      O => B_bcd_6_dpot_179
    );
  B_bcd_7_dpot : LUT6
    generic map(
      INIT => X"EAAAAAAA2AAAAAAA"
    )
    port map (
      I0 => B_bcd(7),
      I1 => B_bit_count(0),
      I2 => B_bit_count(1),
      I3 => B_bit_count(2),
      I4 => B_bit_count(3),
      I5 => B_bcd_digits_2_digit_0_bcd_3_Q,
      O => B_bcd_7_dpot_180
    );
  B_bcd_8_dpot : LUT6
    generic map(
      INIT => X"EAAAAAAA2AAAAAAA"
    )
    port map (
      I0 => B_bcd(8),
      I1 => B_bit_count(0),
      I2 => B_bit_count(1),
      I3 => B_bit_count(2),
      I4 => B_bit_count(3),
      I5 => B_bcd_digits_3_digit_0_bcd_0_Q,
      O => B_bcd_8_dpot_181
    );
  B_bcd_9_dpot : LUT6
    generic map(
      INIT => X"EAAAAAAA2AAAAAAA"
    )
    port map (
      I0 => B_bcd(9),
      I1 => B_bit_count(0),
      I2 => B_bit_count(1),
      I3 => B_bit_count(2),
      I4 => B_bit_count(3),
      I5 => B_bcd_digits_3_digit_0_bcd_1_Q,
      O => B_bcd_9_dpot_182
    );
  B_bcd_10_dpot : LUT6
    generic map(
      INIT => X"EAAAAAAA2AAAAAAA"
    )
    port map (
      I0 => B_bcd(10),
      I1 => B_bit_count(0),
      I2 => B_bit_count(1),
      I3 => B_bit_count(2),
      I4 => B_bit_count(3),
      I5 => B_bcd_digits_3_digit_0_bcd_2_Q,
      O => B_bcd_10_dpot_183
    );
  B_bcd_11_dpot : LUT6
    generic map(
      INIT => X"EAAAAAAA2AAAAAAA"
    )
    port map (
      I0 => B_bcd(11),
      I1 => B_bit_count(0),
      I2 => B_bit_count(1),
      I3 => B_bit_count(2),
      I4 => B_bit_count(3),
      I5 => B_bcd_digits_3_digit_0_bcd_3_Q,
      O => B_bcd_11_dpot_184
    );
  B_bcd_12_dpot : LUT6
    generic map(
      INIT => X"EAAAAAAA2AAAAAAA"
    )
    port map (
      I0 => B_bcd(12),
      I1 => B_bit_count(0),
      I2 => B_bit_count(1),
      I3 => B_bit_count(2),
      I4 => B_bit_count(3),
      I5 => B_bcd_digits_4_digit_0_bcd_0_Q,
      O => B_bcd_12_dpot_185
    );
  B_bcd_13_dpot : LUT6
    generic map(
      INIT => X"EAAAAAAA2AAAAAAA"
    )
    port map (
      I0 => B_bcd(13),
      I1 => B_bit_count(0),
      I2 => B_bit_count(1),
      I3 => B_bit_count(2),
      I4 => B_bit_count(3),
      I5 => B_bcd_digits_4_digit_0_bcd_1_Q,
      O => B_bcd_13_dpot_186
    );
  B_bcd_14_dpot : LUT6
    generic map(
      INIT => X"EAAAAAAA2AAAAAAA"
    )
    port map (
      I0 => B_bcd(14),
      I1 => B_bit_count(0),
      I2 => B_bit_count(1),
      I3 => B_bit_count(2),
      I4 => B_bit_count(3),
      I5 => B_bcd_digits_4_digit_0_bcd_2_Q,
      O => B_bcd_14_dpot_187
    );
  B_bcd_15_dpot : LUT6
    generic map(
      INIT => X"EAAAAAAA2AAAAAAA"
    )
    port map (
      I0 => B_bcd(15),
      I1 => B_bit_count(0),
      I2 => B_bit_count(1),
      I3 => B_bit_count(2),
      I4 => B_bit_count(3),
      I5 => B_bcd_digits_4_digit_0_bcd_3_Q,
      O => B_bcd_15_dpot_188
    );
  clock_BUFGP : BUFGP
    port map (
      I => clock,
      O => clock_BUFGP_14
    );
  D_counter_Mmux_count_1_GND_10_o_mux_3_OUT11_INV_0 : INV
    port map (
      I => D_counter_count(0),
      O => D_counter_count_1_GND_10_o_mux_3_OUT_0_Q
    );
  D_clk_filter_Mcount_divisore_frequenza_count_xor_0_11_INV_0 : INV
    port map (
      I => D_clk_filter_divisore_frequenza_count(0),
      O => D_clk_filter_Mcount_divisore_frequenza_count
    );

end Structure;

