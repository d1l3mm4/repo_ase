----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:01:44 08/31/2020 
-- Design Name: 
-- Module Name:    Decimal_Display - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Decimal_Display is
    Port ( clock : in  STD_LOGIC;
           reset : in  STD_LOGIC;
           load : in  STD_LOGIC;
           switches : in  STD_LOGIC_VECTOR (12 DOWNTO 0);
           anodes : out  STD_LOGIC_VECTOR (7 DOWNTO 0);
           cathodes : out  STD_LOGIC_VECTOR (7 DOWNTO 0);
			  busyLed : out STD_LOGIC
			  );
end Decimal_Display;

architecture Structural of Decimal_Display is
	
	COMPONENT Display
	Generic (
			  --frequenza in ingresso (leggere sull'ucf)
			  clock_freq_in : integer := 100000000;
			  --frequenza in uscita
			  clock_freq_out : integer := 5000000
			  );
    Port ( value : in  STD_LOGIC_VECTOR (15 downto 0);
			  reset: in STD_LOGIC;
			  clk: in STD_LOGIC;
           dots : in  STD_LOGIC_VECTOR(7 downto 0);
			  enable: in STD_LOGIC_VECTOR(7 downto 0);
           cathodes : out  STD_LOGIC_VECTOR(7 downto 0);
           anodes : out  STD_LOGIC_VECTOR(7 downto 0));
	end COMPONENT;
	
	
	COMPONENT binary_to_bcd
		GENERIC(
			bits		:	INTEGER := 13;		--size of the binary input numbers in bits
			digits	:	INTEGER := 4);		--number of BCD digits to convert to
		PORT(
			clk		:	IN		STD_LOGIC;											--system clock
			reset_n	:	IN		STD_LOGIC;											--active low asynchronus reset
			ena		:	IN		STD_LOGIC;											--latches in new binary number and starts conversion
			binary	:	IN		STD_LOGIC_VECTOR(bits-1 DOWNTO 0);			--binary number to convert
			busy		:	OUT	STD_LOGIC;											--indicates conversion in progress
			bcd		:	OUT	STD_LOGIC_VECTOR(digits*4-1 DOWNTO 0));	--resulting BCD number
			
	END COMPONENT;
	
	SIGNAL valueToDisplay :  STD_LOGIC_VECTOR(15 DOWNTO 0);
	SIGNAL reset_n: STD_LOGIC;

begin

reset_n <= not(reset);

D: Display
PORT MAP(
	valueToDisplay,
	reset,
	clock,
	"00000000",
	"00001111",
	cathodes,
	anodes
);

B: binary_to_bcd
PORT MAP(
	clock,
	reset_n,
	load,
	switches,
	busyLed,
	valueToDisplay
);

end Structural;

