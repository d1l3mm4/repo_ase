--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   16:33:12 08/31/2020
-- Design Name:   
-- Module Name:   C:/ASE/ProvaBinary2BCD/decimal_display_tb.vhd
-- Project Name:  ProvaBinary2BCD
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: Decimal_Display
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY decimal_display_tb IS
END decimal_display_tb;
 
ARCHITECTURE behavior OF decimal_display_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT Decimal_Display
    PORT(
         clock : IN  std_logic;
         reset : IN  std_logic;
         load : IN  std_logic;
         switches : IN  std_logic_vector(13 downto 0);
         anodes : OUT  std_logic_vector(7 downto 0);
         cathodes : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clock : std_logic := '0';
   signal reset : std_logic := '0';
   signal load : std_logic := '0';
   signal switches : std_logic_vector(13 downto 0) := (others => '0');

 	--Outputs
   signal anodes : std_logic_vector(7 downto 0);
   signal cathodes : std_logic_vector(7 downto 0);

   -- Clock period definitions
   constant clock_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: Decimal_Display PORT MAP (
          clock => clock,
          reset => reset,
          load => load,
          switches => switches,
          anodes => anodes,
          cathodes => cathodes
        );

   -- Clock process definitions
   clock_process :process
   begin
		clock <= '0';
		wait for clock_period/2;
		clock <= '1';
		wait for clock_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

      wait for clock_period*10;
		
		reset <= '1';
		wait for clock_period*10;
		
		reset <= '0';
		switches <= "00000010000000";
		load <= '1';
		
		wait for clock_period*10;
		load <= '0';
		
		

      wait;
   end process;

END;
