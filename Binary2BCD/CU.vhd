----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:10:36 08/31/2020 
-- Design Name: 
-- Module Name:    CU - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity CU is
	GENERIC (
		bits	:	INTEGER := 14 ;
		digits:	INTEGER :=	4 );
	
    Port ( reset : in  STD_LOGIC;
           enable : in  STD_LOGIC;
           switch : in  STD_LOGIC_VECTOR(bits-1 DOWNTO 0);
           cathodes : out  STD_LOGIC_VECTOR(7 DOWNTO 0);
           anodes : out  STD_LOGIC_VECTOR(digits-1 DOWNTO 0);
end CU;

architecture Behavioral of CU is

begin


end Behavioral;

