/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "C:/ASE/ProvaBinary2BCD/anodes_instance.vhd";
extern char *IEEE_P_2592010699;

char *ieee_p_2592010699_sub_1735675855_503743352(char *, char *, char *, char *, char *, char *);
char *ieee_p_2592010699_sub_1837678034_503743352(char *, char *, char *, char *);


static void work_a_1291719539_3212880686_p_0(char *t0)
{
    char t1[16];
    char t2[16];
    char t6[16];
    char *t3;
    char *t4;
    char *t5;
    char *t7;
    char *t8;
    char *t9;
    char *t10;
    char *t11;
    unsigned int t12;
    unsigned int t13;
    unsigned char t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;
    char *t19;
    char *t20;

LAB0:    xsi_set_current_line(44, ng0);

LAB3:    t3 = (t0 + 1512U);
    t4 = *((char **)t3);
    t3 = (t0 + 5128U);
    t5 = ieee_p_2592010699_sub_1837678034_503743352(IEEE_P_2592010699, t2, t4, t3);
    t7 = (t0 + 1192U);
    t8 = *((char **)t7);
    t7 = (t0 + 5096U);
    t9 = ieee_p_2592010699_sub_1837678034_503743352(IEEE_P_2592010699, t6, t8, t7);
    t10 = ieee_p_2592010699_sub_1735675855_503743352(IEEE_P_2592010699, t1, t5, t2, t9, t6);
    t11 = (t1 + 12U);
    t12 = *((unsigned int *)t11);
    t13 = (1U * t12);
    t14 = (8U != t13);
    if (t14 == 1)
        goto LAB5;

LAB6:    t15 = (t0 + 3336);
    t16 = (t15 + 56U);
    t17 = *((char **)t16);
    t18 = (t17 + 56U);
    t19 = *((char **)t18);
    memcpy(t19, t10, 8U);
    xsi_driver_first_trans_fast_port(t15);

LAB2:    t20 = (t0 + 3240);
    *((int *)t20) = 1;

LAB1:    return;
LAB4:    goto LAB2;

LAB5:    xsi_size_not_matching(8U, t13, 0);
    goto LAB6;

}

static void work_a_1291719539_3212880686_p_1(char *t0)
{
    char t19[16];
    char t21[16];
    char t26[16];
    char *t1;
    char *t2;
    char *t3;
    int t4;
    char *t5;
    char *t6;
    int t7;
    char *t8;
    char *t9;
    int t10;
    char *t11;
    char *t12;
    int t13;
    char *t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;
    char *t20;
    char *t22;
    char *t23;
    int t24;
    unsigned int t25;
    char *t27;
    int t28;
    unsigned char t29;
    char *t30;
    char *t31;
    char *t32;
    char *t33;

LAB0:    xsi_set_current_line(50, ng0);
    t1 = (t0 + 1032U);
    t2 = *((char **)t1);
    t1 = (t0 + 5172);
    t4 = xsi_mem_cmp(t1, t2, 4U);
    if (t4 == 1)
        goto LAB3;

LAB8:    t5 = (t0 + 5176);
    t7 = xsi_mem_cmp(t5, t2, 4U);
    if (t7 == 1)
        goto LAB4;

LAB9:    t8 = (t0 + 5180);
    t10 = xsi_mem_cmp(t8, t2, 4U);
    if (t10 == 1)
        goto LAB5;

LAB10:    t11 = (t0 + 5184);
    t13 = xsi_mem_cmp(t11, t2, 4U);
    if (t13 == 1)
        goto LAB6;

LAB11:
LAB7:    xsi_set_current_line(60, ng0);
    t1 = xsi_get_transient_memory(8U);
    memset(t1, 0, 8U);
    t2 = t1;
    memset(t2, (unsigned char)2, 8U);
    t3 = (t0 + 3400);
    t5 = (t3 + 56U);
    t6 = *((char **)t5);
    t8 = (t6 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t1, 8U);
    xsi_driver_first_trans_fast(t3);

LAB2:    t1 = (t0 + 3256);
    *((int *)t1) = 1;

LAB1:    return;
LAB3:    xsi_set_current_line(52, ng0);
    t14 = (t0 + 5188);
    t16 = (t0 + 5192);
    t20 = ((IEEE_P_2592010699) + 4024);
    t22 = (t21 + 0U);
    t23 = (t22 + 0U);
    *((int *)t23) = 0;
    t23 = (t22 + 4U);
    *((int *)t23) = 3;
    t23 = (t22 + 8U);
    *((int *)t23) = 1;
    t24 = (3 - 0);
    t25 = (t24 * 1);
    t25 = (t25 + 1);
    t23 = (t22 + 12U);
    *((unsigned int *)t23) = t25;
    t23 = (t26 + 0U);
    t27 = (t23 + 0U);
    *((int *)t27) = 0;
    t27 = (t23 + 4U);
    *((int *)t27) = 3;
    t27 = (t23 + 8U);
    *((int *)t27) = 1;
    t28 = (3 - 0);
    t25 = (t28 * 1);
    t25 = (t25 + 1);
    t27 = (t23 + 12U);
    *((unsigned int *)t27) = t25;
    t18 = xsi_base_array_concat(t18, t19, t20, (char)97, t14, t21, (char)97, t16, t26, (char)101);
    t25 = (4U + 4U);
    t29 = (8U != t25);
    if (t29 == 1)
        goto LAB13;

LAB14:    t27 = (t0 + 3400);
    t30 = (t27 + 56U);
    t31 = *((char **)t30);
    t32 = (t31 + 56U);
    t33 = *((char **)t32);
    memcpy(t33, t18, 8U);
    xsi_driver_first_trans_fast(t27);
    goto LAB2;

LAB4:    xsi_set_current_line(54, ng0);
    t1 = (t0 + 5196);
    t3 = (t0 + 5200);
    t8 = ((IEEE_P_2592010699) + 4024);
    t9 = (t21 + 0U);
    t11 = (t9 + 0U);
    *((int *)t11) = 0;
    t11 = (t9 + 4U);
    *((int *)t11) = 3;
    t11 = (t9 + 8U);
    *((int *)t11) = 1;
    t4 = (3 - 0);
    t25 = (t4 * 1);
    t25 = (t25 + 1);
    t11 = (t9 + 12U);
    *((unsigned int *)t11) = t25;
    t11 = (t26 + 0U);
    t12 = (t11 + 0U);
    *((int *)t12) = 0;
    t12 = (t11 + 4U);
    *((int *)t12) = 3;
    t12 = (t11 + 8U);
    *((int *)t12) = 1;
    t7 = (3 - 0);
    t25 = (t7 * 1);
    t25 = (t25 + 1);
    t12 = (t11 + 12U);
    *((unsigned int *)t12) = t25;
    t6 = xsi_base_array_concat(t6, t19, t8, (char)97, t1, t21, (char)97, t3, t26, (char)101);
    t25 = (4U + 4U);
    t29 = (8U != t25);
    if (t29 == 1)
        goto LAB15;

LAB16:    t12 = (t0 + 3400);
    t14 = (t12 + 56U);
    t15 = *((char **)t14);
    t16 = (t15 + 56U);
    t17 = *((char **)t16);
    memcpy(t17, t6, 8U);
    xsi_driver_first_trans_fast(t12);
    goto LAB2;

LAB5:    xsi_set_current_line(56, ng0);
    t1 = (t0 + 5204);
    t3 = (t0 + 5208);
    t8 = ((IEEE_P_2592010699) + 4024);
    t9 = (t21 + 0U);
    t11 = (t9 + 0U);
    *((int *)t11) = 0;
    t11 = (t9 + 4U);
    *((int *)t11) = 3;
    t11 = (t9 + 8U);
    *((int *)t11) = 1;
    t4 = (3 - 0);
    t25 = (t4 * 1);
    t25 = (t25 + 1);
    t11 = (t9 + 12U);
    *((unsigned int *)t11) = t25;
    t11 = (t26 + 0U);
    t12 = (t11 + 0U);
    *((int *)t12) = 0;
    t12 = (t11 + 4U);
    *((int *)t12) = 3;
    t12 = (t11 + 8U);
    *((int *)t12) = 1;
    t7 = (3 - 0);
    t25 = (t7 * 1);
    t25 = (t25 + 1);
    t12 = (t11 + 12U);
    *((unsigned int *)t12) = t25;
    t6 = xsi_base_array_concat(t6, t19, t8, (char)97, t1, t21, (char)97, t3, t26, (char)101);
    t25 = (4U + 4U);
    t29 = (8U != t25);
    if (t29 == 1)
        goto LAB17;

LAB18:    t12 = (t0 + 3400);
    t14 = (t12 + 56U);
    t15 = *((char **)t14);
    t16 = (t15 + 56U);
    t17 = *((char **)t16);
    memcpy(t17, t6, 8U);
    xsi_driver_first_trans_fast(t12);
    goto LAB2;

LAB6:    xsi_set_current_line(58, ng0);
    t1 = (t0 + 5212);
    t3 = (t0 + 5216);
    t8 = ((IEEE_P_2592010699) + 4024);
    t9 = (t21 + 0U);
    t11 = (t9 + 0U);
    *((int *)t11) = 0;
    t11 = (t9 + 4U);
    *((int *)t11) = 3;
    t11 = (t9 + 8U);
    *((int *)t11) = 1;
    t4 = (3 - 0);
    t25 = (t4 * 1);
    t25 = (t25 + 1);
    t11 = (t9 + 12U);
    *((unsigned int *)t11) = t25;
    t11 = (t26 + 0U);
    t12 = (t11 + 0U);
    *((int *)t12) = 0;
    t12 = (t11 + 4U);
    *((int *)t12) = 3;
    t12 = (t11 + 8U);
    *((int *)t12) = 1;
    t7 = (3 - 0);
    t25 = (t7 * 1);
    t25 = (t25 + 1);
    t12 = (t11 + 12U);
    *((unsigned int *)t12) = t25;
    t6 = xsi_base_array_concat(t6, t19, t8, (char)97, t1, t21, (char)97, t3, t26, (char)101);
    t25 = (4U + 4U);
    t29 = (8U != t25);
    if (t29 == 1)
        goto LAB19;

LAB20:    t12 = (t0 + 3400);
    t14 = (t12 + 56U);
    t15 = *((char **)t14);
    t16 = (t15 + 56U);
    t17 = *((char **)t16);
    memcpy(t17, t6, 8U);
    xsi_driver_first_trans_fast(t12);
    goto LAB2;

LAB12:;
LAB13:    xsi_size_not_matching(8U, t25, 0);
    goto LAB14;

LAB15:    xsi_size_not_matching(8U, t25, 0);
    goto LAB16;

LAB17:    xsi_size_not_matching(8U, t25, 0);
    goto LAB18;

LAB19:    xsi_size_not_matching(8U, t25, 0);
    goto LAB20;

}


extern void work_a_1291719539_3212880686_init()
{
	static char *pe[] = {(void *)work_a_1291719539_3212880686_p_0,(void *)work_a_1291719539_3212880686_p_1};
	xsi_register_didat("work_a_1291719539_3212880686", "isim/decimal_display_tb_isim_beh.exe.sim/work/a_1291719539_3212880686.didat");
	xsi_register_executes(pe);
}
