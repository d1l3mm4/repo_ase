----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:25:17 08/31/2020 
-- Design Name: 
-- Module Name:    counter - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.math_real.all;
use IEEE.std_logic_arith.all ;
use IEEE.numeric_std.all ;

entity  Counter_ModN  is generic (N : integer := 4) ;
  port (
			clock : in  STD_LOGIC;
      	reset : in  STD_LOGIC;
			enable : in STD_LOGIC;
      	counter : out  STD_LOGIC_VECTOR ( 3 DOWNTO 0) ; 
			counter_hit : out std_logic := '0'
		); 
end Counter_ModN;

architecture Behavioral of Counter_ModN is

signal count : integer range 0 to N-1 := 0;

begin

counter <= std_logic_vector( to_unsigned( count, counter'length ) );


conteggio: process(reset, clock, enable)
	
	begin
		if reset = '1' then
			count <= 0;
			counter_hit <= '0';
		elsif(rising_edge(clock) and enable = '1') then
			if(count = N-1) then
				count <= 0;
				counter_hit <= '1';
			else 
				count <= count + 1;
				counter_hit <= '0';
			end if;
		end if;
	end process;
	
	
end Behavioral;



