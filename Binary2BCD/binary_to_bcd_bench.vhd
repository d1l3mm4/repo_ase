--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   14:40:08 08/31/2020
-- Design Name:   
-- Module Name:   C:/ASE/ProvaBinary2BCD/binary_to_bcd_bench.vhd
-- Project Name:  ProvaBinary2BCD
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: binary_to_bcd
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY binary_to_bcd_bench IS
END binary_to_bcd_bench;
 
ARCHITECTURE behavior OF binary_to_bcd_bench IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT binary_to_bcd
    PORT(
         clk : IN  std_logic;
         reset_n : IN  std_logic;
         ena : IN  std_logic;
         binary : IN  std_logic_vector(13 downto 0);
         busy : OUT  std_logic;
         bcd : OUT  std_logic_vector(15 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal reset_n : std_logic := '0';
   signal ena : std_logic := '0';
   signal binary : std_logic_vector(13 downto 0) := (others => '0');

 	--Outputs
   signal busy : std_logic;
   signal bcd : std_logic_vector(15 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: binary_to_bcd PORT MAP (
          clk => clk,
          reset_n => reset_n,
          ena => ena,
          binary => binary,
          busy => busy,
          bcd => bcd
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

      wait for clk_period*10;

      -- insert stimulus here 
		reset_n <= '0';
		
		wait for clk_period*10;
		
		reset_n <= '1';
		binary <= "10011100001111";
		ena <= '1';
		
		wait for clk_period*2;
		ena <= '0';

      wait for 300 ns;
		
		reset_n <= '0';
		
		wait for clk_period*10;
		
		reset_n <= '1';
		binary <= "00100100100110"; --2342
		ena <= '1';
		wait for clk_period * 2;
		ena <= '0';
		
		wait;
		
   end process;

END;
