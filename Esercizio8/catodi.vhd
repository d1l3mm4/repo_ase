----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    21:52:53 01/17/2020 
-- Design Name: 
-- Module Name:    catodi - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity catodi is
    Port ( counter : in  STD_LOGIC_VECTOR (3 downto 0);			
           value : in  STD_LOGIC_VECTOR (15 downto 0);  -- valore da mostrare
           dots : in  STD_LOGIC_VECTOR (7 downto 0); -- punti
           cathodes : out  STD_LOGIC_VECTOR (7 downto 0)
			  );
end catodi;

architecture Behavioral of catodi is

--definisco delle costanti che rappresentano la codifica del valore corrispondente definito con l'etichetta 
--essendo in logica negata, i segmenti che dovrebbero essere accesi sono zero
--ogni cifra � un nibble

constant zero   : std_logic_vector(6 downto 0) := "1000000"; 
constant one    : std_logic_vector(6 downto 0) := "1111001"; 
constant two    : std_logic_vector(6 downto 0) := "0100100"; 
constant three  : std_logic_vector(6 downto 0) := "0110000"; 
constant four   : std_logic_vector(6 downto 0) := "0011001"; 
constant five   : std_logic_vector(6 downto 0) := "0010010"; 
constant six    : std_logic_vector(6 downto 0) := "0000010"; 
constant seven  : std_logic_vector(6 downto 0) := "1111000"; 
constant eight  : std_logic_vector(6 downto 0) := "0000000"; 
constant nine   : std_logic_vector(6 downto 0) := "0010000"; 
constant a   	 : std_logic_vector(6 downto 0) := "0001000"; 
constant b      : std_logic_vector(6 downto 0) := "0000011"; 
constant c      : std_logic_vector(6 downto 0) := "1000110"; 
constant d      : std_logic_vector(6 downto 0) := "0100001"; 
constant e      : std_logic_vector(6 downto 0) := "0000110"; 
constant f      : std_logic_vector(6 downto 0) := "0001110";

--il valore in ingresso su 16 bit deve essere diviso sulle 4 cifre del display, 
-- quindi viene diviso su 4 bit (nibble)
--alias definisce un'alias 

--in base al valore di conteggio devo decodificare una parte del mio 
--segnale di ingresso su 16 bit, e codificarla in base alla codifica del gestore dei 
--catodi, in modo da mostrare la cifra del valore corretto
alias digit_0 is value (3 downto 0);
alias digit_1 is value (7 downto 4);
alias digit_2 is value (11 downto 8);
alias digit_3 is value (15 downto 12);

signal cathodes_for_digit : std_logic_Vector(6 downto 0) := (others => '0');
signal nibble :std_logic_vector(3 downto 0) := (others => '0');

begin

--ho due process

--il primo process fa il digit switching, � un multiplexer che in base al valore di 
--conteggio mette sul segnale nibble il digit corretto

digit_switching: process(counter, value)

begin
	case counter is
		--per 00 metto digit 0 
		when "0000" =>
			nibble <= digit_0;
		when "0001" =>
			nibble <= digit_1;
		when "0010" =>
			nibble <= digit_2;
		when "0011" =>
			nibble <= digit_3;
		when others =>
			--� corretto mettere others?
			nibble <= (others => '0');
	end case;
end process;
			
--il secondo process fa da encoder delle 16 combinazioni in ingresso 
seven_segment_decoder_process: process(nibble) 
  begin 
    case nibble is 
		--quando � tutto zero metto su questo segnale la costante zero
      when "0000" => cathodes_for_digit <= zero; 
      when "0001" => cathodes_for_digit <= one; 
      when "0010" => cathodes_for_digit <= two; 
      when "0011" => cathodes_for_digit <= three; 
      when "0100" => cathodes_for_digit <= four; 
      when "0101" => cathodes_for_digit <= five; 
      when "0110" => cathodes_for_digit <= six; 
      when "0111" => cathodes_for_digit <= seven; 
      when "1000" => cathodes_for_digit <= eight; 
      when "1001" => cathodes_for_digit <= nine; 
      when "1010" => cathodes_for_digit <= a; 
      when "1011" => cathodes_for_digit <= b; 
      when "1100" => cathodes_for_digit <= c; 
      when "1101" => cathodes_for_digit <= d; 
      when "1110" => cathodes_for_digit <= e; 
      when "1111" => cathodes_for_digit <= f;
		when others => cathodes_for_digit <= (others => '0');
    end case; 
  end process seven_segment_decoder_process;
  
--il valore che metto sui catodi � il negato dei dot 
--ho selezionato il punto tramite il costrutto (to_integer(unsigned(counter))) � un modo
--per descrivere un multiplexer, � un cotrutto di selezione
--& significa accodamento
--accodo il valore del process superiore e quindi l'accensione di quella cifra particolare
--otterr� come stringa di uscita come primo bit se devo accendere o spegnere il punto 
--e la configurazione di tutto il resto dei 7 segmenti
--cambia il valore del contatore, quindi seleziona il ribble successivo, interroga il
--il segnale dei punti, e cambio di nuovo il valore dei catodi per mostrare la cifra desiderata
cathodes <= not dots(to_integer(unsigned(counter))) & cathodes_for_digit;



end Behavioral;

