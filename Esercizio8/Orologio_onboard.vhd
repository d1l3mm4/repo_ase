----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:10:39 01/17/2020 
-- Design Name: 
-- Module Name:    Orologio_onboard - Structural 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Orologio_onboard is
port( CLOCK: IN STD_LOGIC;
		SW: IN STD_LOGIC_VECTOR(7 downto 0);
		RESET: IN STD_LOGIC;
		LOAD1 : IN STD_LOGIC;
		LOAD2: IN STD_LOGIC;
		LOAD3: IN STD_LOGIC;
		START: IN STD_LOGIC;
		ANODES: OUT STD_LOGIC_VECTOR(7 downto 0);
		CATHODES: OUT STD_LOGIC_VECTOR(7 downto 0);
		SEC: OUT  STD_LOGIC_VECTOR(7 downto 0)
	--	MIN: OUT STD_LOGIC_VECTOR(7 downto 0)
);
end Orologio_onboard;

architecture Structural of Orologio_onboard is

signal clk2: std_logic;
signal reset_cu: std_logic;
signal en_s, en_m, en_h: std_logic;
signal cnt: std_logic;
signal en: std_logic;
signal value: std_logic_vector(7 downto 0);
signal min, h: std_logic_vector(7 downto 0);
signal bcd_min, bcd_h: std_logic_vector(7 downto 0);
signal reset_n: std_logic; --segnale di reset negato per convertitore BCD

component Orologio
port(CLOCK: in std_logic;
	  RESET: in std_logic;
	  VALUE_IN: in std_logic_vector(7 downto 0); -- valori di inizializzazione, per secondi minuti e ore
	  ENABLE1: in std_logic; -- abilita il contatore dei secondi a prendere il valore  iniziale
	  ENABLE2: in std_logic; -- abilita il contatore dei minuti a prendere il valore  iniziale
	  ENABLE3: in std_logic;-- abilita il contatore delle ore a prendere il valore  iniziale
	  COUNT: in std_logic; -- abilita i 3 contatori a contare
	  COUNT_S: out std_logic_vector(7 downto 0);
	  COUNT_M: out std_logic_vector(7 downto 0);
	  COUNT_H: out std_logic_vector(7 downto 0));
end component;

component Control_Unit
port(CLOCK: in STD_LOGIC;
	  RESET_IN: in STD_LOGIC;
	  LOAD1_IN: in STD_LOGIC;
	  LOAD2_IN: IN STD_LOGIC;
	  LOAD3_IN: IN STD_LOGIC;
	  START_IN: in STD_LOGIC;
	  ENABLE_1: out STD_LOGIC;
	  ENABLE_2: out STD_LOGIC;
	  ENABLE_3: out STD_LOGIC;
	  RESET: out STD_LOGIC;
	  COUNT : out STD_LOGIC);
end component;

component Register_n
    Port ( LOAD : in  STD_LOGIC;
           CLOCK : in  STD_LOGIC;
           RESET : in  STD_LOGIC;
           VALUE_IN : in  STD_LOGIC_VECTOR(7 downto 0);
           VALUE_OUT : out  STD_LOGIC_VECTOR(7 downto 0));
end component;

component Display
	 Generic (
			  --frequenza in ingresso (leggere sull'ucf)
			  clock_freq_in : integer := 100000000;
			  --frequenza in uscita
			  clock_freq_out : integer := 5000000
			  );
    Port ( value : in  STD_LOGIC_VECTOR (15 downto 0);
			  reset: in STD_LOGIC;
			  clk: in STD_LOGIC;
           dots : in  STD_LOGIC_VECTOR(7 downto 0);
			  enable: in STD_LOGIC_VECTOR(7 downto 0);
           cathodes : out  STD_LOGIC_VECTOR(7 downto 0);
           anodes : out  STD_LOGIC_VECTOR(7 downto 0));
end component;

component binary_to_bcd
	GENERIC(
		bits		:	INTEGER := 13;		--size of the binary input numbers in bits
		digits	:	INTEGER := 4);		--number of BCD digits to convert to
	PORT(
		clk		:	IN		STD_LOGIC;											--system clock
		reset_n	:	IN		STD_LOGIC;											--active low asynchronus reset
		ena		:	IN		STD_LOGIC;											--latches in new binary number and starts conversion
		binary	:	IN		STD_LOGIC_VECTOR(bits-1 DOWNTO 0);			--binary number to convert
		busy		:	OUT	STD_LOGIC;											--indicates conversion in progress
		bcd		:	OUT	STD_LOGIC_VECTOR(digits*4-1 DOWNTO 0);	--resulting BCD number
		carry_out: 	OUT	STD_LOGIC);
end component;

begin

en <= (en_s OR en_m OR en_h);
reset_n <= not(reset);
--enable_bcd <= en AND sec(0);


D: Display
generic map (100000000,
				 100000)
port map(bcd_h & bcd_min,
			reset_cu,
			clock,
			"00000100",
			"00001111",
			CATHODES,
			ANODES);
			

CU: Control_Unit
port map (clock,
			 RESET, 
			 LOAD1,
			 LOAD2,
			 LOAD3,
			 START,
			 en_s,
			 en_m,
			 en_h,
			 reset_cu,
			 cnt);

REG: Register_n
port map( en,
			 clock,
			 reset_cu,
			 sw,
			 value);
			 
OP: Orologio
port map( clock,
			 reset_cu,
			 value,
			 en_s,
			 en_m,
			 en_h,
			 cnt,
			 sec,
			 min,
			 h);

Bmin: binary_to_bcd
generic map(7, 2)
port map(
	clock,
	reset_n,
	'1', --Il convertitore mi serve attivo solo quando sono attivi i minuti
	min(6 downto 0),
	open,
	bcd_min,
	open);
	
Bhour: binary_to_bcd
generic map(7,2)
port map(
	clock,
	reset_n,
	'1',
	h(6 downto 0),
	open,
	bcd_h,
	open);
	
	




end Structural;

