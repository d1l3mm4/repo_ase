----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:31:34 01/17/2020 
-- Design Name: 
-- Module Name:    Orologio - Structural 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Orologio is
port(CLOCK: in std_logic;
	  RESET: in std_logic;
	  VALUE_IN: in std_logic_vector(7 downto 0); --valori di inizializzazione, per secondi minuti e ore
	  ENABLE1: in std_logic; --abilita il contatore dei secondi a prendere il valore  iniziale
	  ENABLE2: in std_logic; ----abilita il contatore dei minuti a prendere il valore  iniziale
	  ENABLE3: in std_logic;----abilita il contatore delle ore a prendere il valore  iniziale
	  COUNT: in std_logic; -- abilita i 3 contatori a contare
	  COUNT_S: out std_logic_vector(7 downto 0);
	  COUNT_M: out std_logic_vector(7 downto 0);
	  COUNT_H: out std_logic_vector(7 downto 0));
end Orologio;

architecture Structural of Orologio is

component clock_filter
	 --le frequenze sono generic perch� possono cambiare in base alle board
	 Generic (
			  --frequenza in ingresso (leggere sull'ucf)
			  clock_freq_in : integer := 1000000000;
			  --frequenza in uscita
			  clock_freq_out : integer := 100000000
			  );
    Port ( clock_in : in  STD_LOGIC;
           reset : in  STD_LOGIC;
           clock_out : out  STD_LOGIC);
end component;

--component Counter_Mod60
--  port (
--			clock : in  STD_LOGIC;
--      	reset : in  STD_LOGIC;
--			load: in STD_LOGIC;
--			data_in: in STD_LOGIC_VECTOR(7 DOWNTO 0);
--			enable : in STD_LOGIC;
--      	counter : out  STD_LOGIC_VECTOR( 7 DOWNTO 0) ; 
--			counter_hit : out std_logic := '0'
--		); 
--end component;
--
--component Counter_Mod24
--  port (
--			clock : in  STD_LOGIC;
--      	reset : in  STD_LOGIC;
--			load: in STD_LOGIC;
--			data_in: in STD_LOGIC_VECTOR(7 DOWNTO 0);
--			enable : in STD_LOGIC;
--      	counter : out  STD_LOGIC_VECTOR( 7 DOWNTO 0) ; 
--			counter_hit : out std_logic := '0'
--		); 
--end component;

component MyCounter_ModN
	Generic (
			  --Limite superiore del contatore (max 63: 8 bit)
			  n : integer := 60
			  );
	port (
				clock : in  STD_LOGIC;
				reset : in  STD_LOGIC;
				load: in STD_LOGIC;
				data_in: in STD_LOGIC_VECTOR(7 DOWNTO 0);
				enable : in STD_LOGIC;
				counter : out  STD_LOGIC_VECTOR( 7 DOWNTO 0) ; 
				counter_hit : out std_logic := '0'
			); 
end component;


--signal count_s: std_logic_vector( 7 downto 0);
--signal count_m: std_logic_vector( 7 downto 0);
--signal count_h: std_logic_vector( 7 downto 0);
--signal reset: std_logic;

signal s, m, h: std_logic;
begin

CF: clock_filter
generic map(100000000,
		  1)
port map(clock,
			reset,
			s);


--CNT_S: MyCounter_ModN
--generic map(60)
--port map(s,
--		    reset,
--			 enable1,
--			 value_in,
--			 count,
--			 count_s,
--			 m);

CNT_S: Counter_Mod60
port map( s,
		    reset,
			 enable1,
			 value_in,
			 count,
			 count_s,
			 m);

CNT_M: Counter_Mod60
port map( m,
		    reset,
			 enable2, 
		    value_in,
			 count,
			 count_m,
			 h);

--CNT_M: MyCounter_ModN
--generic map(60)
--port map( m,
--		    reset,
--			 enable2, 
--		    value_in,
--			 count,
--			 count_m,
--			 h);

CNT_H: Counter_Mod24
port map( h,
		    reset,
			 enable3,
			 value_in,
			 count,
			 count_h,
			 open);

--CNT_H: MyCounter_ModN
--generic map(24)
--port map( h,
--		    reset,
--			 enable3,
--			 value_in,
--			 count,
--			 count_h,
--			 open);
			 
--clock_s <= s;
--clock_min <= m;
--clock_h <= h;
			 

			

end Structural;

