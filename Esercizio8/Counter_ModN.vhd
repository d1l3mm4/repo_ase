----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:55:51 09/24/2020 
-- Design Name: 
-- Module Name:    Counter_ModN - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.math_real.all;
--use IEEE.std_logic_arith.all ;
use IEEE.numeric_std.all ;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity MyCounter_ModN is
	Generic (
			  --Limite superiore del contatore (max 63: 8 bit)
			  n : integer := 60
			  );
	port (
				clock : in  STD_LOGIC;
				reset : in  STD_LOGIC;
				load: in STD_LOGIC;
				data_in: in STD_LOGIC_VECTOR(7 DOWNTO 0);
				enable : in STD_LOGIC;
				counter : out  STD_LOGIC_VECTOR( 7 DOWNTO 0) ; 
				counter_hit : out std_logic := '0'
			); 
end MyCounter_ModN;

architecture Behavioral of MyCounter_ModN is

signal count : integer range 0 to n-1 := 0;

begin

counter <= std_logic_vector( to_unsigned( count, counter'length ) );


conteggio: process(reset, data_in, load, clock, enable)
	
	begin
		if reset = '1' then
			count <= 0;
			counter_hit <= '0';
		elsif load='1' then
			count <= to_integer(unsigned(data_in));
			counter_hit <= '0';
		elsif(rising_edge(clock) and enable = '1') then
			if(count = n-1) then
				count <= 0;
				counter_hit <= '1';
			else 
				count <= count + 1;
				counter_hit <= '0';
			end if;
		end if;
	end process;
	
 
end Behavioral;

