----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:45:54 01/17/2020 
-- Design Name: 
-- Module Name:    Counter_Mod24 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.math_real.all;
--use IEEE.std_logic_arith.all ;
use IEEE.numeric_std.all ;

entity  Counter_Mod24  is 
  port (
			clock : in  STD_LOGIC;
      	reset : in  STD_LOGIC;
			load: in STD_LOGIC;
			data_in: in STD_LOGIC_VECTOR(7 DOWNTO 0);
			enable : in STD_LOGIC;
      	counter : out  STD_LOGIC_VECTOR( 7 DOWNTO 0) ; 
			counter_hit : out std_logic := '0'
		); 
end Counter_Mod24;

architecture Behavioral of Counter_Mod24 is

signal count : integer range 0 to 24-1 := 0;

begin

counter <= std_logic_vector( to_unsigned( count, counter'length ) );


conteggio: process(reset, load, data_in, clock, enable)
	
	begin
		if reset = '1' then
			count <= 0;
			counter_hit <= '0';
		elsif load='1' then
			count <= to_integer(unsigned(data_in));
			counter_hit <='0';
		elsif(rising_edge(clock) and enable = '1') then
			if(count = 24-1) then
				count <= 0;
				counter_hit <= '1';
			else 
				count <= count + 1;
				counter_hit <= '0';
			end if;
		end if;
	end process;
	
	
end Behavioral;
