----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:18:21 01/17/2020 
-- Design Name: 
-- Module Name:    registern - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity register_n is
    Port ( LOAD : in  STD_LOGIC;
           CLOCK : in  STD_LOGIC;
           RESET : in  STD_LOGIC;
           VALUE_IN : in  STD_LOGIC_VECTOR(7 downto 0);
           VALUE_OUT : out  STD_LOGIC_VECTOR(7 downto 0));
end register_n;

architecture Behavioral of register_n is

signal data: std_logic_vector(7 downto 0);

begin

proc: process(LOAD, VALUE_IN, CLOCK, RESET)
begin

if (reset='1') then
data <= "00000000";
elsif (clock'event AND clock='1' AND load='1') then
data<=VALUE_IN;
end if;

end process;

value_out <=data;

end Behavioral;

