----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:23:48 01/17/2020 
-- Design Name: 
-- Module Name:    Control_Unit - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Control_Unit is
port(CLOCK: in STD_LOGIC;
	  RESET_IN: in STD_LOGIC;
	  LOAD1_IN: in STD_LOGIC;
	  LOAD2_IN: in STD_LOGIC;
	  LOAD3_IN: in STD_LOGIC;
	  START_IN: in STD_LOGIC;
	  ENABLE_1: out STD_LOGIC;
	  ENABLE_2: out STD_LOGIC;
	  ENABLE_3: out STD_LOGIC;
	  RESET: out STD_LOGIC;
	  COUNT : out STD_LOGIC);
end Control_Unit;

architecture Behavioral of Control_Unit is

type state is (RES, LOAD1, LOAD2, LOAD3, START, CLK);

signal current_state, next_state: state;

begin

proc1: process(CLOCK, RESET_IN)
begin
if (reset_in ='1') then
current_state <= RES;
elsif( rising_edge(clock)) then
current_state <=next_state;
end if;

end process;


proc2: process(CURRENT_STATE, CLOCK, RESET_IN, LOAD1_IN, LOAD2_IN, LOAD3_IN, START_IN)
begin

case current_state is

when RES => RESET <='1';
				ENABLE_1 <='0';
				ENABLE_2 <='0';
				ENABLE_3 <='0';
				COUNT <='0';
				next_state <= LOAD1;

when LOAD1 => RESET <='0';
				ENABLE_1 <='1';
				ENABLE_2 <='0';
				ENABLE_3 <='0';
				COUNT <='0';
				if(load1_in ='1') then
				next_state <=LOAD2;
			   else next_state <=LOAD1;
				end if;

when LOAD2 => RESET <='0';
				ENABLE_1 <='0';
				ENABLE_2 <='1';
				ENABLE_3 <='0';
				COUNT <='0';
				if(load2_in ='1') then
				next_state <=LOAD3;
			   else next_state <=LOAD2;
				end if;
				
WHEN LOAD3 => RESET <='0';
				ENABLE_1 <='0';
				ENABLE_2 <='0';
				ENABLE_3 <='1';
				COUNT <='0';
				if(load3_in ='1') then
				next_state <=START;
			   else next_state <=LOAD3;
				end if;

WHEN START => RESET <='0';
				ENABLE_1 <='0';
				ENABLE_2 <='0';
				ENABLE_3 <='0';
				COUNT <='0';
				if(start_in ='1') then
				next_state <=CLK;
			   else next_state <=START;
				end if;

WHEN CLK => RESET <='0';
				ENABLE_1 <='0';
				ENABLE_2 <='0';
				ENABLE_3 <='0';
				COUNT <='1';
				if(reset_in ='1') then
				next_state <=RES;
			   else next_state <=CLK;
				end if;

END CASE;

end process;

end Behavioral;

