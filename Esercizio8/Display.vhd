----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    21:53:55 01/17/2020 
-- Design Name: 
-- Module Name:    Display - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.math_real.all;
use IEEE.std_logic_arith.all ;
use IEEE.numeric_std.all ;

entity Display is
	 Generic (
			  --frequenza in ingresso (leggere sull'ucf)
			  clock_freq_in : integer := 100000000;
			  --frequenza in uscita
			  clock_freq_out : integer := 5000000
			  );
    Port ( value : in  STD_LOGIC_VECTOR (15 downto 0);
			  reset: in STD_LOGIC;
			  clk: in STD_LOGIC;
           dots : in  STD_LOGIC_VECTOR(7 downto 0);
			  enable: in STD_LOGIC_VECTOR(7 downto 0);
           cathodes : out  STD_LOGIC_VECTOR(7 downto 0);
           anodes : out  STD_LOGIC_VECTOR(7 downto 0));
end Display;

architecture Structural of Display is

component anodi
    Port ( counter : in  STD_LOGIC_VECTOR (3 downto 0);
           enable_digit : in  STD_LOGIC_VECTOR(7 downto 0);
           anodes : out  STD_LOGIC_VECTOR(7 downto 0));
end component;

component catodi
    Port ( counter : in  STD_LOGIC_VECTOR (3 downto 0);			
           value : in  STD_LOGIC_VECTOR (15 downto 0);  -- valore da mostrare
           dots : in  STD_LOGIC_VECTOR (7 downto 0); -- punti
           cathodes : out  STD_LOGIC_VECTOR (7 downto 0)
			  );
end component;

component clock_filter
	 Generic (
			  --frequenza in ingresso (leggere sull'ucf)
			  clock_freq_in : integer := 100000000;
			  --frequenza in uscita
			  clock_freq_out : integer := 5000000
			  );
    Port ( clock_in : in  STD_LOGIC;
           reset : in  STD_LOGIC;
           clock_out : out  STD_LOGIC);
end component;

component Counter_ModN
	generic (N : integer := 4) ;
   port (
			clock : in  STD_LOGIC;
      	reset : in  STD_LOGIC;
			enable : in STD_LOGIC;
      	counter : out  STD_LOGIC_VECTOR ( 3 DOWNTO 0) ; 
			counter_hit : out std_logic := '0'
		); 
end component;

signal count: std_logic_vector(3 downto 0);
signal clk_2: std_logic;

begin

anodes_instance: anodi
port map (count,
			 enable,
			 anodes);

cathodes_instance: catodi
port map (count,
			 value,
			 dots,
			 cathodes);

counter: Counter_ModN
generic map ( 4)
port map ( clk,
			  reset,
			  clk_2,
			  count,
			  open);
			  
clk_filter: clock_filter
generic map(clock_freq_in,
				clock_freq_out)
port map(clk,
			reset,
			clk_2);

end Structural;

