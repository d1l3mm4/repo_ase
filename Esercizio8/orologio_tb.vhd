--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   16:11:27 09/24/2020
-- Design Name:   
-- Module Name:   C:/Users/annal/OneDrive/Documenti/repo_ase/Esercizio8/Orologio_tb.vhd
-- Project Name:  Esercizio8
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: Orologio
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY Orologio_tb IS
END Orologio_tb;
 
ARCHITECTURE behavior OF Orologio_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT Orologio
    PORT(
         CLOCK : IN  std_logic;
         RESET : IN  std_logic;
         VALUE_IN : IN  std_logic_vector(7 downto 0);
         ENABLE1 : IN  std_logic;
         ENABLE2 : IN  std_logic;
         ENABLE3 : IN  std_logic;
         COUNT : IN  std_logic;
         COUNT_S : OUT  std_logic_vector(7 downto 0);
         COUNT_M : OUT  std_logic_vector(7 downto 0);
         COUNT_H : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal CLOCK : std_logic := '0';
   signal RESET : std_logic := '0';
   signal VALUE_IN : std_logic_vector(7 downto 0) := (others => '0');
   signal ENABLE1 : std_logic := '0';
   signal ENABLE2 : std_logic := '0';
   signal ENABLE3 : std_logic := '0';
   signal COUNT : std_logic := '0';

 	--Outputs
   signal COUNT_S : std_logic_vector(7 downto 0);
   signal COUNT_M : std_logic_vector(7 downto 0);
   signal COUNT_H : std_logic_vector(7 downto 0);

   -- Clock period definitions
   constant CLOCK_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: Orologio PORT MAP (
          CLOCK => CLOCK,
          RESET => RESET,
          VALUE_IN => VALUE_IN,
          ENABLE1 => ENABLE1,
          ENABLE2 => ENABLE2,
          ENABLE3 => ENABLE3,
          COUNT => COUNT,
          COUNT_S => COUNT_S,
          COUNT_M => COUNT_M,
          COUNT_H => COUNT_H
        );

   -- Clock process definitions
   CLOCK_process :process
   begin
		CLOCK <= '0';
		wait for CLOCK_period/2;
		CLOCK <= '1';
		wait for CLOCK_period/2;
   end process;
	

 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
	reset <='1';
	enable1 <='0';
	enable2 <='0';
	enable3 <='0';
	count <='0';
	
   wait for 100 ns;	
	reset <='0';
	value_in <="00111010";
	enable1<='1';
	enable2 <='0';
	enable3 <='0';
	
   wait for CLOCK_period*10;
	value_in <="00000100";
	enable1<='0';
	enable2 <='1';
	enable3 <='0';
	
	wait for CLOCK_period*10;
	value_in <="00001000";
	enable1<='0';
	enable2 <='0';
	enable3 <='1';
	
	wait for CLOCK_period*10;
	enable1<='0';
	enable2 <='0';
	enable3 <='0';
	count <='1';
      -- insert stimulus here 
		

      wait;
   end process;

END;
