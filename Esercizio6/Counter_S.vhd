----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:22:27 01/12/2020 
-- Design Name: 
-- Module Name:    Counter_S - structural 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Counter_S is
    Port ( CNT_IN : in  STD_LOGIC;
           RES : in  STD_LOGIC;
           CNT_OUT : out  STD_LOGIC_VECTOR(3 downto 0));
end Counter_S;

architecture structural of Counter_S is

component FF_T
Port		( CLK : in  STD_LOGIC;
           RES : in  STD_LOGIC;
           Y : out  STD_LOGIC);
end component;

signal s: std_logic_vector(3 downto 0);

begin

ff0: FF_T
port map( CNT_IN,
			 RES,
			 s(0));

ff1: FF_T
port map( s(0),
			 RES,
			 s(1));

ff2: FF_T
port map( s(1),
			 RES,
			 s(2));

ff3: FF_T
port map( s(2),
			 RES,
			 s(3));

CNT_OUT <= S(3) & S(2) & S(1) & S(0);

end structural;

