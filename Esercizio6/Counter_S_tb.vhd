--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   11:28:21 01/12/2020
-- Design Name:   
-- Module Name:   C:/Users/annal/Xilinx/Esercizio6/Counter_S_tb.vhd
-- Project Name:  Esercizio6
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: Counter_S
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY Counter_S_tb IS
END Counter_S_tb;
 
ARCHITECTURE behavior OF Counter_S_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT Counter_S
    PORT(
         CNT_IN : IN  std_logic;
         RES : IN  std_logic;
         CNT_OUT : OUT  std_logic_vector(3 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal CNT_IN : std_logic := '0';
   signal RES : std_logic := '0';

 	--Outputs
   signal CNT_OUT : std_logic_vector(3 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 

	
--signal clock: std_logic := '0';
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: Counter_S PORT MAP (
          CNT_IN => CNT_IN,
          RES => RES,
          CNT_OUT => CNT_OUT
        );

   -- Clock process definitions
   clock_process :process
   begin
		cnt_in <='0';
		wait for 5 ns;
		cnt_in <='1';
		wait for 5 ns;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	
		res <='1';
		
     wait for 5 ns;
	  	res <='0';

      -- insert stimulus here 

     wait;
   end process;

END;
