----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:04:08 01/12/2020 
-- Design Name: 
-- Module Name:    FF_T - RTL 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity FF_T is
    Port ( CLK : in  STD_LOGIC;
           RES : in  STD_LOGIC;
           Y : out  STD_LOGIC);
end FF_T;

architecture RTL of FF_T is

signal internal_state: std_logic;

begin

proc: process(CLK,RES)
begin
if (RES='1') then internal_state <= '0';
elsif(CLK'event AND CLK='0') then
internal_state <= NOT(internal_state);
end if;

end process;

Y<= internal_state;

end RTL;

