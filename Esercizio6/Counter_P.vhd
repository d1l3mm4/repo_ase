----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    12:02:38 01/12/2020 
-- Design Name: 
-- Module Name:    Counter_P - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Counter_P is
    Port ( CNT_IN : in  STD_LOGIC;
           RES : in  STD_LOGIC;
           CNT_OUT : out  STD_LOGIC_VECTOR(3 downto 0));
end Counter_P;

architecture Structural of Counter_P is

component FF_T
Port		( CLK : in  STD_LOGIC;
           RES : in  STD_LOGIC;
           Y : out  STD_LOGIC);
end component;

signal S: std_logic_vector(3 downto 0);
signal p: std_logic_vector(2 downto 0);
begin


ff0: FF_T
port map( CNT_IN,
			 RES,
			 S(0));

p(0) <= s(0) AND CNT_IN;

ff1: FF_T
port map( p(0),
			 RES,
			 S(1));

p(1) <= s(1) AND s(0) AND CNT_IN;

ff2: FF_T
port map( p(1),
			 RES,
			 S(2));

p(2) <= s(2) AND s(1) AND s(0) AND CNT_IN;

ff3: FF_T
port map( p(2),
			 RES,
			 S(3));

CNT_OUT <= S(3) & S(2) & S(1) & S(0);

end Structural;

