----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:18:33 01/15/2020 
-- Design Name: 
-- Module Name:    anodi - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity anodi is
    Port ( counter : in  STD_LOGIC_VECTOR (3 downto 0);
           enable_digit : in  STD_LOGIC_VECTOR(7 downto 0);
           anodes : out  STD_LOGIC_VECTOR(7 downto 0));
end anodi;

architecture Behavioral of anodi is

signal switch_anodi: STD_LOGIC_VECTOR( 7 downto 0);

begin

anodes <= not switch_anodi OR not enable_digit;

anodes_process: process(counter, enable_digit)

begin

	case counter is
		when "0000" =>
			switch_anodi <= x"0" & x"1"; --attivo il primo anodo,
		when "0001" =>
			switch_anodi <= x"0" & x"2" ; --attivo il secondo anodo
		when "0010" =>
			switch_anodi <= x"0" & x"4"; --attivo il terzo anodo
		when "0011" =>
			switch_anodi <= x"0" & x"8"; --attivo il quarto anodo
		when others =>
			switch_anodi <= (others => '0');
			
	end case;

end process;

end Behavioral;

