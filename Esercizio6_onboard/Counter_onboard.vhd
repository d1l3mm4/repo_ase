----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    12:42:26 01/12/2020 
-- Design Name: 
-- Module Name:    Counter_onboard - Structural 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Counter_onboard is
    Port ( CLK : in  STD_LOGIC;
           CNT_IN : in  STD_LOGIC;
           RES : in  STD_LOGIC;
			  CATHODES: out STD_LOGIC_VECTOR(7 downto 0);
			  ANODES: out STD_LOGIC_VECTOR(7 downto 0));
end Counter_onboard;

architecture Structural of Counter_onboard is

component Counter_S    
Port 		( CNT_IN : in  STD_LOGIC;
           RES : in  STD_LOGIC;
           CNT_OUT : out  STD_LOGIC_VECTOR(3 downto 0));
end component;

component Display
	 Generic (
			  --frequenza in ingresso (leggere sull'ucf)
			  clock_freq_in : integer := 100000000;
			  --frequenza in uscita
			  clock_freq_out : integer := 5000000
			  );
    Port ( value : in  STD_LOGIC_VECTOR (15 downto 0);
			  reset: in STD_LOGIC;
			  clk: in STD_LOGIC;
           dots : in  STD_LOGIC_VECTOR(7 downto 0);
			  enable: in STD_LOGIC_VECTOR(7 downto 0);
           cathodes : out  STD_LOGIC_VECTOR(7 downto 0);
           anodes : out  STD_LOGIC_VECTOR(7 downto 0));
end component;

 signal count: std_logic;
 signal Y: std_logic_vector(3 downto 0);
 
begin

D: Display
generic map (100000000,
				 10000)
port map( x"000" & Y,
			 RES,
			 CLK,
			 x"00",
			 "00000001",
			 cathodes,
			 anodes);
			 

Counter: Counter_S
port map ( count,
			  RES,
			  Y);
			  
cnt_process: process(clk, cnt_in, res)
begin

if (CLK'event AND CLK='1') then
if (CNT_IN='1') THEN count <='1';
else count <='0';
end if;
end if;


end process;

end Structural;

