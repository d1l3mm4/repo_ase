////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 1995-2012 Xilinx, Inc.  All rights reserved.
////////////////////////////////////////////////////////////////////////////////
//   ____  ____
//  /   /\/   /
// /___/  \  /    Vendor: Xilinx
// \   \   \/     Version: P.58f
//  \   \         Application: netgen
//  /   /         Filename: Counter_onboard_synthesis.v
// /___/   /\     Timestamp: Wed Jan 15 12:01:26 2020
// \   \  /  \ 
//  \___\/\___\
//             
// Command	: -intstyle ise -insert_glbl true -w -dir netgen/synthesis -ofmt verilog -sim Counter_onboard.ngc Counter_onboard_synthesis.v 
// Device	: xc7a100t-3-csg324
// Input file	: Counter_onboard.ngc
// Output file	: C:\Users\annal\Xilinx\Esercizio6_onboard\netgen\synthesis\Counter_onboard_synthesis.v
// # of Modules	: 1
// Design Name	: Counter_onboard
// Xilinx        : C:\Xilinx\14.5\ISE_DS\ISE\
//             
// Purpose:    
//     This verilog netlist is a verification model and uses simulation 
//     primitives which may not represent the true implementation of the 
//     device, however the netlist is functionally correct and should not 
//     be modified. This file cannot be synthesized and should only be used 
//     with supported simulation tools.
//             
// Reference:  
//     Command Line Tools User Guide, Chapter 23 and Synthesis and Simulation Design Guide, Chapter 6
//             
////////////////////////////////////////////////////////////////////////////////

`timescale 1 ns/1 ps

module Counter_onboard (
  CLK, CNT_IN, RES, CATHODES, ANODES
);
  input CLK;
  input CNT_IN;
  input RES;
  output [7 : 0] CATHODES;
  output [7 : 0] ANODES;
  wire CLK_BUFGP_0;
  wire CNT_IN_IBUF_1;
  wire RES_IBUF_2;
  wire CATHODES_6_OBUF_3;
  wire CATHODES_5_OBUF_4;
  wire CATHODES_4_OBUF_5;
  wire CATHODES_3_OBUF_6;
  wire CATHODES_2_OBUF_7;
  wire CATHODES_1_OBUF_8;
  wire CATHODES_0_OBUF_9;
  wire ANODES_0_OBUF_10;
  wire count_11;
  wire \Counter/ff3/internal_state_12 ;
  wire \Counter/ff2/internal_state_13 ;
  wire \Counter/ff1/internal_state_14 ;
  wire \Counter/ff0/internal_state_15 ;
  wire CATHODES_7_OBUF_16;
  wire N1;
  wire \D/clk_filter/end_count_18 ;
  wire \D/counter/count[1]_GND_10_o_mux_3_OUT<0> ;
  wire \D/counter/count[1]_GND_10_o_mux_3_OUT<1> ;
  wire \D/clk_filter/PWR_11_o_divisore_frequenza.count[13]_equal_1_o ;
  wire \Counter/ff3/internal_state_INV_15_o ;
  wire \Counter/ff2/internal_state_INV_15_o ;
  wire \Counter/ff1/internal_state_INV_15_o ;
  wire \Counter/ff0/internal_state_INV_15_o ;
  wire \D/clk_filter/Mcount_divisore_frequenza.count_eqn_0 ;
  wire \D/clk_filter/Mcount_divisore_frequenza.count_eqn_1 ;
  wire \D/clk_filter/Mcount_divisore_frequenza.count_eqn_2 ;
  wire \D/clk_filter/Mcount_divisore_frequenza.count_eqn_3 ;
  wire \D/clk_filter/Mcount_divisore_frequenza.count_eqn_4 ;
  wire \D/clk_filter/Mcount_divisore_frequenza.count_eqn_5 ;
  wire \D/clk_filter/Mcount_divisore_frequenza.count_eqn_6 ;
  wire \D/clk_filter/Mcount_divisore_frequenza.count_eqn_7 ;
  wire \D/clk_filter/Mcount_divisore_frequenza.count_eqn_8 ;
  wire \D/clk_filter/Mcount_divisore_frequenza.count_eqn_9 ;
  wire \D/clk_filter/Mcount_divisore_frequenza.count_eqn_10 ;
  wire \D/clk_filter/Mcount_divisore_frequenza.count_eqn_11 ;
  wire \D/clk_filter/Mcount_divisore_frequenza.count_eqn_12 ;
  wire \D/clk_filter/Mcount_divisore_frequenza.count_eqn_13 ;
  wire \D/clk_filter/PWR_11_o_divisore_frequenza.count[13]_equal_1_o<13> ;
  wire \D/clk_filter/PWR_11_o_divisore_frequenza.count[13]_equal_1_o<13>1_85 ;
  wire \D/clk_filter/Mcount_divisore_frequenza.count_cy<1>_rt_105 ;
  wire \D/clk_filter/Mcount_divisore_frequenza.count_cy<2>_rt_106 ;
  wire \D/clk_filter/Mcount_divisore_frequenza.count_cy<3>_rt_107 ;
  wire \D/clk_filter/Mcount_divisore_frequenza.count_cy<4>_rt_108 ;
  wire \D/clk_filter/Mcount_divisore_frequenza.count_cy<5>_rt_109 ;
  wire \D/clk_filter/Mcount_divisore_frequenza.count_cy<6>_rt_110 ;
  wire \D/clk_filter/Mcount_divisore_frequenza.count_cy<7>_rt_111 ;
  wire \D/clk_filter/Mcount_divisore_frequenza.count_cy<8>_rt_112 ;
  wire \D/clk_filter/Mcount_divisore_frequenza.count_cy<9>_rt_113 ;
  wire \D/clk_filter/Mcount_divisore_frequenza.count_cy<10>_rt_114 ;
  wire \D/clk_filter/Mcount_divisore_frequenza.count_cy<11>_rt_115 ;
  wire \D/clk_filter/Mcount_divisore_frequenza.count_cy<12>_rt_116 ;
  wire \D/clk_filter/Mcount_divisore_frequenza.count_xor<13>_rt_117 ;
  wire [1 : 0] \D/counter/count ;
  wire [13 : 0] \D/clk_filter/divisore_frequenza.count ;
  wire [13 : 0] Result;
  wire [0 : 0] \D/clk_filter/Mcount_divisore_frequenza.count_lut ;
  wire [12 : 0] \D/clk_filter/Mcount_divisore_frequenza.count_cy ;
  VCC   XST_VCC (
    .P(CATHODES_7_OBUF_16)
  );
  GND   XST_GND (
    .G(N1)
  );
  FD   count (
    .C(CLK_BUFGP_0),
    .D(CNT_IN_IBUF_1),
    .Q(count_11)
  );
  FDCE #(
    .INIT ( 1'b0 ))
  \D/counter/count_1  (
    .C(CLK_BUFGP_0),
    .CE(\D/clk_filter/end_count_18 ),
    .CLR(RES_IBUF_2),
    .D(\D/counter/count[1]_GND_10_o_mux_3_OUT<1> ),
    .Q(\D/counter/count [1])
  );
  FDCE #(
    .INIT ( 1'b0 ))
  \D/counter/count_0  (
    .C(CLK_BUFGP_0),
    .CE(\D/clk_filter/end_count_18 ),
    .CLR(RES_IBUF_2),
    .D(\D/counter/count[1]_GND_10_o_mux_3_OUT<0> ),
    .Q(\D/counter/count [0])
  );
  FDC #(
    .INIT ( 1'b0 ))
  \D/clk_filter/end_count  (
    .C(CLK_BUFGP_0),
    .CLR(RES_IBUF_2),
    .D(\D/clk_filter/PWR_11_o_divisore_frequenza.count[13]_equal_1_o ),
    .Q(\D/clk_filter/end_count_18 )
  );
  FDC_1   \Counter/ff3/internal_state  (
    .C(\Counter/ff2/internal_state_13 ),
    .CLR(RES_IBUF_2),
    .D(\Counter/ff3/internal_state_INV_15_o ),
    .Q(\Counter/ff3/internal_state_12 )
  );
  FDC_1   \Counter/ff2/internal_state  (
    .C(\Counter/ff1/internal_state_14 ),
    .CLR(RES_IBUF_2),
    .D(\Counter/ff2/internal_state_INV_15_o ),
    .Q(\Counter/ff2/internal_state_13 )
  );
  FDC_1   \Counter/ff1/internal_state  (
    .C(\Counter/ff0/internal_state_15 ),
    .CLR(RES_IBUF_2),
    .D(\Counter/ff1/internal_state_INV_15_o ),
    .Q(\Counter/ff1/internal_state_14 )
  );
  FDC_1   \Counter/ff0/internal_state  (
    .C(count_11),
    .CLR(RES_IBUF_2),
    .D(\Counter/ff0/internal_state_INV_15_o ),
    .Q(\Counter/ff0/internal_state_15 )
  );
  FDC #(
    .INIT ( 1'b0 ))
  \D/clk_filter/divisore_frequenza.count_0  (
    .C(CLK_BUFGP_0),
    .CLR(RES_IBUF_2),
    .D(\D/clk_filter/Mcount_divisore_frequenza.count_eqn_0 ),
    .Q(\D/clk_filter/divisore_frequenza.count [0])
  );
  FDC #(
    .INIT ( 1'b0 ))
  \D/clk_filter/divisore_frequenza.count_1  (
    .C(CLK_BUFGP_0),
    .CLR(RES_IBUF_2),
    .D(\D/clk_filter/Mcount_divisore_frequenza.count_eqn_1 ),
    .Q(\D/clk_filter/divisore_frequenza.count [1])
  );
  FDC #(
    .INIT ( 1'b0 ))
  \D/clk_filter/divisore_frequenza.count_2  (
    .C(CLK_BUFGP_0),
    .CLR(RES_IBUF_2),
    .D(\D/clk_filter/Mcount_divisore_frequenza.count_eqn_2 ),
    .Q(\D/clk_filter/divisore_frequenza.count [2])
  );
  FDC #(
    .INIT ( 1'b0 ))
  \D/clk_filter/divisore_frequenza.count_3  (
    .C(CLK_BUFGP_0),
    .CLR(RES_IBUF_2),
    .D(\D/clk_filter/Mcount_divisore_frequenza.count_eqn_3 ),
    .Q(\D/clk_filter/divisore_frequenza.count [3])
  );
  FDC #(
    .INIT ( 1'b0 ))
  \D/clk_filter/divisore_frequenza.count_4  (
    .C(CLK_BUFGP_0),
    .CLR(RES_IBUF_2),
    .D(\D/clk_filter/Mcount_divisore_frequenza.count_eqn_4 ),
    .Q(\D/clk_filter/divisore_frequenza.count [4])
  );
  FDC #(
    .INIT ( 1'b0 ))
  \D/clk_filter/divisore_frequenza.count_5  (
    .C(CLK_BUFGP_0),
    .CLR(RES_IBUF_2),
    .D(\D/clk_filter/Mcount_divisore_frequenza.count_eqn_5 ),
    .Q(\D/clk_filter/divisore_frequenza.count [5])
  );
  FDC #(
    .INIT ( 1'b0 ))
  \D/clk_filter/divisore_frequenza.count_6  (
    .C(CLK_BUFGP_0),
    .CLR(RES_IBUF_2),
    .D(\D/clk_filter/Mcount_divisore_frequenza.count_eqn_6 ),
    .Q(\D/clk_filter/divisore_frequenza.count [6])
  );
  FDC #(
    .INIT ( 1'b0 ))
  \D/clk_filter/divisore_frequenza.count_7  (
    .C(CLK_BUFGP_0),
    .CLR(RES_IBUF_2),
    .D(\D/clk_filter/Mcount_divisore_frequenza.count_eqn_7 ),
    .Q(\D/clk_filter/divisore_frequenza.count [7])
  );
  FDC #(
    .INIT ( 1'b0 ))
  \D/clk_filter/divisore_frequenza.count_8  (
    .C(CLK_BUFGP_0),
    .CLR(RES_IBUF_2),
    .D(\D/clk_filter/Mcount_divisore_frequenza.count_eqn_8 ),
    .Q(\D/clk_filter/divisore_frequenza.count [8])
  );
  FDC #(
    .INIT ( 1'b0 ))
  \D/clk_filter/divisore_frequenza.count_9  (
    .C(CLK_BUFGP_0),
    .CLR(RES_IBUF_2),
    .D(\D/clk_filter/Mcount_divisore_frequenza.count_eqn_9 ),
    .Q(\D/clk_filter/divisore_frequenza.count [9])
  );
  FDC #(
    .INIT ( 1'b0 ))
  \D/clk_filter/divisore_frequenza.count_10  (
    .C(CLK_BUFGP_0),
    .CLR(RES_IBUF_2),
    .D(\D/clk_filter/Mcount_divisore_frequenza.count_eqn_10 ),
    .Q(\D/clk_filter/divisore_frequenza.count [10])
  );
  FDC #(
    .INIT ( 1'b0 ))
  \D/clk_filter/divisore_frequenza.count_11  (
    .C(CLK_BUFGP_0),
    .CLR(RES_IBUF_2),
    .D(\D/clk_filter/Mcount_divisore_frequenza.count_eqn_11 ),
    .Q(\D/clk_filter/divisore_frequenza.count [11])
  );
  FDC #(
    .INIT ( 1'b0 ))
  \D/clk_filter/divisore_frequenza.count_12  (
    .C(CLK_BUFGP_0),
    .CLR(RES_IBUF_2),
    .D(\D/clk_filter/Mcount_divisore_frequenza.count_eqn_12 ),
    .Q(\D/clk_filter/divisore_frequenza.count [12])
  );
  FDC #(
    .INIT ( 1'b0 ))
  \D/clk_filter/divisore_frequenza.count_13  (
    .C(CLK_BUFGP_0),
    .CLR(RES_IBUF_2),
    .D(\D/clk_filter/Mcount_divisore_frequenza.count_eqn_13 ),
    .Q(\D/clk_filter/divisore_frequenza.count [13])
  );
  MUXCY   \D/clk_filter/Mcount_divisore_frequenza.count_cy<0>  (
    .CI(N1),
    .DI(CATHODES_7_OBUF_16),
    .S(\D/clk_filter/Mcount_divisore_frequenza.count_lut [0]),
    .O(\D/clk_filter/Mcount_divisore_frequenza.count_cy [0])
  );
  XORCY   \D/clk_filter/Mcount_divisore_frequenza.count_xor<0>  (
    .CI(N1),
    .LI(\D/clk_filter/Mcount_divisore_frequenza.count_lut [0]),
    .O(Result[0])
  );
  MUXCY   \D/clk_filter/Mcount_divisore_frequenza.count_cy<1>  (
    .CI(\D/clk_filter/Mcount_divisore_frequenza.count_cy [0]),
    .DI(N1),
    .S(\D/clk_filter/Mcount_divisore_frequenza.count_cy<1>_rt_105 ),
    .O(\D/clk_filter/Mcount_divisore_frequenza.count_cy [1])
  );
  XORCY   \D/clk_filter/Mcount_divisore_frequenza.count_xor<1>  (
    .CI(\D/clk_filter/Mcount_divisore_frequenza.count_cy [0]),
    .LI(\D/clk_filter/Mcount_divisore_frequenza.count_cy<1>_rt_105 ),
    .O(Result[1])
  );
  MUXCY   \D/clk_filter/Mcount_divisore_frequenza.count_cy<2>  (
    .CI(\D/clk_filter/Mcount_divisore_frequenza.count_cy [1]),
    .DI(N1),
    .S(\D/clk_filter/Mcount_divisore_frequenza.count_cy<2>_rt_106 ),
    .O(\D/clk_filter/Mcount_divisore_frequenza.count_cy [2])
  );
  XORCY   \D/clk_filter/Mcount_divisore_frequenza.count_xor<2>  (
    .CI(\D/clk_filter/Mcount_divisore_frequenza.count_cy [1]),
    .LI(\D/clk_filter/Mcount_divisore_frequenza.count_cy<2>_rt_106 ),
    .O(Result[2])
  );
  MUXCY   \D/clk_filter/Mcount_divisore_frequenza.count_cy<3>  (
    .CI(\D/clk_filter/Mcount_divisore_frequenza.count_cy [2]),
    .DI(N1),
    .S(\D/clk_filter/Mcount_divisore_frequenza.count_cy<3>_rt_107 ),
    .O(\D/clk_filter/Mcount_divisore_frequenza.count_cy [3])
  );
  XORCY   \D/clk_filter/Mcount_divisore_frequenza.count_xor<3>  (
    .CI(\D/clk_filter/Mcount_divisore_frequenza.count_cy [2]),
    .LI(\D/clk_filter/Mcount_divisore_frequenza.count_cy<3>_rt_107 ),
    .O(Result[3])
  );
  MUXCY   \D/clk_filter/Mcount_divisore_frequenza.count_cy<4>  (
    .CI(\D/clk_filter/Mcount_divisore_frequenza.count_cy [3]),
    .DI(N1),
    .S(\D/clk_filter/Mcount_divisore_frequenza.count_cy<4>_rt_108 ),
    .O(\D/clk_filter/Mcount_divisore_frequenza.count_cy [4])
  );
  XORCY   \D/clk_filter/Mcount_divisore_frequenza.count_xor<4>  (
    .CI(\D/clk_filter/Mcount_divisore_frequenza.count_cy [3]),
    .LI(\D/clk_filter/Mcount_divisore_frequenza.count_cy<4>_rt_108 ),
    .O(Result[4])
  );
  MUXCY   \D/clk_filter/Mcount_divisore_frequenza.count_cy<5>  (
    .CI(\D/clk_filter/Mcount_divisore_frequenza.count_cy [4]),
    .DI(N1),
    .S(\D/clk_filter/Mcount_divisore_frequenza.count_cy<5>_rt_109 ),
    .O(\D/clk_filter/Mcount_divisore_frequenza.count_cy [5])
  );
  XORCY   \D/clk_filter/Mcount_divisore_frequenza.count_xor<5>  (
    .CI(\D/clk_filter/Mcount_divisore_frequenza.count_cy [4]),
    .LI(\D/clk_filter/Mcount_divisore_frequenza.count_cy<5>_rt_109 ),
    .O(Result[5])
  );
  MUXCY   \D/clk_filter/Mcount_divisore_frequenza.count_cy<6>  (
    .CI(\D/clk_filter/Mcount_divisore_frequenza.count_cy [5]),
    .DI(N1),
    .S(\D/clk_filter/Mcount_divisore_frequenza.count_cy<6>_rt_110 ),
    .O(\D/clk_filter/Mcount_divisore_frequenza.count_cy [6])
  );
  XORCY   \D/clk_filter/Mcount_divisore_frequenza.count_xor<6>  (
    .CI(\D/clk_filter/Mcount_divisore_frequenza.count_cy [5]),
    .LI(\D/clk_filter/Mcount_divisore_frequenza.count_cy<6>_rt_110 ),
    .O(Result[6])
  );
  MUXCY   \D/clk_filter/Mcount_divisore_frequenza.count_cy<7>  (
    .CI(\D/clk_filter/Mcount_divisore_frequenza.count_cy [6]),
    .DI(N1),
    .S(\D/clk_filter/Mcount_divisore_frequenza.count_cy<7>_rt_111 ),
    .O(\D/clk_filter/Mcount_divisore_frequenza.count_cy [7])
  );
  XORCY   \D/clk_filter/Mcount_divisore_frequenza.count_xor<7>  (
    .CI(\D/clk_filter/Mcount_divisore_frequenza.count_cy [6]),
    .LI(\D/clk_filter/Mcount_divisore_frequenza.count_cy<7>_rt_111 ),
    .O(Result[7])
  );
  MUXCY   \D/clk_filter/Mcount_divisore_frequenza.count_cy<8>  (
    .CI(\D/clk_filter/Mcount_divisore_frequenza.count_cy [7]),
    .DI(N1),
    .S(\D/clk_filter/Mcount_divisore_frequenza.count_cy<8>_rt_112 ),
    .O(\D/clk_filter/Mcount_divisore_frequenza.count_cy [8])
  );
  XORCY   \D/clk_filter/Mcount_divisore_frequenza.count_xor<8>  (
    .CI(\D/clk_filter/Mcount_divisore_frequenza.count_cy [7]),
    .LI(\D/clk_filter/Mcount_divisore_frequenza.count_cy<8>_rt_112 ),
    .O(Result[8])
  );
  MUXCY   \D/clk_filter/Mcount_divisore_frequenza.count_cy<9>  (
    .CI(\D/clk_filter/Mcount_divisore_frequenza.count_cy [8]),
    .DI(N1),
    .S(\D/clk_filter/Mcount_divisore_frequenza.count_cy<9>_rt_113 ),
    .O(\D/clk_filter/Mcount_divisore_frequenza.count_cy [9])
  );
  XORCY   \D/clk_filter/Mcount_divisore_frequenza.count_xor<9>  (
    .CI(\D/clk_filter/Mcount_divisore_frequenza.count_cy [8]),
    .LI(\D/clk_filter/Mcount_divisore_frequenza.count_cy<9>_rt_113 ),
    .O(Result[9])
  );
  MUXCY   \D/clk_filter/Mcount_divisore_frequenza.count_cy<10>  (
    .CI(\D/clk_filter/Mcount_divisore_frequenza.count_cy [9]),
    .DI(N1),
    .S(\D/clk_filter/Mcount_divisore_frequenza.count_cy<10>_rt_114 ),
    .O(\D/clk_filter/Mcount_divisore_frequenza.count_cy [10])
  );
  XORCY   \D/clk_filter/Mcount_divisore_frequenza.count_xor<10>  (
    .CI(\D/clk_filter/Mcount_divisore_frequenza.count_cy [9]),
    .LI(\D/clk_filter/Mcount_divisore_frequenza.count_cy<10>_rt_114 ),
    .O(Result[10])
  );
  MUXCY   \D/clk_filter/Mcount_divisore_frequenza.count_cy<11>  (
    .CI(\D/clk_filter/Mcount_divisore_frequenza.count_cy [10]),
    .DI(N1),
    .S(\D/clk_filter/Mcount_divisore_frequenza.count_cy<11>_rt_115 ),
    .O(\D/clk_filter/Mcount_divisore_frequenza.count_cy [11])
  );
  XORCY   \D/clk_filter/Mcount_divisore_frequenza.count_xor<11>  (
    .CI(\D/clk_filter/Mcount_divisore_frequenza.count_cy [10]),
    .LI(\D/clk_filter/Mcount_divisore_frequenza.count_cy<11>_rt_115 ),
    .O(Result[11])
  );
  MUXCY   \D/clk_filter/Mcount_divisore_frequenza.count_cy<12>  (
    .CI(\D/clk_filter/Mcount_divisore_frequenza.count_cy [11]),
    .DI(N1),
    .S(\D/clk_filter/Mcount_divisore_frequenza.count_cy<12>_rt_116 ),
    .O(\D/clk_filter/Mcount_divisore_frequenza.count_cy [12])
  );
  XORCY   \D/clk_filter/Mcount_divisore_frequenza.count_xor<12>  (
    .CI(\D/clk_filter/Mcount_divisore_frequenza.count_cy [11]),
    .LI(\D/clk_filter/Mcount_divisore_frequenza.count_cy<12>_rt_116 ),
    .O(Result[12])
  );
  XORCY   \D/clk_filter/Mcount_divisore_frequenza.count_xor<13>  (
    .CI(\D/clk_filter/Mcount_divisore_frequenza.count_cy [12]),
    .LI(\D/clk_filter/Mcount_divisore_frequenza.count_xor<13>_rt_117 ),
    .O(Result[13])
  );
  LUT2 #(
    .INIT ( 4'hE ))
  \D/anodes_instance/_n0016<0>1  (
    .I0(\D/counter/count [1]),
    .I1(\D/counter/count [0]),
    .O(ANODES_0_OBUF_10)
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \D/counter/Mmux_count[1]_GND_10_o_mux_3_OUT21  (
    .I0(\D/counter/count [1]),
    .I1(\D/counter/count [0]),
    .O(\D/counter/count[1]_GND_10_o_mux_3_OUT<1> )
  );
  LUT6 #(
    .INIT ( 64'hFFFFFFFFFFFF0941 ))
  \D/cathodes_instance/Mram_cathodes_for_digit61  (
    .I0(\Counter/ff1/internal_state_14 ),
    .I1(\Counter/ff2/internal_state_13 ),
    .I2(\Counter/ff3/internal_state_12 ),
    .I3(\Counter/ff0/internal_state_15 ),
    .I4(\D/counter/count [0]),
    .I5(\D/counter/count [1]),
    .O(CATHODES_6_OBUF_3)
  );
  LUT6 #(
    .INIT ( 64'h0000001010111010 ))
  \D/cathodes_instance/Mram_cathodes_for_digit41  (
    .I0(\D/counter/count [0]),
    .I1(\D/counter/count [1]),
    .I2(\Counter/ff0/internal_state_15 ),
    .I3(\Counter/ff1/internal_state_14 ),
    .I4(\Counter/ff2/internal_state_13 ),
    .I5(\Counter/ff3/internal_state_12 ),
    .O(CATHODES_4_OBUF_5)
  );
  LUT6 #(
    .INIT ( 64'h1110001000101000 ))
  \D/cathodes_instance/Mram_cathodes_for_digit111  (
    .I0(\D/counter/count [0]),
    .I1(\D/counter/count [1]),
    .I2(\Counter/ff2/internal_state_13 ),
    .I3(\Counter/ff0/internal_state_15 ),
    .I4(\Counter/ff1/internal_state_14 ),
    .I5(\Counter/ff3/internal_state_12 ),
    .O(CATHODES_1_OBUF_8)
  );
  LUT6 #(
    .INIT ( 64'h0010100000010010 ))
  \D/cathodes_instance/Mram_cathodes_for_digit11  (
    .I0(\D/counter/count [0]),
    .I1(\D/counter/count [1]),
    .I2(\Counter/ff0/internal_state_15 ),
    .I3(\Counter/ff1/internal_state_14 ),
    .I4(\Counter/ff2/internal_state_13 ),
    .I5(\Counter/ff3/internal_state_12 ),
    .O(CATHODES_0_OBUF_9)
  );
  LUT6 #(
    .INIT ( 64'h1001000010000110 ))
  \D/cathodes_instance/Mram_cathodes_for_digit31  (
    .I0(\D/counter/count [0]),
    .I1(\D/counter/count [1]),
    .I2(\Counter/ff0/internal_state_15 ),
    .I3(\Counter/ff2/internal_state_13 ),
    .I4(\Counter/ff1/internal_state_14 ),
    .I5(\Counter/ff3/internal_state_12 ),
    .O(CATHODES_3_OBUF_6)
  );
  LUT6 #(
    .INIT ( 64'h0110000001010100 ))
  \D/cathodes_instance/Mram_cathodes_for_digit51  (
    .I0(\D/counter/count [0]),
    .I1(\D/counter/count [1]),
    .I2(\Counter/ff3/internal_state_12 ),
    .I3(\Counter/ff1/internal_state_14 ),
    .I4(\Counter/ff0/internal_state_15 ),
    .I5(\Counter/ff2/internal_state_13 ),
    .O(CATHODES_5_OBUF_4)
  );
  LUT6 #(
    .INIT ( 64'h1101000000000100 ))
  \D/cathodes_instance/Mram_cathodes_for_digit21  (
    .I0(\D/counter/count [0]),
    .I1(\D/counter/count [1]),
    .I2(\Counter/ff0/internal_state_15 ),
    .I3(\Counter/ff1/internal_state_14 ),
    .I4(\Counter/ff2/internal_state_13 ),
    .I5(\Counter/ff3/internal_state_12 ),
    .O(CATHODES_2_OBUF_7)
  );
  LUT6 #(
    .INIT ( 64'h8000000000000000 ))
  \D/clk_filter/PWR_11_o_divisore_frequenza.count[13]_equal_1_o<13>1  (
    .I0(\D/clk_filter/divisore_frequenza.count [3]),
    .I1(\D/clk_filter/divisore_frequenza.count [2]),
    .I2(\D/clk_filter/divisore_frequenza.count [9]),
    .I3(\D/clk_filter/divisore_frequenza.count [8]),
    .I4(\D/clk_filter/divisore_frequenza.count [13]),
    .I5(\D/clk_filter/divisore_frequenza.count [10]),
    .O(\D/clk_filter/PWR_11_o_divisore_frequenza.count[13]_equal_1_o<13> )
  );
  LUT6 #(
    .INIT ( 64'h0001000000000000 ))
  \D/clk_filter/PWR_11_o_divisore_frequenza.count[13]_equal_1_o<13>2  (
    .I0(\D/clk_filter/divisore_frequenza.count [4]),
    .I1(\D/clk_filter/divisore_frequenza.count [5]),
    .I2(\D/clk_filter/divisore_frequenza.count [6]),
    .I3(\D/clk_filter/divisore_frequenza.count [7]),
    .I4(\D/clk_filter/divisore_frequenza.count [1]),
    .I5(\D/clk_filter/divisore_frequenza.count [0]),
    .O(\D/clk_filter/PWR_11_o_divisore_frequenza.count[13]_equal_1_o<13>1_85 )
  );
  LUT4 #(
    .INIT ( 16'h1000 ))
  \D/clk_filter/PWR_11_o_divisore_frequenza.count[13]_equal_1_o<13>3  (
    .I0(\D/clk_filter/divisore_frequenza.count [11]),
    .I1(\D/clk_filter/divisore_frequenza.count [12]),
    .I2(\D/clk_filter/PWR_11_o_divisore_frequenza.count[13]_equal_1_o<13>1_85 ),
    .I3(\D/clk_filter/PWR_11_o_divisore_frequenza.count[13]_equal_1_o<13> ),
    .O(\D/clk_filter/PWR_11_o_divisore_frequenza.count[13]_equal_1_o )
  );
  IBUF   CNT_IN_IBUF (
    .I(CNT_IN),
    .O(CNT_IN_IBUF_1)
  );
  IBUF   RES_IBUF (
    .I(RES),
    .O(RES_IBUF_2)
  );
  OBUF   CATHODES_7_OBUF (
    .I(CATHODES_7_OBUF_16),
    .O(CATHODES[7])
  );
  OBUF   CATHODES_6_OBUF (
    .I(CATHODES_6_OBUF_3),
    .O(CATHODES[6])
  );
  OBUF   CATHODES_5_OBUF (
    .I(CATHODES_5_OBUF_4),
    .O(CATHODES[5])
  );
  OBUF   CATHODES_4_OBUF (
    .I(CATHODES_4_OBUF_5),
    .O(CATHODES[4])
  );
  OBUF   CATHODES_3_OBUF (
    .I(CATHODES_3_OBUF_6),
    .O(CATHODES[3])
  );
  OBUF   CATHODES_2_OBUF (
    .I(CATHODES_2_OBUF_7),
    .O(CATHODES[2])
  );
  OBUF   CATHODES_1_OBUF (
    .I(CATHODES_1_OBUF_8),
    .O(CATHODES[1])
  );
  OBUF   CATHODES_0_OBUF (
    .I(CATHODES_0_OBUF_9),
    .O(CATHODES[0])
  );
  OBUF   ANODES_7_OBUF (
    .I(CATHODES_7_OBUF_16),
    .O(ANODES[7])
  );
  OBUF   ANODES_6_OBUF (
    .I(CATHODES_7_OBUF_16),
    .O(ANODES[6])
  );
  OBUF   ANODES_5_OBUF (
    .I(CATHODES_7_OBUF_16),
    .O(ANODES[5])
  );
  OBUF   ANODES_4_OBUF (
    .I(CATHODES_7_OBUF_16),
    .O(ANODES[4])
  );
  OBUF   ANODES_3_OBUF (
    .I(CATHODES_7_OBUF_16),
    .O(ANODES[3])
  );
  OBUF   ANODES_2_OBUF (
    .I(CATHODES_7_OBUF_16),
    .O(ANODES[2])
  );
  OBUF   ANODES_1_OBUF (
    .I(CATHODES_7_OBUF_16),
    .O(ANODES[1])
  );
  OBUF   ANODES_0_OBUF (
    .I(ANODES_0_OBUF_10),
    .O(ANODES[0])
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \D/clk_filter/Mcount_divisore_frequenza.count_cy<1>_rt  (
    .I0(\D/clk_filter/divisore_frequenza.count [1]),
    .O(\D/clk_filter/Mcount_divisore_frequenza.count_cy<1>_rt_105 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \D/clk_filter/Mcount_divisore_frequenza.count_cy<2>_rt  (
    .I0(\D/clk_filter/divisore_frequenza.count [2]),
    .O(\D/clk_filter/Mcount_divisore_frequenza.count_cy<2>_rt_106 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \D/clk_filter/Mcount_divisore_frequenza.count_cy<3>_rt  (
    .I0(\D/clk_filter/divisore_frequenza.count [3]),
    .O(\D/clk_filter/Mcount_divisore_frequenza.count_cy<3>_rt_107 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \D/clk_filter/Mcount_divisore_frequenza.count_cy<4>_rt  (
    .I0(\D/clk_filter/divisore_frequenza.count [4]),
    .O(\D/clk_filter/Mcount_divisore_frequenza.count_cy<4>_rt_108 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \D/clk_filter/Mcount_divisore_frequenza.count_cy<5>_rt  (
    .I0(\D/clk_filter/divisore_frequenza.count [5]),
    .O(\D/clk_filter/Mcount_divisore_frequenza.count_cy<5>_rt_109 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \D/clk_filter/Mcount_divisore_frequenza.count_cy<6>_rt  (
    .I0(\D/clk_filter/divisore_frequenza.count [6]),
    .O(\D/clk_filter/Mcount_divisore_frequenza.count_cy<6>_rt_110 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \D/clk_filter/Mcount_divisore_frequenza.count_cy<7>_rt  (
    .I0(\D/clk_filter/divisore_frequenza.count [7]),
    .O(\D/clk_filter/Mcount_divisore_frequenza.count_cy<7>_rt_111 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \D/clk_filter/Mcount_divisore_frequenza.count_cy<8>_rt  (
    .I0(\D/clk_filter/divisore_frequenza.count [8]),
    .O(\D/clk_filter/Mcount_divisore_frequenza.count_cy<8>_rt_112 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \D/clk_filter/Mcount_divisore_frequenza.count_cy<9>_rt  (
    .I0(\D/clk_filter/divisore_frequenza.count [9]),
    .O(\D/clk_filter/Mcount_divisore_frequenza.count_cy<9>_rt_113 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \D/clk_filter/Mcount_divisore_frequenza.count_cy<10>_rt  (
    .I0(\D/clk_filter/divisore_frequenza.count [10]),
    .O(\D/clk_filter/Mcount_divisore_frequenza.count_cy<10>_rt_114 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \D/clk_filter/Mcount_divisore_frequenza.count_cy<11>_rt  (
    .I0(\D/clk_filter/divisore_frequenza.count [11]),
    .O(\D/clk_filter/Mcount_divisore_frequenza.count_cy<11>_rt_115 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \D/clk_filter/Mcount_divisore_frequenza.count_cy<12>_rt  (
    .I0(\D/clk_filter/divisore_frequenza.count [12]),
    .O(\D/clk_filter/Mcount_divisore_frequenza.count_cy<12>_rt_116 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \D/clk_filter/Mcount_divisore_frequenza.count_xor<13>_rt  (
    .I0(\D/clk_filter/divisore_frequenza.count [13]),
    .O(\D/clk_filter/Mcount_divisore_frequenza.count_xor<13>_rt_117 )
  );
  LUT5 #(
    .INIT ( 32'hEFFF0000 ))
  \D/clk_filter/Mcount_divisore_frequenza.count_eqn_131  (
    .I0(\D/clk_filter/divisore_frequenza.count [12]),
    .I1(\D/clk_filter/divisore_frequenza.count [11]),
    .I2(\D/clk_filter/PWR_11_o_divisore_frequenza.count[13]_equal_1_o<13>1_85 ),
    .I3(\D/clk_filter/PWR_11_o_divisore_frequenza.count[13]_equal_1_o<13> ),
    .I4(Result[13]),
    .O(\D/clk_filter/Mcount_divisore_frequenza.count_eqn_13 )
  );
  LUT5 #(
    .INIT ( 32'hEFFF0000 ))
  \D/clk_filter/Mcount_divisore_frequenza.count_eqn_121  (
    .I0(\D/clk_filter/divisore_frequenza.count [12]),
    .I1(\D/clk_filter/divisore_frequenza.count [11]),
    .I2(\D/clk_filter/PWR_11_o_divisore_frequenza.count[13]_equal_1_o<13>1_85 ),
    .I3(\D/clk_filter/PWR_11_o_divisore_frequenza.count[13]_equal_1_o<13> ),
    .I4(Result[12]),
    .O(\D/clk_filter/Mcount_divisore_frequenza.count_eqn_12 )
  );
  LUT5 #(
    .INIT ( 32'hEFFF0000 ))
  \D/clk_filter/Mcount_divisore_frequenza.count_eqn_111  (
    .I0(\D/clk_filter/divisore_frequenza.count [12]),
    .I1(\D/clk_filter/divisore_frequenza.count [11]),
    .I2(\D/clk_filter/PWR_11_o_divisore_frequenza.count[13]_equal_1_o<13>1_85 ),
    .I3(\D/clk_filter/PWR_11_o_divisore_frequenza.count[13]_equal_1_o<13> ),
    .I4(Result[11]),
    .O(\D/clk_filter/Mcount_divisore_frequenza.count_eqn_11 )
  );
  LUT5 #(
    .INIT ( 32'hEFFF0000 ))
  \D/clk_filter/Mcount_divisore_frequenza.count_eqn_101  (
    .I0(\D/clk_filter/divisore_frequenza.count [12]),
    .I1(\D/clk_filter/divisore_frequenza.count [11]),
    .I2(\D/clk_filter/PWR_11_o_divisore_frequenza.count[13]_equal_1_o<13>1_85 ),
    .I3(\D/clk_filter/PWR_11_o_divisore_frequenza.count[13]_equal_1_o<13> ),
    .I4(Result[10]),
    .O(\D/clk_filter/Mcount_divisore_frequenza.count_eqn_10 )
  );
  LUT5 #(
    .INIT ( 32'hEFFF0000 ))
  \D/clk_filter/Mcount_divisore_frequenza.count_eqn_91  (
    .I0(\D/clk_filter/divisore_frequenza.count [12]),
    .I1(\D/clk_filter/divisore_frequenza.count [11]),
    .I2(\D/clk_filter/PWR_11_o_divisore_frequenza.count[13]_equal_1_o<13>1_85 ),
    .I3(\D/clk_filter/PWR_11_o_divisore_frequenza.count[13]_equal_1_o<13> ),
    .I4(Result[9]),
    .O(\D/clk_filter/Mcount_divisore_frequenza.count_eqn_9 )
  );
  LUT5 #(
    .INIT ( 32'hEFFF0000 ))
  \D/clk_filter/Mcount_divisore_frequenza.count_eqn_81  (
    .I0(\D/clk_filter/divisore_frequenza.count [12]),
    .I1(\D/clk_filter/divisore_frequenza.count [11]),
    .I2(\D/clk_filter/PWR_11_o_divisore_frequenza.count[13]_equal_1_o<13>1_85 ),
    .I3(\D/clk_filter/PWR_11_o_divisore_frequenza.count[13]_equal_1_o<13> ),
    .I4(Result[8]),
    .O(\D/clk_filter/Mcount_divisore_frequenza.count_eqn_8 )
  );
  LUT5 #(
    .INIT ( 32'hEFFF0000 ))
  \D/clk_filter/Mcount_divisore_frequenza.count_eqn_71  (
    .I0(\D/clk_filter/divisore_frequenza.count [12]),
    .I1(\D/clk_filter/divisore_frequenza.count [11]),
    .I2(\D/clk_filter/PWR_11_o_divisore_frequenza.count[13]_equal_1_o<13>1_85 ),
    .I3(\D/clk_filter/PWR_11_o_divisore_frequenza.count[13]_equal_1_o<13> ),
    .I4(Result[7]),
    .O(\D/clk_filter/Mcount_divisore_frequenza.count_eqn_7 )
  );
  LUT5 #(
    .INIT ( 32'hEFFF0000 ))
  \D/clk_filter/Mcount_divisore_frequenza.count_eqn_61  (
    .I0(\D/clk_filter/divisore_frequenza.count [12]),
    .I1(\D/clk_filter/divisore_frequenza.count [11]),
    .I2(\D/clk_filter/PWR_11_o_divisore_frequenza.count[13]_equal_1_o<13>1_85 ),
    .I3(\D/clk_filter/PWR_11_o_divisore_frequenza.count[13]_equal_1_o<13> ),
    .I4(Result[6]),
    .O(\D/clk_filter/Mcount_divisore_frequenza.count_eqn_6 )
  );
  LUT5 #(
    .INIT ( 32'hEFFF0000 ))
  \D/clk_filter/Mcount_divisore_frequenza.count_eqn_51  (
    .I0(\D/clk_filter/divisore_frequenza.count [12]),
    .I1(\D/clk_filter/divisore_frequenza.count [11]),
    .I2(\D/clk_filter/PWR_11_o_divisore_frequenza.count[13]_equal_1_o<13>1_85 ),
    .I3(\D/clk_filter/PWR_11_o_divisore_frequenza.count[13]_equal_1_o<13> ),
    .I4(Result[5]),
    .O(\D/clk_filter/Mcount_divisore_frequenza.count_eqn_5 )
  );
  LUT5 #(
    .INIT ( 32'hEFFF0000 ))
  \D/clk_filter/Mcount_divisore_frequenza.count_eqn_41  (
    .I0(\D/clk_filter/divisore_frequenza.count [12]),
    .I1(\D/clk_filter/divisore_frequenza.count [11]),
    .I2(\D/clk_filter/PWR_11_o_divisore_frequenza.count[13]_equal_1_o<13>1_85 ),
    .I3(\D/clk_filter/PWR_11_o_divisore_frequenza.count[13]_equal_1_o<13> ),
    .I4(Result[4]),
    .O(\D/clk_filter/Mcount_divisore_frequenza.count_eqn_4 )
  );
  LUT5 #(
    .INIT ( 32'hEFFF0000 ))
  \D/clk_filter/Mcount_divisore_frequenza.count_eqn_31  (
    .I0(\D/clk_filter/divisore_frequenza.count [12]),
    .I1(\D/clk_filter/divisore_frequenza.count [11]),
    .I2(\D/clk_filter/PWR_11_o_divisore_frequenza.count[13]_equal_1_o<13>1_85 ),
    .I3(\D/clk_filter/PWR_11_o_divisore_frequenza.count[13]_equal_1_o<13> ),
    .I4(Result[3]),
    .O(\D/clk_filter/Mcount_divisore_frequenza.count_eqn_3 )
  );
  LUT5 #(
    .INIT ( 32'hEFFF0000 ))
  \D/clk_filter/Mcount_divisore_frequenza.count_eqn_21  (
    .I0(\D/clk_filter/divisore_frequenza.count [12]),
    .I1(\D/clk_filter/divisore_frequenza.count [11]),
    .I2(\D/clk_filter/PWR_11_o_divisore_frequenza.count[13]_equal_1_o<13>1_85 ),
    .I3(\D/clk_filter/PWR_11_o_divisore_frequenza.count[13]_equal_1_o<13> ),
    .I4(Result[2]),
    .O(\D/clk_filter/Mcount_divisore_frequenza.count_eqn_2 )
  );
  LUT5 #(
    .INIT ( 32'hEFFF0000 ))
  \D/clk_filter/Mcount_divisore_frequenza.count_eqn_14  (
    .I0(\D/clk_filter/divisore_frequenza.count [12]),
    .I1(\D/clk_filter/divisore_frequenza.count [11]),
    .I2(\D/clk_filter/PWR_11_o_divisore_frequenza.count[13]_equal_1_o<13>1_85 ),
    .I3(\D/clk_filter/PWR_11_o_divisore_frequenza.count[13]_equal_1_o<13> ),
    .I4(Result[1]),
    .O(\D/clk_filter/Mcount_divisore_frequenza.count_eqn_1 )
  );
  LUT5 #(
    .INIT ( 32'hE0F0F0F0 ))
  \D/clk_filter/Mcount_divisore_frequenza.count_eqn_01  (
    .I0(\D/clk_filter/divisore_frequenza.count [12]),
    .I1(\D/clk_filter/divisore_frequenza.count [11]),
    .I2(Result[0]),
    .I3(\D/clk_filter/PWR_11_o_divisore_frequenza.count[13]_equal_1_o<13>1_85 ),
    .I4(\D/clk_filter/PWR_11_o_divisore_frequenza.count[13]_equal_1_o<13> ),
    .O(\D/clk_filter/Mcount_divisore_frequenza.count_eqn_0 )
  );
  BUFGP   CLK_BUFGP (
    .I(CLK),
    .O(CLK_BUFGP_0)
  );
  INV   \D/clk_filter/Mcount_divisore_frequenza.count_lut<0>_INV_0  (
    .I(\D/clk_filter/divisore_frequenza.count [0]),
    .O(\D/clk_filter/Mcount_divisore_frequenza.count_lut [0])
  );
  INV   \D/counter/Mmux_count[1]_GND_10_o_mux_3_OUT11_INV_0  (
    .I(\D/counter/count [0]),
    .O(\D/counter/count[1]_GND_10_o_mux_3_OUT<0> )
  );
  INV   \Counter/ff3/internal_state_INV_15_o1_INV_0  (
    .I(\Counter/ff3/internal_state_12 ),
    .O(\Counter/ff3/internal_state_INV_15_o )
  );
  INV   \Counter/ff2/internal_state_INV_15_o1_INV_0  (
    .I(\Counter/ff2/internal_state_13 ),
    .O(\Counter/ff2/internal_state_INV_15_o )
  );
  INV   \Counter/ff1/internal_state_INV_15_o1_INV_0  (
    .I(\Counter/ff1/internal_state_14 ),
    .O(\Counter/ff1/internal_state_INV_15_o )
  );
  INV   \Counter/ff0/internal_state_INV_15_o1_INV_0  (
    .I(\Counter/ff0/internal_state_15 ),
    .O(\Counter/ff0/internal_state_INV_15_o )
  );
endmodule


`ifndef GLBL
`define GLBL

`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;

    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (weak1, weak0) GSR = GSR_int;
    assign (weak1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule

`endif

