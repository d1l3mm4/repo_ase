----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    12:46:32 01/15/2020 
-- Design Name: 
-- Module Name:    Ripple_Carry_8b - Structural 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Ripple_Carry_8b is
    Port ( x : in  STD_LOGIC_VECTOR(7 downto 0);
           y : in  STD_LOGIC_VECTOR(7 downto 0);
           cin : in  STD_LOGIC;
           cout : out  STD_LOGIC;
           s : out  STD_LOGIC_VECTOR(7 downto 0));
end Ripple_Carry_8b;

architecture Structural of Ripple_Carry_8b is

component Ripple_Carry_4b
    Port ( x : in  STD_LOGIC_VECTOR(3 downto 0);
           y : in  STD_LOGIC_VECTOR(3 downto 0);
           cin : in  STD_LOGIC;
           cout : out  STD_LOGIC;
           s : out  STD_LOGIC_VECTOR(3 downto 0));
end component;

signal c1: std_logic;
begin

rc1: Ripple_Carry_4b
port map ( x(3 downto 0),
			  y(3 downto 0),
			  cin,
			  c1,
			  s(3 downto 0));
			  
rc2: Ripple_Carry_4b
port map (x(7 downto 4),
			 y(7 downto 4),
			 c1,
			 cout,
			 s(7 downto 4));
			 
end Structural;

