----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:39:00 01/15/2020 
-- Design Name: 
-- Module Name:    Control_Unit - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Control_Unit is
    Port ( SW : in  STD_LOGIC_VECTOR(7 downto 0);
			  RIS: in STD_LOGIC_VECTOR(7 downto 0);
			  CLOCK: in STD_LOGIC;
           ADD : in  STD_LOGIC;
           SUB : in  STD_LOGIC;
			  C : in STD_LOGIC;
           LOAD_X : in  STD_LOGIC;
           LOAD_Y : in  STD_LOGIC;
           RESET : in  STD_LOGIC;
			  VALUE_DISPLAY: out STD_LOGIC_VECTOR(15 downto 0);
			  S: out STD_LOGIC;
			  COUT: out STD_LOGIC;
           X_OUT : out  STD_LOGIC_VECTOR(7 downto 0);
           Y_OUT : out  STD_LOGIC_VECTOR(7 downto 0));
end Control_Unit;

architecture Structural of Control_Unit is

component register_n
    Port ( LOAD : in  STD_LOGIC;
           CLOCK : in  STD_LOGIC;
           RESET : in  STD_LOGIC;
           VALUE_IN : in  STD_LOGIC_VECTOR(7 downto 0);
           VALUE_OUT : out  STD_LOGIC_VECTOR(7 downto 0));
end component;

signal op1, op2: STD_LOGIC_VECTOR( 7 downto 0);
signal val: STD_LOGIC_VECTOR(15 downto 0);

begin

reg_x: register_n
port map( LOAD_X,
			 CLOCK,
			 RESET,
			 SW,
			 op1);

reg_y: register_n
port map( LOAD_Y,
			 CLOCK,
			 RESET,
			 SW,
			 op2);


proc: process(LOAD_X, LOAD_Y, CLOCK, RESET)
begin

if(reset='1') then
val <= x"0000";
S <= '0';
COUT <='0';
elsif (clock'event and clock='1') then
if(load_x ='1') then
val <= x"00" & op1;
elsif(load_y='1') then
val <= x"00" & op2;
elsif(ADD ='1') then
val <= x"00" & ris;
COUT <= C;
S <='0';
elsif(SUB ='1') then
val <= x"00" & ris;
COUT <= '0';
S <='1';
end if;
end if;
end process;

value_display <= val;
X_OUT <=op1;
Y_OUT <=op2;
end Structural;

