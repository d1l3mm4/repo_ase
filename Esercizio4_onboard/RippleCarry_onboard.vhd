----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:28:41 01/15/2020 
-- Design Name: 
-- Module Name:    RippleCarry_onboard - Structural 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity RippleCarry_onboard is
    Port ( ADD : in  STD_LOGIC;
           SUB : in  STD_LOGIC;
			  LOAD_X: in STD_LOGIC;
			  LOAD_Y: in STD_LOGIC;
           CLOCK : in  STD_LOGIC;
           RESET : in  STD_LOGIC;
           SW : in  STD_LOGIC_VECTOR(7 downto 0);
           CATHODES : out  STD_LOGIC_VECTOR(7 downto 0);
           ANODES : out  STD_LOGIC_VECTOR(7 downto 0);
           COUT : out  STD_LOGIC);
end RippleCarry_onboard;

architecture Structural of RippleCarry_onboard is

component Ripple_Carry_addsub
    Port ( x : in  STD_LOGIC_VECTOR(7 downto 0);
           y : in  STD_LOGIC_VECTOR(7 downto 0);
           sub : in  STD_LOGIC;
           cout : out  STD_LOGIC;
           s : out  STD_LOGIC_VECTOR(7 downto 0));
end component;
	
component Control_Unit
    Port ( SW : in  STD_LOGIC_VECTOR(7 downto 0);
			  RIS: in STD_LOGIC_VECTOR(7 downto 0);
			  CLOCK: in STD_LOGIC;
           ADD : in  STD_LOGIC;
           SUB : in  STD_LOGIC;
			  C : in STD_LOGIC;
           LOAD_X : in  STD_LOGIC;
           LOAD_Y : in  STD_LOGIC;
           RESET : in  STD_LOGIC;
			  VALUE_DISPLAY: out STD_LOGIC_VECTOR(15 downto 0);
			  S: out STD_LOGIC;
			  COUT: out STD_LOGIC;
           X_OUT : out  STD_LOGIC_VECTOR(7 downto 0);
           Y_OUT : out  STD_LOGIC_VECTOR(7 downto 0));
end component;


component Display
	 Generic (
			  --frequenza in ingresso (leggere sull'ucf)
			  clock_freq_in : integer := 100000000;
			  --frequenza in uscita
			  clock_freq_out : integer := 5000000
			  );
    Port ( value : in  STD_LOGIC_VECTOR (15 downto 0);
			  reset: in STD_LOGIC;
			  clk: in STD_LOGIC;
           dots : in  STD_LOGIC_VECTOR(7 downto 0);
			  enable: in STD_LOGIC_VECTOR(7 downto 0);
           cathodes : out  STD_LOGIC_VECTOR(7 downto 0);
           anodes : out  STD_LOGIC_VECTOR(7 downto 0));
end component;

signal to_display: STD_LOGIC_VECTOR(15 downto 0);
signal risultato: STD_LOGIC_VECTOR(7 downto 0);
signal op1, op2: STD_LOGIC_VECTOR(7 downto 0);
signal S: STD_LOGIC;
signal carry, c_todisplay: STD_LOGIC;

begin


COUT <=c_todisplay;
RC: Ripple_Carry_addsub
port map(op1,
			op2,
			S,
			carry,
			risultato
);

CU: Control_Unit
port map(SW,
			risultato,
			CLOCK,
			ADD,
			SUB,
			carry,
			LOAD_X,
			LOAD_Y,
			RESET,
			to_display,
			S,
			c_todisplay,
			op1,
			op2);
			

D: Display
generic map(100000000,
				10000)
port map(to_display,
			RESET,
			CLOCK,
			x"00",
			"00000011",
			CATHODES,
			ANODES);
			
end Structural;

