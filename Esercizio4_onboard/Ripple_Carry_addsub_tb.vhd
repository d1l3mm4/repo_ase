--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   14:09:06 01/15/2020
-- Design Name:   
-- Module Name:   C:/Users/annal/Xilinx/Esercizio4/Ripple_Carry_addsub_tb.vhd
-- Project Name:  Ripple_Carry_Adder
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: Ripple_Carry_addsub
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY Ripple_Carry_addsub_tb IS
END Ripple_Carry_addsub_tb;
 
ARCHITECTURE behavior OF Ripple_Carry_addsub_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT Ripple_Carry_addsub
    PORT(
         x : IN  std_logic_vector(7 downto 0);
         y : IN  std_logic_vector(7 downto 0);
         sub : IN  std_logic;
         cout : OUT  std_logic;
         s : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal x : std_logic_vector(7 downto 0) := (others => '0');
   signal y : std_logic_vector(7 downto 0) := (others => '0');
   signal sub : std_logic := '0';

 	--Outputs
   signal cout : std_logic;
   signal s : std_logic_vector(7 downto 0);

BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: Ripple_Carry_addsub PORT MAP (
          x => x,
          y => y,
          sub => sub,
          cout => cout,
          s => s
        );

 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

      wait for 50 ns;
		
		x<="00010110"; --22
		y<="00010110"; --22
		sub <='1';
		  
		wait for 50 ns;
		
		x<="00010110";  --22
		y<="00001111";  --15
		sub <='1';
	   wait for 50 ns;
		
		x<="00101111";  --15
		y<="00010110";  --22
		sub <='0';
		
 		wait for 50 ns;

      -- insert stimulus here 

      wait;
   end process;

END;
