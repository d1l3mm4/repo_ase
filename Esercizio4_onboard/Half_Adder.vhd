----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    17:01:50 01/15/2020 
-- Design Name: 
-- Module Name:    Half_Adder - Dataflow 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Half_Adder is

    Port ( x : in  STD_LOGIC;
           y : in  STD_LOGIC; 		-- x e y sono i due ingressi
           cout : out  STD_LOGIC;	-- Riporto in uscita
           s : out  STD_LOGIC);		-- Risultato della somma
			  
end Half_Adder;

architecture Dataflow of Half_Adder is

	begin
	
		s<= x XOR y;						-- Uscita � alta se lo � solo uno dei due ingressi
		cout<= x AND y;					-- Riporto � alto se i due ingressi sono alti contemporaneamente

end Dataflow;

