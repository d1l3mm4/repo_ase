----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    12:21:31 01/16/2020 
-- Design Name: 
-- Module Name:    register_n - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity register_n is
	 Generic(
			  N : natural
	 );
    Port ( clock : in  STD_LOGIC;
           reset_n : in  STD_LOGIC;
           value : in  STD_LOGIC_VECTOR(N-1 downto 0); --quanti bit posso rappresentare a 
																		  --prescindere dai bit
           enable : in  STD_LOGIC;
           output : out  STD_LOGIC_VECTOR(N-1 downto 0)
	  );
end register_n;

architecture Behavioral of register_n is

signal registro : STD_LOGIC_VECTOR(N-1 downto 0);

begin

R: process (value, enable, reset_n, clock)
	begin
		if (reset_n = '1') then
			registro <= ( others => '0');
		elsif (rising_edge(clock) and enable = '1') then
			registro <= value;
		end if;
end process;

output <= registro;
end Behavioral;

