----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    13:42:20 01/16/2020 
-- Design Name: 
-- Module Name:    SS_onboard - Structural 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity SS_onboard is
    Port ( CLOCK : in  STD_LOGIC;
           SHIFT : in  STD_LOGIC;
           LOAD : in  STD_LOGIC;
           RESET : in  STD_LOGIC;
           SW : in  STD_LOGIC_VECTOR(7 downto 0);
           REG : out  STD_LOGIC_VECTOR(7 downto 0));
end SS_onboard;

architecture Structural of SS_onboard is

component clock_filter
	 Generic (
			  --frequenza in ingresso (leggere sull'ucf)
			  clock_freq_in : integer := 1000000000;
			  --frequenza in uscita
			  clock_freq_out : integer := 100000000
			  );
    Port ( clock_in : in  STD_LOGIC;
           reset : in  STD_LOGIC;
           clock_out : out  STD_LOGIC);
end component;


component Register_N
	 Generic(
			  N : natural
	 );
    Port ( clock : in  STD_LOGIC;
           reset_n : in  STD_LOGIC;
           value : in  STD_LOGIC_VECTOR(N-1 downto 0); --quanti bit posso rappresentare a 
																		  --prescindere dai bit
           enable : in  STD_LOGIC;
           output : out  STD_LOGIC_VECTOR(N-1 downto 0)
	  );
end component;

component Control_Unit
    Port ( 
           load_in : in  STD_LOGIC;
           reset_in : in  STD_LOGIC;
           shift_in : in  STD_LOGIC;
           clock : in  STD_LOGIC;
			  shift_out: out STD_LOGIC;
			  reset_out: out STD_LOGIC;
			  load_out: out STD_LOGIC;
			  enable: out STD_LOGIC);
end component;

component Shift_Register
    Port ( data_in : in  STD_LOGIC_VECTOR(7 downto 0);
			  clock: in STD_LOGIC;
			  reset: in STD_LOGIC;
           shift : in  STD_LOGIC;
           enable : in  STD_LOGIC;
           data_out : out  STD_LOGIC_VECTOR(7 downto 0));
end component;

signal shift_reg: std_logic;
signal en: std_logic;
signal ingresso_par: std_logic_vector(7 downto 0);
signal to_led: std_logic_vector(7 downto 0);
signal load_out: std_logic;
signal res: std_logic;
signal clk2: std_logic;


begin

reg <= to_led;


CF: clock_filter
generic map( 100000000,
				 10)
port map(clock,
			reset,
			clk2);
			



regn: register_n
generic map(8)
port map(clk2,
			res,
			sw,
			load_out,
			ingresso_par);
			
CU: Control_Unit
port map(			load,
			reset,
			shift,
			clk2,
			shift_reg,
			res,
			load_out,
			en);

SR: Shift_Register
port map (ingresso_par,
			 clk2,
			 res,
			 shift_reg,
			 en,
			 to_led);
			 
			

end Structural;

