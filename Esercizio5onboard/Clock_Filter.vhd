----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    20:39:59 01/16/2020 
-- Design Name: 
-- Module Name:    Clock_Filter - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

--� un filtro, filtra l'ingresso del clock per avere in uscita una frequenza un po' 
--piu bassa. Io non posso mettere in ingresso ad un contatore un clock e il suo valore 
--di counter it usato dal suo stesso componente. 
--Invece, vorrei prendere un contatore alimentato dal clock, il valore che questo 
--counter deve uscire, lo uso come abilitazione degli elementi di memoria, siano 
--contatori o registri, in maniera tale che se avessi un contatore, come in questo 
--caso, io do in ingresso un segnale di enable e lo stesso clock di questo divisore 
--di frequenza.
--Il ruolo di questo contatore, non ci deve dare un counter it che sia un clock in 
--ingresso scalato in frequenza, ma voglio avere la stessa durata dell'impulso 
--(ovvero mezzo periodo del clock alto), in modo tale che questo segnale, sia 
--effettivamente rispetto al clock di ingresso un clock filtrato.
--Il valore di conteggio mi da un enable che rispetto al clock di ingresso
--...fa avere a questo valore un solo fronte... 
--avr� che questo contatore � stimolato dallo stesso clock di frequenza elevata ma 
--viene abilitato solamente per un tempo che � esattamente pari alla durata del duty
--cicle, ma gli eventi di abilitazione 
--uso il fine conteggio come enable arrivano con una frequenza divisa da questo valore
--di conteggio
--il clock sincronizza la macchina

entity clock_filter is
	 --le frequenze sono generic perch� possono cambiare in base alle board
	 Generic (
			  --frequenza in ingresso (leggere sull'ucf)
			  clock_freq_in : integer := 1000000000;
			  --frequenza in uscita
			  clock_freq_out : integer := 100000000
			  );
    Port ( clock_in : in  STD_LOGIC;
           reset : in  STD_LOGIC;
           clock_out : out  STD_LOGIC);
end clock_filter;

architecture Behavioral of clock_filter is

signal  end_count : STD_LOGIC := '0';

constant max_value_count : integer := clock_freq_in/(clock_freq_out)-1;

begin

clock_out <= end_count;

divisore_frequenza : process (clock_in, reset)
--devo contare fin quando non arrivo al valore massimo di conteggio
variable count : integer range 0 to max_value_count := 0; 
begin 
		--se il reset � alto azzero tutto
		if reset = '1' then
			end_count <= '0';
			count := 0;
		--se ho un fronte di salita del clock
		elsif clock_in'event and clock_in = '1' then
			--se sono arrivato al valore massimo di conteggio allora alzo la fine del conteggio
			--e azzero il conteggio
			if count = max_value_count then
				end_count <= '1';
				count := 0;
			--altrimenti continuo il ciclo e incremento il conteggio
			else 
				end_count <= '0';
				count := count+1;
			end if;
		end if;
end process;
				

end Behavioral;

