----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:23:26 01/16/2020 
-- Design Name: 
-- Module Name:    SS_cell - Structural 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity SS_cell is
port (enable: in std_logic;
		clock: in std_logic;
		d_par: in std_logic;
		d_ser: in std_logic;
		reset: in std_logic;
		shift: in std_logic;
		d_out: out std_logic);
end SS_cell;

architecture structural of SS_cell is

component ff_d_beh   
Port ( d : in  STD_LOGIC;
           enable : in  STD_LOGIC;
           reset : in  STD_LOGIC;
           clock : in  STD_LOGIC;
           q : out  STD_LOGIC);
end component;

component MUX_21
    Port ( a : in  STD_LOGIC;
           b : in  STD_LOGIC;
           c : in  STD_LOGIC;
           z : out  STD_LOGIC);
end component;

signal mux_out : std_logic;

begin

FF: FF_D_BEH
port map(mux_out,
			enable,
			reset,
			clock,
			d_out);
			
mux: MUX_21 --se il segnale di shift � 0 prendo l'ingresso parallelo, se il segnale di shift � 1 prendo l'ingresso seriale
port map (d_par,
			 d_ser,
			 shift,
			 mux_out);
			 

end Structural;

