----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:41:03 01/16/2020 
-- Design Name: 
-- Module Name:    Control_Unit - Structural 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Control_Unit is
    Port ( load_in : in  STD_LOGIC;
           reset_in : in  STD_LOGIC;
           shift_in : in  STD_LOGIC;
           clock : in  STD_LOGIC;
			  shift_out: out STD_LOGIC;
			  reset_out: out STD_LOGIC;
			  load_out: out STD_LOGIC;
			  enable: out STD_LOGIC);
end Control_Unit;

architecture Behavioural of Control_Unit is



type state is (res, load, shift, idle);

signal current_state, next_state: state := res;

begin

		

proc1: process(reset_in, clock)
begin
if(reset_in='1') then
current_state <= res;
elsif (clock'event and clock='1') then
current_state<= next_state;
end if;

end process;

proc2: process(current_state, reset_in, load_in, shift_in)
begin


case current_state is

when res => shift_out <='0';
				reset_out <='1';
				enable <='0';
				load_out <='1';
				next_state <= load;

when load => enable <='1';
				 reset_out <='0';
				 shift_out <='0';
				 load_out<='1';
				 if(load_in='1') then
				 next_state <= shift;
				 else next_state <= load;
				 end if;
				
				 
when shift => shift_out <='1';
				 load_out <='0';
				 enable <= '0';
				 reset_out <='0';
				 if(shift_in='1') then
				 next_state <= idle;
				 else next_state <= shift;
				 end if;
				 
when idle => enable <='1';
				 load_out <= '0';
				 shift_out <='1';
				 reset_out <='0';
				 next_state <= shift;

end case;

end process;

end Behavioural;

