----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    17:33:19 11/15/2019 
-- Design Name: 
-- Module Name:    Ripple_Carry - structural 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Ripple_Carry_4b is
    Port ( x : in  STD_LOGIC_VECTOR(3 downto 0);
           y : in  STD_LOGIC_VECTOR(3 downto 0);
           cin : in  STD_LOGIC;
           cout : out  STD_LOGIC;
           s : out  STD_LOGIC_VECTOR(3 downto 0));
end Ripple_Carry_4b;

architecture structural of Ripple_Carry_4b is

component Full_Adder
port( 		x : in  STD_LOGIC;
           y : in  STD_LOGIC;
           cin : in  STD_LOGIC;
           cout : out  STD_LOGIC;
           s : out  STD_LOGIC
			  );
end component;

signal c1, c2, c3: std_logic;

begin

fa0: Full_adder
port map ( x => x(0),
				y => y(0),
				cin => cin,
				cout =>c1,
				s=>s(0)
				);

fa1: Full_adder
port map ( x => x(1),
				y => y(1),
				cin => c1,
				cout =>c2,
				s=>s(1)
				);

fa2: Full_adder
port map ( x => x(2),
				y => y(2),
				cin => c2,
				cout =>c3,
				s=>s(2)
				);

fa3: Full_adder
port map ( x => x(3),
				y => y(3),
				cin => c3,
				cout =>cout,
				s=>s(3)
				);
				
end structural;

