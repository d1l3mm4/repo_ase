----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:29:36 11/15/2019 
-- Design Name: 
-- Module Name:    Full_Adder - Structural 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Full_Adder is
    Port ( x : in  STD_LOGIC;
           y : in  STD_LOGIC;
           cin : in  STD_LOGIC;
           cout : out  STD_LOGIC;
           s : out  STD_LOGIC);
end Full_Adder;

architecture Structural of Full_Adder is

component Half_Adder
 Port ( x : in  STD_LOGIC;
           y : in  STD_LOGIC; 		-- x e y sono i due ingressi
           cout : out  STD_LOGIC;	-- Riporto in uscita
           s : out  STD_LOGIC);
end component;

signal hc, hs, tc: STD_LOGIC;

begin

ha1: Half_Adder port map (
x => x,
y =>y,
cout =>hc,
s => hs);

ha2: Half_Adder port map (
x=>hs,
y=>cin,
cout =>tc,
s=> s);

cout <=  tc OR hc;


end Structural;

