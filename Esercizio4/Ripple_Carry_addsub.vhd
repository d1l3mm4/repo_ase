----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    17:33:19 11/15/2019 
-- Design Name: 
-- Module Name:    Ripple_Carry - structural 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Ripple_Carry_addsub is
    Port ( x : in  STD_LOGIC_VECTOR(7 downto 0);
           y : in  STD_LOGIC_VECTOR(7 downto 0);
           sub : in  STD_LOGIC;
           cout : out  STD_LOGIC;
           s : out  STD_LOGIC_VECTOR(7 downto 0));
end Ripple_Carry_addsub;

architecture structural of Ripple_Carry_addsub is

component Ripple_Carry_8b
    Port ( x : in  STD_LOGIC_VECTOR(7 downto 0);
           y : in  STD_LOGIC_VECTOR(7 downto 0);
           cin : in  STD_LOGIC;
           cout : out  STD_LOGIC;
           s : out  STD_LOGIC_VECTOR(7 downto 0));
end component;

signal nY: std_logic_vector (7 downto 0); 
signal sub2: std_logic_vector(7 downto 0);

begin

sub2 <= "00000000" when sub='0' else "11111111" when sub='1' else "--------";

nY<= y xor sub2;

rc: Ripple_Carry_8b
port map (
				x => x,
				y => nY,
				cin => sub,
				cout =>cout,
				s =>s);

end structural;

