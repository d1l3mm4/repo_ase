----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    21:40:15 01/14/2020 
-- Design Name: 
-- Module Name:    Display_onboard - Structural 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Display_onboard is
    Port ( CLOCK : in  STD_LOGIC;
           LOAD : in  STD_LOGIC;
           DATA_IN : in  STD_LOGIC_VECTOR(3 downto 0);
           RESET : in  STD_LOGIC;
           ANODES : out  STD_LOGIC_VECTOR(7 downto 0);
           CATHODES : out  STD_LOGIC_VECTOR(7 downto 0));
end Display_onboard;

architecture Structural of Display_onboard is

component Display
	 Generic (
			  --frequenza in ingresso (leggere sull'ucf)
			  clock_freq_in : integer := 100000000;
			  --frequenza in uscita
			  clock_freq_out : integer := 5000000
			  );
    Port ( value : in  STD_LOGIC_VECTOR (15 downto 0);
			  reset: in STD_LOGIC;
			  clk: in STD_LOGIC;
           dots : in  STD_LOGIC_VECTOR(7 downto 0);
			  enable: in STD_LOGIC_VECTOR(7 downto 0);
           cathodes : out  STD_LOGIC_VECTOR(7 downto 0);
           anodes : out  STD_LOGIC_VECTOR(7 downto 0));
end component;

component Control_Unit
    Port ( load : in  STD_LOGIC;
           data_in : in  STD_LOGIC_VECTOR(3 downto 0);
           reset : in  STD_LOGIC;
           clock : in  STD_LOGIC;
           data_out : out  STD_LOGIC_VECTOR(15 downto 0));
end component;

signal value: std_logic_vector(15 downto 0);

begin

CU: Control_Unit
port map(load,
			data_in,
			reset,
			clock,
			value);
			
D: Display
generic map(100000000,
				10000)
port map(value,
			reset,
			clock,
			x"00",
			"00001111",
			cathodes,
			anodes);
			


end Structural;

