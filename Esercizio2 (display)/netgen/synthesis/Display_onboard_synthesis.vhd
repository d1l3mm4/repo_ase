--------------------------------------------------------------------------------
-- Copyright (c) 1995-2012 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Version: P.58f
--  \   \         Application: netgen
--  /   /         Filename: Display_onboard_synthesis.vhd
-- /___/   /\     Timestamp: Wed Jan 15 10:37:06 2020
-- \   \  /  \ 
--  \___\/\___\
--             
-- Command	: -intstyle ise -ar Structure -tm Display_onboard -w -dir netgen/synthesis -ofmt vhdl -sim Display_onboard.ngc Display_onboard_synthesis.vhd 
-- Device	: xc7a100t-3-csg324
-- Input file	: Display_onboard.ngc
-- Output file	: C:\Users\annal\Xilinx\Display\netgen\synthesis\Display_onboard_synthesis.vhd
-- # of Entities	: 1
-- Design Name	: Display_onboard
-- Xilinx	: C:\Xilinx\14.5\ISE_DS\ISE\
--             
-- Purpose:    
--     This VHDL netlist is a verification model and uses simulation 
--     primitives which may not represent the true implementation of the 
--     device, however the netlist is functionally correct and should not 
--     be modified. This file cannot be synthesized and should only be used 
--     with supported simulation tools.
--             
-- Reference:  
--     Command Line Tools User Guide, Chapter 23
--     Synthesis and Simulation Design Guide, Chapter 6
--             
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
use UNISIM.VPKG.ALL;

entity Display_onboard is
  port (
    CLOCK : in STD_LOGIC := 'X'; 
    LOAD : in STD_LOGIC := 'X'; 
    RESET : in STD_LOGIC := 'X'; 
    DATA_IN : in STD_LOGIC_VECTOR ( 3 downto 0 ); 
    ANODES : out STD_LOGIC_VECTOR ( 7 downto 0 ); 
    CATHODES : out STD_LOGIC_VECTOR ( 7 downto 0 ) 
  );
end Display_onboard;

architecture Structure of Display_onboard is
  signal DATA_IN_3_IBUF_0 : STD_LOGIC; 
  signal DATA_IN_2_IBUF_1 : STD_LOGIC; 
  signal DATA_IN_1_IBUF_2 : STD_LOGIC; 
  signal DATA_IN_0_IBUF_3 : STD_LOGIC; 
  signal CLOCK_BUFGP_4 : STD_LOGIC; 
  signal LOAD_IBUF_5 : STD_LOGIC; 
  signal RESET_IBUF_6 : STD_LOGIC; 
  signal CATHODES_6_OBUF_11 : STD_LOGIC; 
  signal CATHODES_5_OBUF_12 : STD_LOGIC; 
  signal CATHODES_4_OBUF_13 : STD_LOGIC; 
  signal CATHODES_3_OBUF_14 : STD_LOGIC; 
  signal CATHODES_2_OBUF_15 : STD_LOGIC; 
  signal CATHODES_1_OBUF_16 : STD_LOGIC; 
  signal CATHODES_0_OBUF_17 : STD_LOGIC; 
  signal ANODES_3_OBUF_18 : STD_LOGIC; 
  signal ANODES_2_OBUF_19 : STD_LOGIC; 
  signal ANODES_1_OBUF_20 : STD_LOGIC; 
  signal ANODES_0_OBUF_21 : STD_LOGIC; 
  signal ANODES_7_OBUF_22 : STD_LOGIC; 
  signal N1 : STD_LOGIC; 
  signal D_clk_filter_end_count_24 : STD_LOGIC; 
  signal D_counter_count_1_GND_11_o_mux_3_OUT_0_Q : STD_LOGIC; 
  signal D_counter_count_1_GND_11_o_mux_3_OUT_1_Q : STD_LOGIC; 
  signal D_clk_filter_PWR_12_o_divisore_frequenza_count_13_equal_1_o : STD_LOGIC; 
  signal D_clk_filter_Mcount_divisore_frequenza_count_eqn_0 : STD_LOGIC; 
  signal D_clk_filter_Mcount_divisore_frequenza_count_eqn_1 : STD_LOGIC; 
  signal D_clk_filter_Mcount_divisore_frequenza_count_eqn_2 : STD_LOGIC; 
  signal D_clk_filter_Mcount_divisore_frequenza_count_eqn_3 : STD_LOGIC; 
  signal D_clk_filter_Mcount_divisore_frequenza_count_eqn_4 : STD_LOGIC; 
  signal D_clk_filter_Mcount_divisore_frequenza_count_eqn_5 : STD_LOGIC; 
  signal D_clk_filter_Mcount_divisore_frequenza_count_eqn_6 : STD_LOGIC; 
  signal D_clk_filter_Mcount_divisore_frequenza_count_eqn_7 : STD_LOGIC; 
  signal D_clk_filter_Mcount_divisore_frequenza_count_eqn_8 : STD_LOGIC; 
  signal D_clk_filter_Mcount_divisore_frequenza_count_eqn_9 : STD_LOGIC; 
  signal D_clk_filter_Mcount_divisore_frequenza_count_eqn_10 : STD_LOGIC; 
  signal D_clk_filter_Mcount_divisore_frequenza_count_eqn_11 : STD_LOGIC; 
  signal D_clk_filter_Mcount_divisore_frequenza_count_eqn_12 : STD_LOGIC; 
  signal D_clk_filter_Mcount_divisore_frequenza_count_eqn_13 : STD_LOGIC; 
  signal D_clk_filter_PWR_12_o_divisore_frequenza_count_13_equal_1_o_13_Q : STD_LOGIC; 
  signal D_clk_filter_PWR_12_o_divisore_frequenza_count_13_equal_1_o_13_1_87 : STD_LOGIC; 
  signal D_clk_filter_Mcount_divisore_frequenza_count_cy_1_rt_111 : STD_LOGIC; 
  signal D_clk_filter_Mcount_divisore_frequenza_count_cy_2_rt_112 : STD_LOGIC; 
  signal D_clk_filter_Mcount_divisore_frequenza_count_cy_3_rt_113 : STD_LOGIC; 
  signal D_clk_filter_Mcount_divisore_frequenza_count_cy_4_rt_114 : STD_LOGIC; 
  signal D_clk_filter_Mcount_divisore_frequenza_count_cy_5_rt_115 : STD_LOGIC; 
  signal D_clk_filter_Mcount_divisore_frequenza_count_cy_6_rt_116 : STD_LOGIC; 
  signal D_clk_filter_Mcount_divisore_frequenza_count_cy_7_rt_117 : STD_LOGIC; 
  signal D_clk_filter_Mcount_divisore_frequenza_count_cy_8_rt_118 : STD_LOGIC; 
  signal D_clk_filter_Mcount_divisore_frequenza_count_cy_9_rt_119 : STD_LOGIC; 
  signal D_clk_filter_Mcount_divisore_frequenza_count_cy_10_rt_120 : STD_LOGIC; 
  signal D_clk_filter_Mcount_divisore_frequenza_count_cy_11_rt_121 : STD_LOGIC; 
  signal D_clk_filter_Mcount_divisore_frequenza_count_cy_12_rt_122 : STD_LOGIC; 
  signal D_clk_filter_Mcount_divisore_frequenza_count_xor_13_rt_123 : STD_LOGIC; 
  signal CU_data : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal D_counter_count : STD_LOGIC_VECTOR ( 1 downto 0 ); 
  signal D_clk_filter_divisore_frequenza_count : STD_LOGIC_VECTOR ( 13 downto 0 ); 
  signal Result : STD_LOGIC_VECTOR ( 13 downto 0 ); 
  signal D_clk_filter_Mcount_divisore_frequenza_count_lut : STD_LOGIC_VECTOR ( 0 downto 0 ); 
  signal D_clk_filter_Mcount_divisore_frequenza_count_cy : STD_LOGIC_VECTOR ( 12 downto 0 ); 
begin
  XST_VCC : VCC
    port map (
      P => ANODES_7_OBUF_22
    );
  XST_GND : GND
    port map (
      G => N1
    );
  CU_data_3 : FDCE
    port map (
      C => CLOCK_BUFGP_4,
      CE => LOAD_IBUF_5,
      CLR => RESET_IBUF_6,
      D => DATA_IN_3_IBUF_0,
      Q => CU_data(3)
    );
  CU_data_2 : FDCE
    port map (
      C => CLOCK_BUFGP_4,
      CE => LOAD_IBUF_5,
      CLR => RESET_IBUF_6,
      D => DATA_IN_2_IBUF_1,
      Q => CU_data(2)
    );
  CU_data_1 : FDCE
    port map (
      C => CLOCK_BUFGP_4,
      CE => LOAD_IBUF_5,
      CLR => RESET_IBUF_6,
      D => DATA_IN_1_IBUF_2,
      Q => CU_data(1)
    );
  CU_data_0 : FDCE
    port map (
      C => CLOCK_BUFGP_4,
      CE => LOAD_IBUF_5,
      CLR => RESET_IBUF_6,
      D => DATA_IN_0_IBUF_3,
      Q => CU_data(0)
    );
  D_counter_count_1 : FDCE
    generic map(
      INIT => '0'
    )
    port map (
      C => CLOCK_BUFGP_4,
      CE => D_clk_filter_end_count_24,
      CLR => RESET_IBUF_6,
      D => D_counter_count_1_GND_11_o_mux_3_OUT_1_Q,
      Q => D_counter_count(1)
    );
  D_counter_count_0 : FDCE
    generic map(
      INIT => '0'
    )
    port map (
      C => CLOCK_BUFGP_4,
      CE => D_clk_filter_end_count_24,
      CLR => RESET_IBUF_6,
      D => D_counter_count_1_GND_11_o_mux_3_OUT_0_Q,
      Q => D_counter_count(0)
    );
  D_clk_filter_end_count : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => CLOCK_BUFGP_4,
      CLR => RESET_IBUF_6,
      D => D_clk_filter_PWR_12_o_divisore_frequenza_count_13_equal_1_o,
      Q => D_clk_filter_end_count_24
    );
  D_clk_filter_divisore_frequenza_count_0 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => CLOCK_BUFGP_4,
      CLR => RESET_IBUF_6,
      D => D_clk_filter_Mcount_divisore_frequenza_count_eqn_0,
      Q => D_clk_filter_divisore_frequenza_count(0)
    );
  D_clk_filter_divisore_frequenza_count_1 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => CLOCK_BUFGP_4,
      CLR => RESET_IBUF_6,
      D => D_clk_filter_Mcount_divisore_frequenza_count_eqn_1,
      Q => D_clk_filter_divisore_frequenza_count(1)
    );
  D_clk_filter_divisore_frequenza_count_2 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => CLOCK_BUFGP_4,
      CLR => RESET_IBUF_6,
      D => D_clk_filter_Mcount_divisore_frequenza_count_eqn_2,
      Q => D_clk_filter_divisore_frequenza_count(2)
    );
  D_clk_filter_divisore_frequenza_count_3 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => CLOCK_BUFGP_4,
      CLR => RESET_IBUF_6,
      D => D_clk_filter_Mcount_divisore_frequenza_count_eqn_3,
      Q => D_clk_filter_divisore_frequenza_count(3)
    );
  D_clk_filter_divisore_frequenza_count_4 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => CLOCK_BUFGP_4,
      CLR => RESET_IBUF_6,
      D => D_clk_filter_Mcount_divisore_frequenza_count_eqn_4,
      Q => D_clk_filter_divisore_frequenza_count(4)
    );
  D_clk_filter_divisore_frequenza_count_5 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => CLOCK_BUFGP_4,
      CLR => RESET_IBUF_6,
      D => D_clk_filter_Mcount_divisore_frequenza_count_eqn_5,
      Q => D_clk_filter_divisore_frequenza_count(5)
    );
  D_clk_filter_divisore_frequenza_count_6 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => CLOCK_BUFGP_4,
      CLR => RESET_IBUF_6,
      D => D_clk_filter_Mcount_divisore_frequenza_count_eqn_6,
      Q => D_clk_filter_divisore_frequenza_count(6)
    );
  D_clk_filter_divisore_frequenza_count_7 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => CLOCK_BUFGP_4,
      CLR => RESET_IBUF_6,
      D => D_clk_filter_Mcount_divisore_frequenza_count_eqn_7,
      Q => D_clk_filter_divisore_frequenza_count(7)
    );
  D_clk_filter_divisore_frequenza_count_8 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => CLOCK_BUFGP_4,
      CLR => RESET_IBUF_6,
      D => D_clk_filter_Mcount_divisore_frequenza_count_eqn_8,
      Q => D_clk_filter_divisore_frequenza_count(8)
    );
  D_clk_filter_divisore_frequenza_count_9 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => CLOCK_BUFGP_4,
      CLR => RESET_IBUF_6,
      D => D_clk_filter_Mcount_divisore_frequenza_count_eqn_9,
      Q => D_clk_filter_divisore_frequenza_count(9)
    );
  D_clk_filter_divisore_frequenza_count_10 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => CLOCK_BUFGP_4,
      CLR => RESET_IBUF_6,
      D => D_clk_filter_Mcount_divisore_frequenza_count_eqn_10,
      Q => D_clk_filter_divisore_frequenza_count(10)
    );
  D_clk_filter_divisore_frequenza_count_11 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => CLOCK_BUFGP_4,
      CLR => RESET_IBUF_6,
      D => D_clk_filter_Mcount_divisore_frequenza_count_eqn_11,
      Q => D_clk_filter_divisore_frequenza_count(11)
    );
  D_clk_filter_divisore_frequenza_count_12 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => CLOCK_BUFGP_4,
      CLR => RESET_IBUF_6,
      D => D_clk_filter_Mcount_divisore_frequenza_count_eqn_12,
      Q => D_clk_filter_divisore_frequenza_count(12)
    );
  D_clk_filter_divisore_frequenza_count_13 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => CLOCK_BUFGP_4,
      CLR => RESET_IBUF_6,
      D => D_clk_filter_Mcount_divisore_frequenza_count_eqn_13,
      Q => D_clk_filter_divisore_frequenza_count(13)
    );
  D_clk_filter_Mcount_divisore_frequenza_count_cy_0_Q : MUXCY
    port map (
      CI => N1,
      DI => ANODES_7_OBUF_22,
      S => D_clk_filter_Mcount_divisore_frequenza_count_lut(0),
      O => D_clk_filter_Mcount_divisore_frequenza_count_cy(0)
    );
  D_clk_filter_Mcount_divisore_frequenza_count_xor_0_Q : XORCY
    port map (
      CI => N1,
      LI => D_clk_filter_Mcount_divisore_frequenza_count_lut(0),
      O => Result(0)
    );
  D_clk_filter_Mcount_divisore_frequenza_count_cy_1_Q : MUXCY
    port map (
      CI => D_clk_filter_Mcount_divisore_frequenza_count_cy(0),
      DI => N1,
      S => D_clk_filter_Mcount_divisore_frequenza_count_cy_1_rt_111,
      O => D_clk_filter_Mcount_divisore_frequenza_count_cy(1)
    );
  D_clk_filter_Mcount_divisore_frequenza_count_xor_1_Q : XORCY
    port map (
      CI => D_clk_filter_Mcount_divisore_frequenza_count_cy(0),
      LI => D_clk_filter_Mcount_divisore_frequenza_count_cy_1_rt_111,
      O => Result(1)
    );
  D_clk_filter_Mcount_divisore_frequenza_count_cy_2_Q : MUXCY
    port map (
      CI => D_clk_filter_Mcount_divisore_frequenza_count_cy(1),
      DI => N1,
      S => D_clk_filter_Mcount_divisore_frequenza_count_cy_2_rt_112,
      O => D_clk_filter_Mcount_divisore_frequenza_count_cy(2)
    );
  D_clk_filter_Mcount_divisore_frequenza_count_xor_2_Q : XORCY
    port map (
      CI => D_clk_filter_Mcount_divisore_frequenza_count_cy(1),
      LI => D_clk_filter_Mcount_divisore_frequenza_count_cy_2_rt_112,
      O => Result(2)
    );
  D_clk_filter_Mcount_divisore_frequenza_count_cy_3_Q : MUXCY
    port map (
      CI => D_clk_filter_Mcount_divisore_frequenza_count_cy(2),
      DI => N1,
      S => D_clk_filter_Mcount_divisore_frequenza_count_cy_3_rt_113,
      O => D_clk_filter_Mcount_divisore_frequenza_count_cy(3)
    );
  D_clk_filter_Mcount_divisore_frequenza_count_xor_3_Q : XORCY
    port map (
      CI => D_clk_filter_Mcount_divisore_frequenza_count_cy(2),
      LI => D_clk_filter_Mcount_divisore_frequenza_count_cy_3_rt_113,
      O => Result(3)
    );
  D_clk_filter_Mcount_divisore_frequenza_count_cy_4_Q : MUXCY
    port map (
      CI => D_clk_filter_Mcount_divisore_frequenza_count_cy(3),
      DI => N1,
      S => D_clk_filter_Mcount_divisore_frequenza_count_cy_4_rt_114,
      O => D_clk_filter_Mcount_divisore_frequenza_count_cy(4)
    );
  D_clk_filter_Mcount_divisore_frequenza_count_xor_4_Q : XORCY
    port map (
      CI => D_clk_filter_Mcount_divisore_frequenza_count_cy(3),
      LI => D_clk_filter_Mcount_divisore_frequenza_count_cy_4_rt_114,
      O => Result(4)
    );
  D_clk_filter_Mcount_divisore_frequenza_count_cy_5_Q : MUXCY
    port map (
      CI => D_clk_filter_Mcount_divisore_frequenza_count_cy(4),
      DI => N1,
      S => D_clk_filter_Mcount_divisore_frequenza_count_cy_5_rt_115,
      O => D_clk_filter_Mcount_divisore_frequenza_count_cy(5)
    );
  D_clk_filter_Mcount_divisore_frequenza_count_xor_5_Q : XORCY
    port map (
      CI => D_clk_filter_Mcount_divisore_frequenza_count_cy(4),
      LI => D_clk_filter_Mcount_divisore_frequenza_count_cy_5_rt_115,
      O => Result(5)
    );
  D_clk_filter_Mcount_divisore_frequenza_count_cy_6_Q : MUXCY
    port map (
      CI => D_clk_filter_Mcount_divisore_frequenza_count_cy(5),
      DI => N1,
      S => D_clk_filter_Mcount_divisore_frequenza_count_cy_6_rt_116,
      O => D_clk_filter_Mcount_divisore_frequenza_count_cy(6)
    );
  D_clk_filter_Mcount_divisore_frequenza_count_xor_6_Q : XORCY
    port map (
      CI => D_clk_filter_Mcount_divisore_frequenza_count_cy(5),
      LI => D_clk_filter_Mcount_divisore_frequenza_count_cy_6_rt_116,
      O => Result(6)
    );
  D_clk_filter_Mcount_divisore_frequenza_count_cy_7_Q : MUXCY
    port map (
      CI => D_clk_filter_Mcount_divisore_frequenza_count_cy(6),
      DI => N1,
      S => D_clk_filter_Mcount_divisore_frequenza_count_cy_7_rt_117,
      O => D_clk_filter_Mcount_divisore_frequenza_count_cy(7)
    );
  D_clk_filter_Mcount_divisore_frequenza_count_xor_7_Q : XORCY
    port map (
      CI => D_clk_filter_Mcount_divisore_frequenza_count_cy(6),
      LI => D_clk_filter_Mcount_divisore_frequenza_count_cy_7_rt_117,
      O => Result(7)
    );
  D_clk_filter_Mcount_divisore_frequenza_count_cy_8_Q : MUXCY
    port map (
      CI => D_clk_filter_Mcount_divisore_frequenza_count_cy(7),
      DI => N1,
      S => D_clk_filter_Mcount_divisore_frequenza_count_cy_8_rt_118,
      O => D_clk_filter_Mcount_divisore_frequenza_count_cy(8)
    );
  D_clk_filter_Mcount_divisore_frequenza_count_xor_8_Q : XORCY
    port map (
      CI => D_clk_filter_Mcount_divisore_frequenza_count_cy(7),
      LI => D_clk_filter_Mcount_divisore_frequenza_count_cy_8_rt_118,
      O => Result(8)
    );
  D_clk_filter_Mcount_divisore_frequenza_count_cy_9_Q : MUXCY
    port map (
      CI => D_clk_filter_Mcount_divisore_frequenza_count_cy(8),
      DI => N1,
      S => D_clk_filter_Mcount_divisore_frequenza_count_cy_9_rt_119,
      O => D_clk_filter_Mcount_divisore_frequenza_count_cy(9)
    );
  D_clk_filter_Mcount_divisore_frequenza_count_xor_9_Q : XORCY
    port map (
      CI => D_clk_filter_Mcount_divisore_frequenza_count_cy(8),
      LI => D_clk_filter_Mcount_divisore_frequenza_count_cy_9_rt_119,
      O => Result(9)
    );
  D_clk_filter_Mcount_divisore_frequenza_count_cy_10_Q : MUXCY
    port map (
      CI => D_clk_filter_Mcount_divisore_frequenza_count_cy(9),
      DI => N1,
      S => D_clk_filter_Mcount_divisore_frequenza_count_cy_10_rt_120,
      O => D_clk_filter_Mcount_divisore_frequenza_count_cy(10)
    );
  D_clk_filter_Mcount_divisore_frequenza_count_xor_10_Q : XORCY
    port map (
      CI => D_clk_filter_Mcount_divisore_frequenza_count_cy(9),
      LI => D_clk_filter_Mcount_divisore_frequenza_count_cy_10_rt_120,
      O => Result(10)
    );
  D_clk_filter_Mcount_divisore_frequenza_count_cy_11_Q : MUXCY
    port map (
      CI => D_clk_filter_Mcount_divisore_frequenza_count_cy(10),
      DI => N1,
      S => D_clk_filter_Mcount_divisore_frequenza_count_cy_11_rt_121,
      O => D_clk_filter_Mcount_divisore_frequenza_count_cy(11)
    );
  D_clk_filter_Mcount_divisore_frequenza_count_xor_11_Q : XORCY
    port map (
      CI => D_clk_filter_Mcount_divisore_frequenza_count_cy(10),
      LI => D_clk_filter_Mcount_divisore_frequenza_count_cy_11_rt_121,
      O => Result(11)
    );
  D_clk_filter_Mcount_divisore_frequenza_count_cy_12_Q : MUXCY
    port map (
      CI => D_clk_filter_Mcount_divisore_frequenza_count_cy(11),
      DI => N1,
      S => D_clk_filter_Mcount_divisore_frequenza_count_cy_12_rt_122,
      O => D_clk_filter_Mcount_divisore_frequenza_count_cy(12)
    );
  D_clk_filter_Mcount_divisore_frequenza_count_xor_12_Q : XORCY
    port map (
      CI => D_clk_filter_Mcount_divisore_frequenza_count_cy(11),
      LI => D_clk_filter_Mcount_divisore_frequenza_count_cy_12_rt_122,
      O => Result(12)
    );
  D_clk_filter_Mcount_divisore_frequenza_count_xor_13_Q : XORCY
    port map (
      CI => D_clk_filter_Mcount_divisore_frequenza_count_cy(12),
      LI => D_clk_filter_Mcount_divisore_frequenza_count_xor_13_rt_123,
      O => Result(13)
    );
  D_anodes_instance_n0016_0_1 : LUT2
    generic map(
      INIT => X"E"
    )
    port map (
      I0 => D_counter_count(1),
      I1 => D_counter_count(0),
      O => ANODES_0_OBUF_21
    );
  D_anodes_instance_n0016_1_1 : LUT2
    generic map(
      INIT => X"D"
    )
    port map (
      I0 => D_counter_count(0),
      I1 => D_counter_count(1),
      O => ANODES_1_OBUF_20
    );
  D_anodes_instance_n0016_2_1 : LUT2
    generic map(
      INIT => X"D"
    )
    port map (
      I0 => D_counter_count(1),
      I1 => D_counter_count(0),
      O => ANODES_2_OBUF_19
    );
  D_anodes_instance_n0016_3_1 : LUT2
    generic map(
      INIT => X"7"
    )
    port map (
      I0 => D_counter_count(1),
      I1 => D_counter_count(0),
      O => ANODES_3_OBUF_18
    );
  D_counter_Mmux_count_1_GND_11_o_mux_3_OUT21 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => D_counter_count(1),
      I1 => D_counter_count(0),
      O => D_counter_count_1_GND_11_o_mux_3_OUT_1_Q
    );
  D_cathodes_instance_Mram_cathodes_for_digit61 : LUT6
    generic map(
      INIT => X"FFFFFFFFFFFF0941"
    )
    port map (
      I0 => CU_data(1),
      I1 => CU_data(2),
      I2 => CU_data(3),
      I3 => CU_data(0),
      I4 => D_counter_count(0),
      I5 => D_counter_count(1),
      O => CATHODES_6_OBUF_11
    );
  D_cathodes_instance_Mram_cathodes_for_digit41 : LUT6
    generic map(
      INIT => X"0000001010111010"
    )
    port map (
      I0 => D_counter_count(0),
      I1 => D_counter_count(1),
      I2 => CU_data(0),
      I3 => CU_data(1),
      I4 => CU_data(2),
      I5 => CU_data(3),
      O => CATHODES_4_OBUF_13
    );
  D_cathodes_instance_Mram_cathodes_for_digit111 : LUT6
    generic map(
      INIT => X"1110001000101000"
    )
    port map (
      I0 => D_counter_count(0),
      I1 => D_counter_count(1),
      I2 => CU_data(2),
      I3 => CU_data(0),
      I4 => CU_data(1),
      I5 => CU_data(3),
      O => CATHODES_1_OBUF_16
    );
  D_cathodes_instance_Mram_cathodes_for_digit11 : LUT6
    generic map(
      INIT => X"0010100000010010"
    )
    port map (
      I0 => D_counter_count(0),
      I1 => D_counter_count(1),
      I2 => CU_data(0),
      I3 => CU_data(1),
      I4 => CU_data(2),
      I5 => CU_data(3),
      O => CATHODES_0_OBUF_17
    );
  D_cathodes_instance_Mram_cathodes_for_digit31 : LUT6
    generic map(
      INIT => X"1001000010000110"
    )
    port map (
      I0 => D_counter_count(0),
      I1 => D_counter_count(1),
      I2 => CU_data(0),
      I3 => CU_data(2),
      I4 => CU_data(1),
      I5 => CU_data(3),
      O => CATHODES_3_OBUF_14
    );
  D_cathodes_instance_Mram_cathodes_for_digit51 : LUT6
    generic map(
      INIT => X"0110000001010100"
    )
    port map (
      I0 => D_counter_count(0),
      I1 => D_counter_count(1),
      I2 => CU_data(3),
      I3 => CU_data(1),
      I4 => CU_data(0),
      I5 => CU_data(2),
      O => CATHODES_5_OBUF_12
    );
  D_cathodes_instance_Mram_cathodes_for_digit21 : LUT6
    generic map(
      INIT => X"1101000000000100"
    )
    port map (
      I0 => D_counter_count(0),
      I1 => D_counter_count(1),
      I2 => CU_data(0),
      I3 => CU_data(1),
      I4 => CU_data(2),
      I5 => CU_data(3),
      O => CATHODES_2_OBUF_15
    );
  D_clk_filter_PWR_12_o_divisore_frequenza_count_13_equal_1_o_13_1 : LUT6
    generic map(
      INIT => X"8000000000000000"
    )
    port map (
      I0 => D_clk_filter_divisore_frequenza_count(3),
      I1 => D_clk_filter_divisore_frequenza_count(2),
      I2 => D_clk_filter_divisore_frequenza_count(9),
      I3 => D_clk_filter_divisore_frequenza_count(8),
      I4 => D_clk_filter_divisore_frequenza_count(13),
      I5 => D_clk_filter_divisore_frequenza_count(10),
      O => D_clk_filter_PWR_12_o_divisore_frequenza_count_13_equal_1_o_13_Q
    );
  D_clk_filter_PWR_12_o_divisore_frequenza_count_13_equal_1_o_13_2 : LUT6
    generic map(
      INIT => X"0001000000000000"
    )
    port map (
      I0 => D_clk_filter_divisore_frequenza_count(4),
      I1 => D_clk_filter_divisore_frequenza_count(5),
      I2 => D_clk_filter_divisore_frequenza_count(6),
      I3 => D_clk_filter_divisore_frequenza_count(7),
      I4 => D_clk_filter_divisore_frequenza_count(1),
      I5 => D_clk_filter_divisore_frequenza_count(0),
      O => D_clk_filter_PWR_12_o_divisore_frequenza_count_13_equal_1_o_13_1_87
    );
  D_clk_filter_PWR_12_o_divisore_frequenza_count_13_equal_1_o_13_3 : LUT4
    generic map(
      INIT => X"1000"
    )
    port map (
      I0 => D_clk_filter_divisore_frequenza_count(11),
      I1 => D_clk_filter_divisore_frequenza_count(12),
      I2 => D_clk_filter_PWR_12_o_divisore_frequenza_count_13_equal_1_o_13_1_87,
      I3 => D_clk_filter_PWR_12_o_divisore_frequenza_count_13_equal_1_o_13_Q,
      O => D_clk_filter_PWR_12_o_divisore_frequenza_count_13_equal_1_o
    );
  DATA_IN_3_IBUF : IBUF
    port map (
      I => DATA_IN(3),
      O => DATA_IN_3_IBUF_0
    );
  DATA_IN_2_IBUF : IBUF
    port map (
      I => DATA_IN(2),
      O => DATA_IN_2_IBUF_1
    );
  DATA_IN_1_IBUF : IBUF
    port map (
      I => DATA_IN(1),
      O => DATA_IN_1_IBUF_2
    );
  DATA_IN_0_IBUF : IBUF
    port map (
      I => DATA_IN(0),
      O => DATA_IN_0_IBUF_3
    );
  LOAD_IBUF : IBUF
    port map (
      I => LOAD,
      O => LOAD_IBUF_5
    );
  RESET_IBUF : IBUF
    port map (
      I => RESET,
      O => RESET_IBUF_6
    );
  ANODES_7_OBUF : OBUF
    port map (
      I => ANODES_7_OBUF_22,
      O => ANODES(7)
    );
  ANODES_6_OBUF : OBUF
    port map (
      I => ANODES_7_OBUF_22,
      O => ANODES(6)
    );
  ANODES_5_OBUF : OBUF
    port map (
      I => ANODES_7_OBUF_22,
      O => ANODES(5)
    );
  ANODES_4_OBUF : OBUF
    port map (
      I => ANODES_7_OBUF_22,
      O => ANODES(4)
    );
  ANODES_3_OBUF : OBUF
    port map (
      I => ANODES_3_OBUF_18,
      O => ANODES(3)
    );
  ANODES_2_OBUF : OBUF
    port map (
      I => ANODES_2_OBUF_19,
      O => ANODES(2)
    );
  ANODES_1_OBUF : OBUF
    port map (
      I => ANODES_1_OBUF_20,
      O => ANODES(1)
    );
  ANODES_0_OBUF : OBUF
    port map (
      I => ANODES_0_OBUF_21,
      O => ANODES(0)
    );
  CATHODES_7_OBUF : OBUF
    port map (
      I => ANODES_7_OBUF_22,
      O => CATHODES(7)
    );
  CATHODES_6_OBUF : OBUF
    port map (
      I => CATHODES_6_OBUF_11,
      O => CATHODES(6)
    );
  CATHODES_5_OBUF : OBUF
    port map (
      I => CATHODES_5_OBUF_12,
      O => CATHODES(5)
    );
  CATHODES_4_OBUF : OBUF
    port map (
      I => CATHODES_4_OBUF_13,
      O => CATHODES(4)
    );
  CATHODES_3_OBUF : OBUF
    port map (
      I => CATHODES_3_OBUF_14,
      O => CATHODES(3)
    );
  CATHODES_2_OBUF : OBUF
    port map (
      I => CATHODES_2_OBUF_15,
      O => CATHODES(2)
    );
  CATHODES_1_OBUF : OBUF
    port map (
      I => CATHODES_1_OBUF_16,
      O => CATHODES(1)
    );
  CATHODES_0_OBUF : OBUF
    port map (
      I => CATHODES_0_OBUF_17,
      O => CATHODES(0)
    );
  D_clk_filter_Mcount_divisore_frequenza_count_cy_1_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => D_clk_filter_divisore_frequenza_count(1),
      O => D_clk_filter_Mcount_divisore_frequenza_count_cy_1_rt_111
    );
  D_clk_filter_Mcount_divisore_frequenza_count_cy_2_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => D_clk_filter_divisore_frequenza_count(2),
      O => D_clk_filter_Mcount_divisore_frequenza_count_cy_2_rt_112
    );
  D_clk_filter_Mcount_divisore_frequenza_count_cy_3_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => D_clk_filter_divisore_frequenza_count(3),
      O => D_clk_filter_Mcount_divisore_frequenza_count_cy_3_rt_113
    );
  D_clk_filter_Mcount_divisore_frequenza_count_cy_4_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => D_clk_filter_divisore_frequenza_count(4),
      O => D_clk_filter_Mcount_divisore_frequenza_count_cy_4_rt_114
    );
  D_clk_filter_Mcount_divisore_frequenza_count_cy_5_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => D_clk_filter_divisore_frequenza_count(5),
      O => D_clk_filter_Mcount_divisore_frequenza_count_cy_5_rt_115
    );
  D_clk_filter_Mcount_divisore_frequenza_count_cy_6_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => D_clk_filter_divisore_frequenza_count(6),
      O => D_clk_filter_Mcount_divisore_frequenza_count_cy_6_rt_116
    );
  D_clk_filter_Mcount_divisore_frequenza_count_cy_7_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => D_clk_filter_divisore_frequenza_count(7),
      O => D_clk_filter_Mcount_divisore_frequenza_count_cy_7_rt_117
    );
  D_clk_filter_Mcount_divisore_frequenza_count_cy_8_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => D_clk_filter_divisore_frequenza_count(8),
      O => D_clk_filter_Mcount_divisore_frequenza_count_cy_8_rt_118
    );
  D_clk_filter_Mcount_divisore_frequenza_count_cy_9_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => D_clk_filter_divisore_frequenza_count(9),
      O => D_clk_filter_Mcount_divisore_frequenza_count_cy_9_rt_119
    );
  D_clk_filter_Mcount_divisore_frequenza_count_cy_10_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => D_clk_filter_divisore_frequenza_count(10),
      O => D_clk_filter_Mcount_divisore_frequenza_count_cy_10_rt_120
    );
  D_clk_filter_Mcount_divisore_frequenza_count_cy_11_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => D_clk_filter_divisore_frequenza_count(11),
      O => D_clk_filter_Mcount_divisore_frequenza_count_cy_11_rt_121
    );
  D_clk_filter_Mcount_divisore_frequenza_count_cy_12_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => D_clk_filter_divisore_frequenza_count(12),
      O => D_clk_filter_Mcount_divisore_frequenza_count_cy_12_rt_122
    );
  D_clk_filter_Mcount_divisore_frequenza_count_xor_13_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => D_clk_filter_divisore_frequenza_count(13),
      O => D_clk_filter_Mcount_divisore_frequenza_count_xor_13_rt_123
    );
  D_clk_filter_Mcount_divisore_frequenza_count_eqn_131 : LUT5
    generic map(
      INIT => X"EFFF0000"
    )
    port map (
      I0 => D_clk_filter_divisore_frequenza_count(12),
      I1 => D_clk_filter_divisore_frequenza_count(11),
      I2 => D_clk_filter_PWR_12_o_divisore_frequenza_count_13_equal_1_o_13_1_87,
      I3 => D_clk_filter_PWR_12_o_divisore_frequenza_count_13_equal_1_o_13_Q,
      I4 => Result(13),
      O => D_clk_filter_Mcount_divisore_frequenza_count_eqn_13
    );
  D_clk_filter_Mcount_divisore_frequenza_count_eqn_121 : LUT5
    generic map(
      INIT => X"EFFF0000"
    )
    port map (
      I0 => D_clk_filter_divisore_frequenza_count(12),
      I1 => D_clk_filter_divisore_frequenza_count(11),
      I2 => D_clk_filter_PWR_12_o_divisore_frequenza_count_13_equal_1_o_13_1_87,
      I3 => D_clk_filter_PWR_12_o_divisore_frequenza_count_13_equal_1_o_13_Q,
      I4 => Result(12),
      O => D_clk_filter_Mcount_divisore_frequenza_count_eqn_12
    );
  D_clk_filter_Mcount_divisore_frequenza_count_eqn_111 : LUT5
    generic map(
      INIT => X"EFFF0000"
    )
    port map (
      I0 => D_clk_filter_divisore_frequenza_count(12),
      I1 => D_clk_filter_divisore_frequenza_count(11),
      I2 => D_clk_filter_PWR_12_o_divisore_frequenza_count_13_equal_1_o_13_1_87,
      I3 => D_clk_filter_PWR_12_o_divisore_frequenza_count_13_equal_1_o_13_Q,
      I4 => Result(11),
      O => D_clk_filter_Mcount_divisore_frequenza_count_eqn_11
    );
  D_clk_filter_Mcount_divisore_frequenza_count_eqn_101 : LUT5
    generic map(
      INIT => X"EFFF0000"
    )
    port map (
      I0 => D_clk_filter_divisore_frequenza_count(12),
      I1 => D_clk_filter_divisore_frequenza_count(11),
      I2 => D_clk_filter_PWR_12_o_divisore_frequenza_count_13_equal_1_o_13_1_87,
      I3 => D_clk_filter_PWR_12_o_divisore_frequenza_count_13_equal_1_o_13_Q,
      I4 => Result(10),
      O => D_clk_filter_Mcount_divisore_frequenza_count_eqn_10
    );
  D_clk_filter_Mcount_divisore_frequenza_count_eqn_91 : LUT5
    generic map(
      INIT => X"EFFF0000"
    )
    port map (
      I0 => D_clk_filter_divisore_frequenza_count(12),
      I1 => D_clk_filter_divisore_frequenza_count(11),
      I2 => D_clk_filter_PWR_12_o_divisore_frequenza_count_13_equal_1_o_13_1_87,
      I3 => D_clk_filter_PWR_12_o_divisore_frequenza_count_13_equal_1_o_13_Q,
      I4 => Result(9),
      O => D_clk_filter_Mcount_divisore_frequenza_count_eqn_9
    );
  D_clk_filter_Mcount_divisore_frequenza_count_eqn_81 : LUT5
    generic map(
      INIT => X"EFFF0000"
    )
    port map (
      I0 => D_clk_filter_divisore_frequenza_count(12),
      I1 => D_clk_filter_divisore_frequenza_count(11),
      I2 => D_clk_filter_PWR_12_o_divisore_frequenza_count_13_equal_1_o_13_1_87,
      I3 => D_clk_filter_PWR_12_o_divisore_frequenza_count_13_equal_1_o_13_Q,
      I4 => Result(8),
      O => D_clk_filter_Mcount_divisore_frequenza_count_eqn_8
    );
  D_clk_filter_Mcount_divisore_frequenza_count_eqn_71 : LUT5
    generic map(
      INIT => X"EFFF0000"
    )
    port map (
      I0 => D_clk_filter_divisore_frequenza_count(12),
      I1 => D_clk_filter_divisore_frequenza_count(11),
      I2 => D_clk_filter_PWR_12_o_divisore_frequenza_count_13_equal_1_o_13_1_87,
      I3 => D_clk_filter_PWR_12_o_divisore_frequenza_count_13_equal_1_o_13_Q,
      I4 => Result(7),
      O => D_clk_filter_Mcount_divisore_frequenza_count_eqn_7
    );
  D_clk_filter_Mcount_divisore_frequenza_count_eqn_61 : LUT5
    generic map(
      INIT => X"EFFF0000"
    )
    port map (
      I0 => D_clk_filter_divisore_frequenza_count(12),
      I1 => D_clk_filter_divisore_frequenza_count(11),
      I2 => D_clk_filter_PWR_12_o_divisore_frequenza_count_13_equal_1_o_13_1_87,
      I3 => D_clk_filter_PWR_12_o_divisore_frequenza_count_13_equal_1_o_13_Q,
      I4 => Result(6),
      O => D_clk_filter_Mcount_divisore_frequenza_count_eqn_6
    );
  D_clk_filter_Mcount_divisore_frequenza_count_eqn_51 : LUT5
    generic map(
      INIT => X"EFFF0000"
    )
    port map (
      I0 => D_clk_filter_divisore_frequenza_count(12),
      I1 => D_clk_filter_divisore_frequenza_count(11),
      I2 => D_clk_filter_PWR_12_o_divisore_frequenza_count_13_equal_1_o_13_1_87,
      I3 => D_clk_filter_PWR_12_o_divisore_frequenza_count_13_equal_1_o_13_Q,
      I4 => Result(5),
      O => D_clk_filter_Mcount_divisore_frequenza_count_eqn_5
    );
  D_clk_filter_Mcount_divisore_frequenza_count_eqn_41 : LUT5
    generic map(
      INIT => X"EFFF0000"
    )
    port map (
      I0 => D_clk_filter_divisore_frequenza_count(12),
      I1 => D_clk_filter_divisore_frequenza_count(11),
      I2 => D_clk_filter_PWR_12_o_divisore_frequenza_count_13_equal_1_o_13_1_87,
      I3 => D_clk_filter_PWR_12_o_divisore_frequenza_count_13_equal_1_o_13_Q,
      I4 => Result(4),
      O => D_clk_filter_Mcount_divisore_frequenza_count_eqn_4
    );
  D_clk_filter_Mcount_divisore_frequenza_count_eqn_31 : LUT5
    generic map(
      INIT => X"EFFF0000"
    )
    port map (
      I0 => D_clk_filter_divisore_frequenza_count(12),
      I1 => D_clk_filter_divisore_frequenza_count(11),
      I2 => D_clk_filter_PWR_12_o_divisore_frequenza_count_13_equal_1_o_13_1_87,
      I3 => D_clk_filter_PWR_12_o_divisore_frequenza_count_13_equal_1_o_13_Q,
      I4 => Result(3),
      O => D_clk_filter_Mcount_divisore_frequenza_count_eqn_3
    );
  D_clk_filter_Mcount_divisore_frequenza_count_eqn_21 : LUT5
    generic map(
      INIT => X"EFFF0000"
    )
    port map (
      I0 => D_clk_filter_divisore_frequenza_count(12),
      I1 => D_clk_filter_divisore_frequenza_count(11),
      I2 => D_clk_filter_PWR_12_o_divisore_frequenza_count_13_equal_1_o_13_1_87,
      I3 => D_clk_filter_PWR_12_o_divisore_frequenza_count_13_equal_1_o_13_Q,
      I4 => Result(2),
      O => D_clk_filter_Mcount_divisore_frequenza_count_eqn_2
    );
  D_clk_filter_Mcount_divisore_frequenza_count_eqn_14 : LUT5
    generic map(
      INIT => X"EFFF0000"
    )
    port map (
      I0 => D_clk_filter_divisore_frequenza_count(12),
      I1 => D_clk_filter_divisore_frequenza_count(11),
      I2 => D_clk_filter_PWR_12_o_divisore_frequenza_count_13_equal_1_o_13_1_87,
      I3 => D_clk_filter_PWR_12_o_divisore_frequenza_count_13_equal_1_o_13_Q,
      I4 => Result(1),
      O => D_clk_filter_Mcount_divisore_frequenza_count_eqn_1
    );
  D_clk_filter_Mcount_divisore_frequenza_count_eqn_01 : LUT5
    generic map(
      INIT => X"E0F0F0F0"
    )
    port map (
      I0 => D_clk_filter_divisore_frequenza_count(12),
      I1 => D_clk_filter_divisore_frequenza_count(11),
      I2 => Result(0),
      I3 => D_clk_filter_PWR_12_o_divisore_frequenza_count_13_equal_1_o_13_1_87,
      I4 => D_clk_filter_PWR_12_o_divisore_frequenza_count_13_equal_1_o_13_Q,
      O => D_clk_filter_Mcount_divisore_frequenza_count_eqn_0
    );
  CLOCK_BUFGP : BUFGP
    port map (
      I => CLOCK,
      O => CLOCK_BUFGP_4
    );
  D_clk_filter_Mcount_divisore_frequenza_count_lut_0_INV_0 : INV
    port map (
      I => D_clk_filter_divisore_frequenza_count(0),
      O => D_clk_filter_Mcount_divisore_frequenza_count_lut(0)
    );
  D_counter_Mmux_count_1_GND_11_o_mux_3_OUT11_INV_0 : INV
    port map (
      I => D_counter_count(0),
      O => D_counter_count_1_GND_11_o_mux_3_OUT_0_Q
    );

end Structure;

