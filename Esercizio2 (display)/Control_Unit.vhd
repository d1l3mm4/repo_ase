----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    18:24:55 01/14/2020 
-- Design Name: 
-- Module Name:    Control_Unit - Behavioural 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Control_Unit is
    Port ( load : in  STD_LOGIC;
           data_in : in  STD_LOGIC_VECTOR(3 downto 0);
           reset : in  STD_LOGIC;
           clock : in  STD_LOGIC;
           data_out : out  STD_LOGIC_VECTOR(15 downto 0));
end Control_Unit;

architecture Behavioural of Control_Unit is

signal data: std_logic_vector(3 downto 0);


begin
 

CU_proc: process(load, clock, data_in, reset)
begin

if(reset ='1') then
data <= (others => '0');
elsif (clock'event and clock='1' and load='1') then
data <=data_in;
end if;

end process;

data_out <= x"000" & data;
end Behavioural;

